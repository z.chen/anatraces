#!/bin/bash
#*************************************************************#
#**A bash script used for changing format of data*************#
#**measured by 8-ch Scope*************************************#
#**Last updated @ 22 Nov,2023*********************************#
#**Z.Chen*****************************************************#
#**z.chen@gsi.de**********************************************#
#**Execution:*************************************************#
#**$ sh ChangeFormat.sh INPUT_PATH OUTPUT_PATH ***************#
#*************************************************************#

echo "Changing file name..."

REMOVEHEAD=""          #remove head
TS=""                  #timestamp
CNT=1                  #new file number

for i in ${1}/*.csv
do
	REMOVEHEAD=${i#*_}               #(from left to right)remove everything before the first _
	TS=${REMOVEHEAD%%.*}             #(from right to left)remove everything after the last .
	echo $TS >> "timestamp.log"
	cp $i ${2}/$CNT.txt
	CNT=$((CNT+1))
done

mv timestamp.log ${2}
cd ${2}
echo "Changing data format inside file..."
sed -i 's/,/ /g' *.txt

echo "running..."
sed -i 's/inf/0/g' *.txt

echo "running..."
sed -i '1,12d' *.txt

echo "In total,"
ls *.txt | wc -l
echo "files have been midified"

echo "ChangeFormat complete!"
