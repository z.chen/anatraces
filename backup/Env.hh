//
//last updated@ 24 Feb,2023
//Z.Chen
//E-mail: z.chen@gsi.de
//

//*********************************************************************************//
//*********************************release notes***********************************//
//22Jan,2023
//some updates on energy calibration
//29Jan,2023
//some updates related to trapez
//10Feb, 2023
//some updates related to Febex4
//21 Feb
//some updates related to display
//6 Mar
//extend num of peaks to fit from 3 to 6
//11 Mar
//add gFebexTraceL to restrict trace length
//add isFebex and isScope
//26 Mar
//optimize setMeans and setSigmas
//
//**********************************************************************************//
#ifndef _ENV_HH_
#define _ENV_HH_

#include <sys/stat.h>
#include <iostream>

//const char* gSettings = "settings.set";
const char* gSettings = "settings.set";
TString gV1742data = "../V1742/output_0018.root";
TString gFebex4data = "./LISA_root_tree.root";
TString gTxtDir = "../fD_fS_3.5V/";
TString gTsfilename = "timestamp.log";
ofstream optPar("export_pars.txt",ios::app);

const int mV = 1000;
const int ns = 1e9;
//const int hMIN = 0;
//const int hMAX = 40000;
int ghMIN = 0;
int ghMAX = 40000;
//const int hTMIN = -400;
//const int hTMAX = 400;
int ghTMIN = -2000;
int ghTMAX = 2000;
const int MHZtoNS = 1000;// unit:  MHz/ns

double window[2] = {-0.2,2.};//Integral window;[ns]
int gtrapezSampleWindow[2] = {700,1500};////Trapez Sample Window;[sample point;pts]
int gtrapezMovingWindowLength = 3200;//[ns]
int gRisingTime = 100;//[ns]
int gDecaytime = 48000;//[ns]
double bwindow[2] = {-40.,-10.};
int gRun[2] = {1,1000};
int gTreeEntry[2] = {0,3000};
int gNbins = 4000;
double gSampleRate = 62.5;// MHz
int gESrange[2] = {0,20000};
int gTraceAmp[2] ={-2000,2000};
double gTSrange[2] = {-200.,200.};
double gCFDwin[2] = {140.,160.};
map<int,double>gRawE;
vector <TGraph*>gRawTraces;
//map<int,TGraph*>gRawTraces;
map<int,Long64_t>gTs;
//map<int,Long64_t>gTs_S;//short
double gCalibPar[2] = {0.,1.};
double gamp[6] = {100.,100.,100.};
double gmean[6] = {11141.,11887.,12573.};
double gsigma[6] = {173.,156.,137.};
double gEres[6] = {-1.,-1.,-1.};
double gTZAmp[2] = {-200,200};//140,-200,1700
//double gTZ2 = 200;//150,+200,1800
double gFra = 0.9;
double gDel = 1.2;//ns
double gParLimit = 12;
int gVerbose = 0;
int gFebexCh = 0;//febex channel ID
int gFebexTraceL = 2000;//febex trace length
bool isFebex = false;
bool isScope = false;
vector <double> gCFDtime;
double gCFDref = 0.0;
TH1F *ghist;
int gcnt_FEC = 0;
int gcnt_loadSetting = 0;
bool gBaselineCorr = false;
bool gTimeCorr = false;
bool glTs = false;
bool gMWD = false;
bool gIntegral = false;
//*************tree***************//
TString gOptRootFileName = "sCVD.root";
Int_t runnum;
Long64_t runts;
Long64_t runts_S;
Double_t Eraw;
Double_t Ecal;
vector<double> tracesX;
vector<double> tracesY;
bool fSaveAsRoot = false;
bool fsaveTraces = false;
TFile *optfile;
TTree *optTree;

inline void setSettingFile(const char* fstring){
	gSettings = fstring;
}

inline void showSettingFile(){
	std::cout<<"setting file: "<<gSettings<<std::endl;
}

inline void exportSettingFile(){
	optPar<<"Settings.filename:   "<<gSettings<<std::endl;
}

inline void setScopeDataDir(const char* fstring){
	gTxtDir = fstring;
}

inline void showScopeDataDir(){
	std::cout<<"Scope data directory: "<<gTxtDir<<std::endl;
}

inline void exportScopeDataDir(){
	optPar<<"Scope.data.directory:   "<<gTxtDir<<std::endl;
}

inline void setTsFile(const char* fstring){
	gTsfilename = fstring;
}

inline void showTsFile(){
	std::cout<<"Timestamp filename: "<<gTsfilename<<std::endl;
}

inline void exportTsFile(){
	optPar<<"Scope.timestamp.filename:   "<<gTsfilename<<std::endl;
}

inline void exportV1742Data(){
	optPar<<"V1742.data.filename:   "<<gV1742data<<std::endl;
}

inline void exportFebex4Data(){
	optPar<<"Febex4.data.filename:   "<<gFebex4data<<std::endl;
}

inline void setIntergralWindow(double fval1, double fval2){
	window[0] = fval1;
	window[1] = fval2;
}

inline void showIntergralWindow(){
	std::cout<<"win0 = "<<window[0]<<" win1 = "<<window[1]<<std::endl;
}

inline void exportIntergralWindow(){
	optPar<<"Integral.window.0:   "<<window[0]<<std::endl;
	optPar<<"Integral.window.1:   "<<window[1]<<std::endl;
}

inline void setTrapezWindow(int fval1, int fval2){
	gtrapezSampleWindow[0] = fval1;
	gtrapezSampleWindow[1] = fval2;
}

inline void showTrapezWindow(){
	std::cout<<"win0 = "<<gtrapezSampleWindow[0]<<" win1 = "<<gtrapezSampleWindow[1]<<std::endl;
}

inline void exportTrapezWindow(){
	optPar<<"Trapez.Sample.window.0:   "<<gtrapezSampleWindow[0]<<std::endl;
	optPar<<"Trapez.Sample.window.1:   "<<gtrapezSampleWindow[1]<<std::endl;
}

inline void showFebexChannel(){
	std::cout<<"current febex channel = "<<gFebexCh<<std::endl;
}

inline void exportFebexChannel(){
	optPar<<"Febex4.read.from.ch:   "<<gFebexCh<<std::endl;
}

inline void showFebexTraceLength(){
	std::cout<<"Trace Length = "<<gFebexTraceL<<std::endl;
}

inline void exportFebexTraceLength(){
	optPar<<"Febex4.trace.length:   "<<gFebexTraceL<<std::endl;
}

inline void setTrapezAmpCalcWindow(int fval1, int fval2){
	gTZAmp[0] = fval1;
	gTZAmp[1] = fval2;
}

inline void showTrapezAmpCalcWindow(){
	std::cout<<"win0 = "<<gTZAmp[0]<<"[ns]; win1 = "<<gTZAmp[1]<<std::endl;
}

inline void exportTrapezAmpCalcWindow(){
	optPar<<"Trapez.Amp.Calc.window.0:   "<<gTZAmp[0]<<std::endl;
	optPar<<"Trapez.Amp.Calc.window.1:   "<<gTZAmp[1]<<std::endl;
}

inline void exportScopeRun(){
	optPar<<"Scope.data.runnummber.0:   "<<gRun[0]<<std::endl;
	optPar<<"Scope.data.runnummber.1:   "<<gRun[1]<<std::endl;
}

inline void exportTreeEntry(){
	optPar<<"TreeEntry.read.from.0:   "<<gTreeEntry[0]<<std::endl;
	optPar<<"TreeEntry.read.from.1:   "<<gTreeEntry[1]<<std::endl;
}

inline void setEnergySpecRange(int fval1, int fval2){
	gESrange[0] = fval1;
	gESrange[1] = fval2;
}

inline void showEnergySpecRange(){
	std::cout<<"ESrange0 = "<<gESrange[0]<<" ESrange1 = "<<gESrange[1]<<std::endl;
}

inline void exportEnergySpecRange(){
	optPar<<"Energy.spectrum.range.0:   "<<gESrange[0]<<std::endl;
	optPar<<"Energy.spectrum.range.1:   "<<gESrange[1]<<std::endl;
}

inline void setTimeSpecRange(int fval1, int fval2){
	gTSrange[0] = fval1;
	gTSrange[1] = fval2;
}

inline void showTimeSpecRange(){
	std::cout<<"TSrange0 = "<<gTSrange[0]<<" TSrange1 = "<<gTSrange[1]<<std::endl;
}

inline void exportTimeSpecRange(){
	optPar<<"Time.spectrum.range.0:   "<<gTSrange[0]<<std::endl;
	optPar<<"Time.spectrum.range.1:   "<<gTSrange[1]<<std::endl;
}

inline void setTraceAmplitude(int fval1, int fval2){
	gTraceAmp[0] = fval1;
	gTraceAmp[1] = fval2;
}

inline void showTraceAmplitude(){
	std::cout<<"min = "<<gTraceAmp[0]<<" max = "<<gTraceAmp[1]<<std::endl;
}

inline void exportTraceAmplitude(){
	optPar<<"Trace.graph.Y.min:   "<<gTraceAmp[0]<<std::endl;
	optPar<<"Trace.graph.Y.max:   "<<gTraceAmp[1]<<std::endl;
}

inline void setCFDwindow(double fval1, double fval2){
	gCFDwin[0] = fval1;
	gCFDwin[1] = fval2;
}

inline void showCFDwindow(){
	std::cout<<"CFDwin0 = "<<gCFDwin[0]<<" CFDwin1 = "<<gCFDwin[1]<<std::endl;
}

inline void exportCFDwindow(){
	optPar<<"CFD.window.0:   "<<gCFDwin[0]<<std::endl;
	optPar<<"CFD.window.1:   "<<gCFDwin[1]<<std::endl;
}

inline void setCFDreference(double fval){
	gCFDref = fval;
}

inline void showCFDreference(){
	std::cout<<"CFDref = "<<gCFDref<<std::endl;
}

inline void exportCFDreference(){
	optPar<<"CFD.reference:   "<<gCFDref<<std::endl;
}

inline void setSampleRate(double fval){
	gSampleRate = fval;
}

inline void showSampleRate(){
	std::cout<<"Sample rate = "<<gSampleRate<<std::endl;
}

inline void exportSampleRate(){
	optPar<<"Sample.rate[MHz]:   "<<gSampleRate<<std::endl;
}

inline void setTrapezMovingWindowLength(double fval){
	gtrapezMovingWindowLength = fval;
}

inline void showTrapezMovingWindowLength(){
	std::cout<<"Moving window length = "<<gtrapezMovingWindowLength<<std::endl;
}

inline void exportTrapezMovingWindowLength(){
	optPar<<"Trapez.Moving.Window.Length[ns]:   "<<gtrapezMovingWindowLength<<std::endl;
}

inline void setRisingTime(double fval){
	gRisingTime = fval;
}

inline void showRisingTime(){
	std::cout<<"Rising Time = "<<gRisingTime<<std::endl;
}

inline void exportRisingTime(){
	optPar<<"Rising.time[ns]:   "<<gRisingTime<<std::endl;
}

inline void setDeacytime(double fval){
	gDecaytime = fval;
}

inline void showDecaytime(){
	std::cout<<"Decay Time = "<<gDecaytime<<std::endl;
}

inline void exportDecaytime(){
	optPar<<"Decay.time[ns]:   "<<gDecaytime<<std::endl;
}

inline void setFitParLimit(double fval){
	gParLimit = fval;
}

inline void showFitParLimit(){
	std::cout<<"fit pars limit = "<<gParLimit<<std::endl;
}

inline void exportFitParLimit(){
	optPar<<"Fit.Pars.Limit:   "<<gParLimit<<std::endl;
}

inline void setNbins(int fval){
	gNbins = fval;
}

inline void showNbins(){
	std::cout<<"Nbins = "<<gNbins<<std::endl;
}

inline void exportNbins(){
	optPar<<"Histogram.bins:   "<<gNbins<<std::endl;
}

inline void setCFDpars(double fFra, double fDel){
	gFra = fFra;
	gDel = fDel;
}

inline void showCFDpars(){
	std::cout<<"Fraction = "<<gFra<<" delay = "<<gDel<<std::endl;
}

inline void exportCFDpars(){
	optPar<<"CFD.pars.fraction:   "<<gFra<<std::endl;
	optPar<<"CFD.pars.delay:   "<<gDel<<std::endl;
}

inline void setVerbose(int fval){
	gVerbose = fval;
}

inline void showVerbose(){
	std::cout<<"verbose = "<<gVerbose<<std::endl;
}

inline void exportVerbose(){
	optPar<<"Verbos:   "<<gVerbose<<std::endl;
}

inline void setMeans(double fm1){
	gmean[0] = fm1;
}

inline void setMeans(double fm1, double fm2){
	gmean[0] = fm1;
	gmean[1] = fm2;
}

inline void setMeans(double fm1, double fm2, double fm3){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
	gmean[5] = fm6;
}

inline void showMeans(){
	std::cout<<"mean0 = "<<gmean[0]<<" mean1 = "<<gmean[1]<<" mean2 = "<<gmean[2]<<std::endl;
	std::cout<<"mean3 = "<<gmean[3]<<" mean4 = "<<gmean[4]<<" mean5 = "<<gmean[5]<<std::endl;
}

inline void exportMeans(){
	optPar<<"Mean.value.0:   "<<gmean[0]<<std::endl;
	optPar<<"Mean.value.1:   "<<gmean[1]<<std::endl;
	optPar<<"Mean.value.2:   "<<gmean[2]<<std::endl;
	optPar<<"Mean.value.3:   "<<gmean[3]<<std::endl;
	optPar<<"Mean.value.4:   "<<gmean[4]<<std::endl;
	optPar<<"Mean.value.5:   "<<gmean[5]<<std::endl;
}

inline void exportEnergyResolution(){
	optPar<<"dE/E.value.0:   "<<gEres[0]<<std::endl;
	optPar<<"dE/E.value.1:   "<<gEres[1]<<std::endl;
	optPar<<"dE/E.value.2:   "<<gEres[2]<<std::endl;
	optPar<<"dE/E.value.3:   "<<gEres[3]<<std::endl;
	optPar<<"dE/E.value.4:   "<<gEres[4]<<std::endl;
	optPar<<"dE/E.value.5:   "<<gEres[5]<<std::endl;
}

inline void setSigmas(double fs1){
	gsigma[0] = fs1;
}

inline void setSigmas(double fs1, double fs2){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
}

inline void setSigmas(double fs1, double fs2, double fs3){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
	gsigma[5] = fs6;
}

inline void showSigmas(){
	std::cout<<"sigma0 = "<<gsigma[0]<<" sigma1 = "<<gsigma[1]<<" sigma2 = "<<gsigma[2]<<std::endl;
	std::cout<<"sigma3 = "<<gsigma[3]<<" sigma4 = "<<gsigma[4]<<" sigma5 = "<<gsigma[5]<<std::endl;
}

inline void exportSigmas(){
	optPar<<"Sigma.value.0:   "<<gsigma[0]<<std::endl;
	optPar<<"Sigma.value.1:   "<<gsigma[1]<<std::endl;
	optPar<<"Sigma.value.2:   "<<gsigma[2]<<std::endl;
	optPar<<"Sigma.value.3:   "<<gsigma[3]<<std::endl;
	optPar<<"Sigma.value.4:   "<<gsigma[4]<<std::endl;
	optPar<<"Sigma.value.5:   "<<gsigma[5]<<std::endl;
}

inline void setAmplitude(double fs1){
	gamp[0] = fs1;
}

inline void setAmplitude(double fs1, double fs2){
	gamp[0] = fs1;
	gamp[1] = fs2;
}

inline void setAmplitude(double fs1, double fs2, double fs3){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
}

inline void showAmplitude(){
	std::cout<<"amp0 = "<<gamp[0]<<" amp1 = "<<gamp[1]<<" amp2 = "<<gamp[2]<<std::endl;
}

inline void exportAmplitude(){
	optPar<<"Amplitude.value.0:   "<<gamp[0]<<std::endl;
	optPar<<"Amplitude.value.1:   "<<gamp[1]<<std::endl;
	optPar<<"Amplitude.value.2:   "<<gamp[2]<<std::endl;
}

inline void setEnergyCalibrationPars(double fbias, double fgrad){
	gCalibPar[0] = fbias;
	gCalibPar[1] = fgrad;
}

inline void showEnergyCalibrationPars(){
	std::cout<<"bias = "<<gCalibPar[0]<<" gradient = "<<gCalibPar[1]<<std::endl;
}

inline void exportEnergyCalibrationPars(){
	optPar<<"Energy.calibration.par.bias:   "<<gCalibPar[0]<<std::endl;
	optPar<<"Energy.calibration.par.gradient:   "<<gCalibPar[1]<<std::endl;
}

inline void setOutputRootFile(const char* fstring){
	gOptRootFileName = fstring;
}

inline void showOutputRootFile(){
	std::cout<<"Output root file: "<<gOptRootFileName<<std::endl;
}

inline void exportOutputRootFile(){
	optPar<<"SaveAsRoot.filename:   "<<gOptRootFileName<<std::endl;
}

inline void enableBaseLineCorr(){
	gBaselineCorr = true;
}

inline void disableBaseLineCorr(){
	gBaselineCorr = false;
}

inline void exportBaselineCorr(){
	optPar<<"Enable.baseline.correction:   "<<gBaselineCorr<<std::endl;
}

inline void enableIntegral(){
	gIntegral = true;
	gMWD = false;
}

inline void disableIntegral(){
	gIntegral = false;
	gMWD = true;
}

inline void exportIntegralSetting(){
	optPar<<"Enable.energyCalculation.integral:   "<<gIntegral<<std::endl;
}

inline void enableAlgorithm(){
	gMWD = true;
	gIntegral = false;
}

inline void disableAlgorithm(){
	gMWD = false;
	gIntegral = true;
}

inline void exportAlgorithmSetting(){
	optPar<<"Enable.energyCalculation.algorithm:   "<<gIntegral<<std::endl;
}

inline void enableLoadTimestamp(){
	glTs = true;
}

inline void disableLoadTimestamp(){
	glTs = false;
}

inline void enableTimeCorr(){
	gTimeCorr = true;
}

inline void disableTimeCorr(){
	gTimeCorr = false;
}

inline void exportTimeCorr(){
	optPar<<"Enable.time.correction:   "<<gTimeCorr<<std::endl;
}

inline void enableSaveAsRoot(){
	fSaveAsRoot = true;
}

inline void disableSaveAsRoot(){
	fSaveAsRoot = false;
}

inline void enableSaveTraces(){
	fsaveTraces = true;
}

inline void disableSaveTraces(){
	fsaveTraces = false;
}

inline bool isOpen(const std::string& name){
	struct stat buffer;
	return (stat(name.c_str(),&buffer) == 0);
}

inline void exportParameters(){
	exportVerbose();
	exportScopeRun();
	exportTreeEntry();
	exportScopeDataDir();
	exportTsFile();
	exportV1742Data();
	exportIntergralWindow();
	exportNbins();
	exportBaselineCorr();
	exportTimeCorr();
	exportTraceAmplitude();
	exportTrapezWindow();
	exportTrapezAmpCalcWindow();
	exportTrapezMovingWindowLength();
	exportRisingTime();
	exportSampleRate();
	exportDecaytime();
	exportEnergySpecRange();
	exportIntegralSetting();
	exportAlgorithmSetting();
	exportTimeSpecRange();
	exportFitParLimit();
	exportCFDreference();
	exportCFDwindow();
	exportCFDpars();
	exportEnergyCalibrationPars();
	exportMeans();
	exportSigmas();
	exportAmplitude();
	exportEnergyResolution();
	exportSettingFile();
	exportOutputRootFile();
	std::cout<<"--->All the parameters are exported."<<std::endl;
}

inline void setEnt(){
	gROOT->SetStyle("Modern");
	gStyle->SetHistFillColor(7);
	gStyle->SetHistFillStyle(3002);
	gStyle->SetHistLineColor(kBlue);
	gStyle->SetFuncColor(kRed);
	gStyle->SetFrameLineWidth(2);
	gStyle->SetCanvasColor(0);
	gStyle->SetTitleFillColor(0);
	gStyle->SetTitleStyle(0);
	gStyle->SetStatColor(0);
	gStyle->SetStatStyle(0);
	gStyle->SetStatX(0.9);  
	gStyle->SetStatY(0.9);  
	gStyle->SetPalette(1);
	//gStyle->SetOptLogz(1);
	//  gStyle->SetOptTitle(0);
	//gStyle->SetOptFit(1);
	gStyle->SetOptStat(1111111);
	//gStyle->SetOptStat(0);
	//gStyle->SetPadBorderMode(1);
	//  gStyle->SetOptDate(1);
	//gStyle->SetLabelFont(132,"XYZ");
	//gStyle->SetTitleFont(132,"XYZ");
	//gStyle->SetTitleFont(132,"");
	//gStyle->SetTextFont(132);
	//gStyle->SetStatFont(132);
}

//const
inline Double_t fitConst(Double_t *xx, Double_t *fpar){
	return fpar[0] + xx[0] * 0.0;
}
//gaus
inline Double_t fitGaus1(Double_t *xx, Double_t *fpar){
	return fpar[0]*TMath::Exp(-0.5*TMath::Power( (xx[0]-fpar[1])/fpar[2],1) );
}

//const + gaus; 4pars
inline Double_t gauspeaks1(Double_t *xx, Double_t *fpar){
	return fitConst(xx,fpar) + fitGaus1(xx,&fpar[1]);
}

#endif
