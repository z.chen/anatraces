//last updated@ 21 Feb,2023
//Z.Chen
//***************************//
//********release notes****************//
//15 Jan, 2023
//optimize baseline correction in view single trace: "anaV1742_checkSingleTrace()"
//add "welcome()"
//26 Jan, 2023
//some updates related to CSTA2 pre-amp test results
//29 Jan, 2023
//some updates related to trapez
//21 Feb, 2023
//some updates related to display
//10 Mar, 2023
//comment those parts related to checkLastPoint();
//
#include "TH1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TSystem.h"
#include <map>
#include <vector>
#include <iostream>

//basic functions for analysis
#include "viewtrace.hh"
//for 8-ch scope data
#include "anaScope.hh"
//for V1742 digitizer
#include "anaV1742.hh"
//for Febex4 digitizer
#include "anaFebex4.hh"

void ViewTrace(){

	welcome();

}

void checkNew(){

	vector<double> xx;
	vector<double> yy;
	xx.clear();
	yy.clear();
	loadFromTXT(22,xx,yy);
	std::cout<<"test2:"<<xx.back()<<" "<<yy.back()<<std::endl;
}

void anaTxt_checkSingleTrace(int run = 22){
	setEnt();
	if(!loadSettings())return;
	//*****load data*****//
	vector<double> xx;
	vector<double> yy;
	xx.clear();
	yy.clear();
	loadFromTXT(run,xx,yy);

	TCanvas *can = new TCanvas("checkSingleTrace","checkSingleTrace",10,10,1200,1000);
	can->Divide(2,2);
	can->cd(1);

	TGraph *fgr2;
	if(gBaselineCorr){
		fgr2 = baselineCorr(getSingleTrace(xx,yy,false),false);
	}
	else{
		fgr2 = getSingleTrace(xx,yy,false);
	}
	if(fgr2==0)return;
	fgr2->SetMarkerColor(kRed);
	fgr2->Draw("AP*");

	can->cd(2);
	TGraph *fcfd = CFD(fgr2);
	std::cout<<"Time determined by CFD = "<<gCFDtime.back()<<"[ns]."<<std::endl;
	fcfd->Draw("AP*");
	can->cd(3);
	TMultiGraph *mg = new TMultiGraph();
	mg->Add(fgr2);
	mg->Add(fcfd);
	mg->Draw("AP*");
	mg->SetTitle("Time discrimination;Time[ns]");
}

void anaTxt_checkTraces(int run1 = -1, int run2 = 1000, bool fplot = true){
	setEnt();
	//****load data*****//
	if(!loadSettings())return;

	TCanvas *can = new TCanvas("checkTraces","checkTraces",500,10,1200,800);
	can->Divide(2,2);
	can->cd(1);
	ViewTraces(run1,run2,fplot);
	can->cd(2);
	showTimeSpectrum();
	can->cd(3);
	showEnergySpectrum();
	can->cd(4);
	showRawTraces();
}
/*
   void checkCFD(int run){
   TCanvas *testc = new TCanvas("test","test",10,10,1000,1000);
   testc->Divide(2,2);
   testc->cd(1);
   TGraph *fgr1 = ViewSingleTrace(run,1);
   fgr1->SetMarkerColor(kRed);
   testc->cd(2);
   TGraph *fcCFD = CFD(ViewSingleTrace(run,0),0);
   fcCFD->Draw("AP*");
   testc->cd(3);
   TMultiGraph *mg = new TMultiGraph();
   mg->Add(fgr1);
   mg->Add(fcCFD);
   mg->Draw("AP*");

   }

*/
void checkMWDScope(int run=1435){
	setEnt();
	if(!loadSettings())return;
	//*****load data*****//
	vector<double> xx;
	vector<double> yy;
	xx.clear();
	yy.clear();
	loadFromTXT(run,xx,yy);
	TCanvas *testc = new TCanvas("test","test",10,10,1000,1000);
	testc->Divide(2,2);
	testc->cd(1);
	TGraph *fgr2;
	if(gBaselineCorr){
		fgr2 = baselineCorr(getSingleTrace(xx,yy,false),false);
	}
	else{
		fgr2 = getSingleTrace(xx,yy,false);
	}
	if(fgr2==0)return;
	fgr2->SetMarkerColor(kRed);
	fgr2->Draw("AP*");
	testc->cd(2);
	TGraph *fcMWD = MWD(fgr2);
	//getEdg(fcMWD);
	double fE = getEdg3(fcMWD);
	//bool flast = checkLastPoint(fcMWD);
	//if(flast){
	//	std::cout<<"last point checked."<<std::endl;
	//	std::cout<<"Energy(amplitude) = "<<fE<<std::endl;
	//}
	std::cout<<"Energy(amplitude) = "<<fE<<std::endl;
	fcMWD->Draw("AP*");
	fcMWD->GetXaxis()->SetTitle("Time[ns]");
	testc->cd(3);
	TMultiGraph *mg = new TMultiGraph();
	mg->Add(fgr2);
	mg->Add(fcMWD);
	mg->Draw("AP*");
	mg->GetXaxis()->SetTitle("Time[ns]");
}
/*
   void checkCRRC(int run){
   TCanvas *testc = new TCanvas("test","test",10,10,1000,1000);
   testc->Divide(2,2);
   testc->cd(1);
   TGraph *fgr1 = ViewSingleTrace(run,1);
   fgr1->SetMarkerColor(kRed);
//fgr1->Draw("AP*");
testc->cd(2);
TGraph *fcCRRC = CRRC(ViewSingleTrace(run));
//getEdg(fcMWD);
//bool flast = checkLastPoint(fcMWD);
fcCRRC->Draw("AP*");
testc->cd(3);
TMultiGraph *mg = new TMultiGraph();
mg->Add(fgr1);
mg->Add(fcCRRC);
mg->Draw("AP*");
}

void test(int run){
if(ViewSingleTrace(run,0)!=0)baselineCorr(ViewSingleTrace(run,0));
}

//display N traces and the generated energy spectra
void anaScope_checkTraces(int run1=1, int run2=2000, bool fplot = false){
TCanvas *cTraces = new TCanvas("N traces","N traces",10,10,1600,400);
cTraces->Divide(2,2);
cTraces->cd(1);
ViewTraces(run1,run2,fplot);
cTraces->cd(2);
showEnergySpectrum();
cTraces->cd(3);
showTimeSpectrum();
}
*/
void quickCheck(){
	/*
	   ViewTraces(1,1290);
	   energyCalib(1);
	   ViewTraces();
	   setNbins(3200);
	   setEnergySpecRange(4000,7000);
	   fineEnergyCalib();
	   */
	//ViewTraces(1,9999);
}

void anaV1742_checkSingleTrace(Long64_t jentry = 22){
	setEnt();
	if(!loadSettings())return;
	TCanvas *can = new TCanvas("checkSingleTrace","checkSingleTrace",10,10,1200,1000);
	can->Divide(2,2);
	can->cd(1);
	TTree *ftree = loadFromV1742();
	//testTree(ftree);
	TGraph *fgr2;
	if(gBaselineCorr){
		fgr2 = baselineCorr(getSingleTrace(ftree,jentry,false),false);
	}
	else{
		fgr2 = getSingleTrace(ftree,jentry,false);
	}
	if(fgr2==0)return;
	fgr2->SetMarkerColor(kRed);
	fgr2->Draw("AP*");
	//getTraces(ftree);
	can->cd(2);
	TGraph *fcfd = CFD(fgr2);
	fcfd->Draw("AP*");
	can->cd(3);
	TMultiGraph *mg = new TMultiGraph();
	mg->Add(fgr2);
	mg->Add(fcfd);
	mg->Draw("AP*");
	mg->SetTitle("Time discrimination;Time[ns]");
	can->cd(4);
	TGraph *fMWD = MWD(fgr2);
	double fenergy = getEdg(fMWD);
	fMWD->Draw("AP*");
	std::cout<<"--->Energy = "<<fenergy<<std::endl;

}

void anaV1742_checkTraces(){
	//TStopwatch tt;
	//tt.Start();
	setEnt();
	//****load data*****//
	if(!loadSettings())return;
	TTree *ftree = loadFromV1742();
	TCanvas *can = new TCanvas("checkTraces","checkTraces",500,10,1200,800);
	can->Divide(2,2);
	can->cd(1);
	//testTree(ftree);
	getTraces(ftree);
	can->cd(2);
	showTimeSpectrum();
	can->cd(3);
	showEnergySpectrum();
	//tt.Stop();
	//tt.Print();
}

void anaV1742_channels(Long64_t jentry = 22){
	setEnt();
	if(!loadSettings())return;
	TCanvas *can = new TCanvas("checkSingleTrace","checkSingleTrace",10,10,1200,1000);
	can->Divide(2,2);
	can->cd(1);
	TTree *ftree = loadFromV1742();
	TGraph *fgr1 = getSingleTrace(ftree,jentry,16,false);
	//TGraph *fgr1 = baselineCorr(getSingleTrace(ftree,jentry,false),false);
	if(fgr1==0)return;
	fgr1->SetMarkerColor(kRed);
	fgr1->Draw("AP*");

	can->cd(2);
	TGraph *fgr2 = getSingleTrace(ftree,jentry,23,false);
	fgr2->Draw("AP*");

	can->cd(3);
	TGraph *fgr3 = getSingleTrace(ftree,jentry,24,false);
	fgr3->Draw("AP*");

	can->cd(4);
	TGraph *fgr4 = getSingleTrace(ftree,jentry,30,false);
	//TGraph *fgr4 = graphAdd(fgr1,fgr2,1,-1);
	//fgr4->SetTitle("fgr1 + fgr2");
	fgr4->Draw("AP*");
}

void anaFebex4_checkSingleTrace(Long64_t jentry = 22){
	setEnt();
	if(!loadSettings())return;
	TCanvas *can = new TCanvas("checkSingleTrace","checkSingleTrace",10,10,1200,1000);
	can->Divide(2,2);
	can->cd(1);
	TTree *ftree = loadFromFebex4();
	//testFebex4Tree(ftree);
	//return;
	TGraph *fgr2;
	if(gBaselineCorr){
		fgr2 = baselineCorr(getSingleTraceFebex4(ftree,jentry,false),false);
	}
	else{
		fgr2 = getSingleTraceFebex4(ftree,jentry,false);
	}
	if(fgr2==0)return;
	fgr2->SetMarkerColor(kRed);
	fgr2->Draw("AP*");
	//getTraces(ftree);
	can->cd(2);
	TGraph *fcfd = CFD(fgr2);
	std::cout<<"Time determined by CFD = "<<gCFDtime.back()<<"[ns]."<<std::endl;
	fcfd->Draw("AP*");
	can->cd(3);
	TMultiGraph *mg = new TMultiGraph();
	mg->Add(fgr2);
	mg->Add(fcfd);
	mg->Draw("AP*");
	mg->SetTitle("Time discrimination;Time[ns]");

	can->cd(4);

	TGraph *fcMWD = MWD(fgr2);
	//getEdg(fcMWD);
	double fE = getEdg3(fcMWD);
	//bool flast = checkLastPoint(fcMWD);
	//if(flast)std::cout<<"last point."<<std::endl;
	std::cout<<"Energy(amplitude) = "<<fE<<std::endl;
	fcMWD->Draw("AP*");
	fcMWD->GetXaxis()->SetTitle("Time[ns]");
	//testc->cd(3);
	//TMultiGraph *mg = new TMultiGraph();
	//mg->Add(fgr2);
	//mg->Add(fcMWD);
	//mg->Draw("AP*");
	//mg->GetXaxis()->SetTitle("Time[ns]");
}

void anaFebex4_checkTraces(){
	//TStopwatch tt;
	//tt.Start();
	setEnt();
	//****load data*****//
	if(!loadSettings())return;
	TTree *ftree = loadFromFebex4();
	TCanvas *can = new TCanvas("checkTraces","checkTraces",500,10,1200,800);
	can->Divide(2,2);
	can->cd(1);
	//testTree(ftree);
	getTracesFebex4(ftree);
	can->cd(2);
	showTimeSpectrum();
	can->cd(3);
	showEnergySpectrum();
	can->cd(4);
	showRawTraces();
	//tt.Stop();
	//tt.Print();
}

void checkMWDFebex(int run=1435){
	setEnt();
	if(!loadSettings())return;
	//*****load data*****//
	TTree *ftree = loadFromFebex4();
	TCanvas *testc = new TCanvas("test","test",10,10,1000,1000);
	testc->Divide(2,2);
	testc->cd(1);
	TGraph *fgr2;
	if(gBaselineCorr){
		fgr2 = baselineCorr(getSingleTraceFebex4(ftree,run,false),false);
	}
	else{
		fgr2 = getSingleTraceFebex4(ftree,run,false);
	}
	if(fgr2==0)return;
	fgr2->SetMarkerColor(kRed);
	fgr2->Draw("AP*");
	testc->cd(2);
	TGraph *fcMWD = MWD(fgr2);
	//getEdg(fcMWD);
	double fE = getEdg3(fcMWD);
	//bool flast = checkLastPoint(fcMWD);
	//if(flast)std::cout<<"last point."<<std::endl;
	std::cout<<"Energy(amplitude) = "<<fE<<std::endl;
	fcMWD->Draw("AP*");
	fcMWD->GetXaxis()->SetTitle("Time[ns]");
	testc->cd(3);
	TMultiGraph *mg = new TMultiGraph();
	mg->Add(fgr2);
	mg->Add(fcMWD);
	mg->Draw("AP*");
	mg->GetXaxis()->SetTitle("Time[ns]");
}
