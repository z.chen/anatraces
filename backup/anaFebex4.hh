//
//last updated@21 Feb,2022
//Z.Chen
//
//Error6xx.
//

//********release notes****************//
//21 Feb, 2023
//add timer to getTracesFebex4();
//some updates on raw traces display
//10 Mar, 2023
//comment those parts related to checkLastPoint();
//export raw traces to root file;(previously, only export traces after filter)
//11 Mar
//add gFebexTraceL to restrict trace length
//add isFebex
//

#ifndef _ANAFEBEX4_HH_
#define _ANAFEBEX4_HH_

#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>

#include "viewtrace.hh"


// Declaration of leaf types
//Double_t        tracesX[10000];
//Double_t        tracesY[10000];
Double_t        feb4_trace_raw[16][10000];
//Double_t        feb4_trace_amp[16][10000];

// List of branches
//TBranch        *b_tracesX;   //!
//TBranch        *b_tracesY;   //!
TBranch        *b_feb4_trace_raw;   //!
//TBranch        *b_feb4_trace_amp;   //!


void InitFebex4Tree(TTree *ftree)
{
	// Set branch addresses and branch pointers
	if (!ftree) {
		std::cout<<"Error601!!! No tree. PLease check input file"<<std::endl;
		return;
	}

	//ftree->SetBranchAddress("tracesX", tracesX, &b_tracesX);
	//ftree->SetBranchAddress("tracesY", tracesY, &b_tracesY);
	ftree->SetBranchAddress("feb4_trace_raw[16][10000]", feb4_trace_raw, &b_feb4_trace_raw);
	//ftree->SetBranchAddress("feb4_trace_amp[16][10000]", feb4_trace_amp, &b_feb4_trace_amp);
}


TTree* loadFromFebex4(){
	std::cout<<"--->Load data from "<<gFebex4data<<std::endl;
	TFile *iptfile = new TFile(gFebex4data);
	if(!iptfile->IsOpen()){
		std::cout<<"Error602!!! Can not open/find data file"<<gFebex4data<<std::endl;
		return 0;
	}
	TTree *iptTree = (TTree*)iptfile->Get("tree");
	if(!iptTree){
		std::cout<<"Error603!!! Can not find tree"<<std::endl;
		return 0;
	}
	InitFebex4Tree(iptTree);
	return iptTree;
}


void testFebex4Tree(TTree* ftree){
	if (!ftree) {
		std::cout<<"Error604!!! No tree. PLease check input file"<<std::endl;
		return;
	}
	ftree->Draw("feb4_trace_raw[0]","","colz");
	//ftree->Draw("tracesY:tracesX","","colz");
}

TGraph* getSingleTraceFebex4(TTree *ftree, Long64_t jentry, int fch, bool fdraw=false){
	if(gVerbose>0)std::cout<<"--->Reading single trace from event "<<jentry<<std::endl;
	if (!ftree) {
		std::cout<<"Error605!!! No tree. PLease check input file"<<std::endl;
		return 0;
	}
	isFebex=true;
	vector<double>yy;//amplitude
	vector<double>xx;//sample
	yy.clear();
	xx.clear();
	ftree->GetEntry(jentry);
	for(int i=0;i<gFebexTraceL;i++){
		//yy.push_back(tracesY[i]);
		//xx.push_back(tracesX[i]*10);//samples to ns
		//yy.push_back(feb4_trace_amp[gFebexCh][i]);
		yy.push_back(feb4_trace_raw[gFebexCh][i]);
		//yy.push_back(feb4_trace_raw[0][i]);
		xx.push_back(i*10);//samples to ns
		//test
		//std::cout<<"yy = "<<yy.back()<<std::endl;
	}
	//std::cout<<"Max num of sample = "<<1024<<std::endl;
	TGraph *grtem = new TGraph(yy.size(),&xx[0],&yy[0]);
	if(fdraw)grtem->Draw("AP*");
	grtem->SetTitle(Form("Febex4_Ch%d: 10ns/Sample",0));
	grtem->GetXaxis()->SetTitle("Samples");
	grtem->GetYaxis()->SetTitle("Amplitude[arb.]");
	return grtem;
}

void getTracesFebex4(TTree* ftree){
	if (!ftree) {
		std::cout<<"Error606!!! No tree. PLease check input file"<<std::endl;
		return;
	}

	isFebex=true;
	vector<TGraph*>graphs;
	graphs.clear();
	gCFDtime.clear();
	gRawE.clear();
	gRawTraces.clear();

	if(fSaveAsRoot)initRoot();

	//Long64_t fnentries = 20;
	Long64_t fnentries = ftree->GetEntriesFast();
	std::cout<<"--->Total events: "<<std::endl;
	std::cout<<fnentries<<std::endl;
	TGraph *fgrsin;
	TStopwatch timer;
	for(Long64_t jentry=gTreeEntry[0];jentry<fnentries;jentry++){
		//if(jentry<gTreeEntry[0])continue;
		if(getSingleTraceFebex4(ftree,jentry,false) == 0)continue;
		if(gTimeCorr){
			fgrsin	= timeDriftCorr( baselineCorr(getSingleTraceFebex4(ftree,jentry,false),false),false );
		}else if(gBaselineCorr && !gTimeCorr){
			fgrsin = baselineCorr(getSingleTraceFebex4(ftree,jentry,false),false);
		}
		else{
			fgrsin = getSingleTraceFebex4(ftree,jentry,false);
		}
		if(!fgrsin)continue;

		//*****get time info*****//
		CFD(fgrsin,false);
		//*** save raw traces to a vector  ***//
		gRawTraces.push_back(fgrsin);
		if(gIntegral){
			//****************** determine energy**************//
			graphs.push_back(fgrsin);gRawE[jentry] = Integrate(graphs.back());
		}
		else if(gMWD){
			//if(checkLastPoint(MWD(fgrsin))){
				graphs.push_back(MWD(fgrsin));
				gRawE[jentry] = getEdg3(graphs.back());
				//gRawE[jentry] = getEdg2(graphs.back());
				//gRawE[jentry] = getEdg(graphs.back());
				if(gRawE[jentry]<0)gRawE[jentry]*=(-1);
			//}
		}else{
			std::cout<<"Warning607!!! Please select energy calculation method."<<std::endl;return;

		}
		//std::cout<<"***energy = "<<gRawE[jentry]<<std::endl;
		//gRawE[jentry] =  Integrate(graphs.back()) ;//map<int,double>;calculate energy
		double fEnergy = gCalibPar[0] + gRawE[jentry] * gCalibPar[1];//calibration
		Long64_t fts=-1;
		timer.Stop();
		double fspeed = timer.RealTime();
		double ftimeToGo = (gTreeEntry[1]-jentry)*fspeed;
		//if(fSaveAsRoot)saveAsRoot(optTree,jentry,fts,gRawE[jentry],fEnergy,graphs.back());
		if(fSaveAsRoot)saveAsRoot(optTree,jentry,fts,gRawE[jentry],fEnergy,gRawTraces.back());
		if(jentry%10==0)std::cout<<"Num:"<<jentry<<"/"<<fnentries<<"  "<<ftimeToGo<<"s to go.";
		fflush(stdout);
		printf("\r");

		if(jentry>gTreeEntry[1])break;
	}
	if( gBaselineCorr && !gTimeCorr )std::cout<<"--->Baseline correction: On."<<std::endl;
	if( gTimeCorr )std::cout<<"--->Time correction: On."<<std::endl;
	std::cout<<"N = "<<graphs.size()<<" traces have been loaded."<<std::endl;
	std::cout<<"complete!"<<std::endl;
	if(fSaveAsRoot)closeRoot(optTree,optfile);
	//TCanvas *mycan = new TCanvas("traces","traces",10,10,1000,1000);
	//mycan->cd(1);
	if(graphs.size()>0){
		graphs.at(0)->Draw("APL");
		graphs.at(0)->SetTitle("Febex Traces: 10ns/Sample");
		graphs.at(0)->GetXaxis()->SetTitle("Time[ns]");
		graphs.at(0)->GetYaxis()->SetTitle("Amplitude[arb.]");
		graphs.at(0)->SetMaximum(gTraceAmp[1]);
		graphs.at(0)->SetMinimum(gTraceAmp[0]);
		for(int i=0;i<graphs.size();i++){
			graphs.at(i)->SetLineColor(1+i%40);
			graphs.at(i)->Draw("PL");
		}
	}
}




#endif
