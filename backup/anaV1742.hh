//
//last updated@15 Feb,2023
//Z.Chen
//
//Error5xx.
//

#ifndef _ANAV1742_HH_
#define _ANAV1742_HH_

#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>

#include "viewtrace.hh"

Bool_t          FRSUnpackEvent_bEvent;
Int_t           FRSUnpackEvent_s2wfd2[32][1024];
Int_t           FRSUnpackEvent_s2wfd2_trn[4][1024];
Float_t         FRSUnpackEvent_s2wfd2_ns_per_ch;
Int_t           FRSUnpackEvent_s2wfd2_startid[4];
Int_t           FRSUnpackEvent_s2wfd2_nsample;
Long64_t        FRSUnpackEvent_s2wfd2_timestamp;

TBranch        *b_FRSUnpackEvent_bEvent;   //!
TBranch        *b_FRSUnpackEvent_s2wfd2;   //!
TBranch        *b_FRSUnpackEvent_s2wfd2_trn;   //!
TBranch        *b_FRSUnpackEvent_s2wfd2_ns_per_ch;   //!
TBranch        *b_FRSUnpackEvent_s2wfd2_startid;   //!
TBranch        *b_FRSUnpackEvent_s2wfd2_nsample;   //!
TBranch        *b_FRSUnpackEvent_s2wfd2_timestamp;   //!

void Init(TTree *ftree)
{
	// Set branch addresses and branch pointers
	if (!ftree) {
		std::cout<<"Error501!!! No tree. PLease check input file"<<std::endl;
		return;
	}

	ftree->SetBranchAddress("FRSUnpackEvent.bEvent", &FRSUnpackEvent_bEvent, &b_FRSUnpackEvent_bEvent);
	ftree->SetBranchAddress("FRSUnpackEvent.s2wfd2[32][1024]", FRSUnpackEvent_s2wfd2, &b_FRSUnpackEvent_s2wfd2);
	ftree->SetBranchAddress("FRSUnpackEvent.s2wfd2_trn[4][1024]", FRSUnpackEvent_s2wfd2_trn, &b_FRSUnpackEvent_s2wfd2_trn);
	ftree->SetBranchAddress("FRSUnpackEvent.s2wfd2_ns_per_ch", &FRSUnpackEvent_s2wfd2_ns_per_ch, &b_FRSUnpackEvent_s2wfd2_ns_per_ch);
	ftree->SetBranchAddress("FRSUnpackEvent.s2wfd2_startid[4]", FRSUnpackEvent_s2wfd2_startid, &b_FRSUnpackEvent_s2wfd2_startid);
	ftree->SetBranchAddress("FRSUnpackEvent.s2wfd2_nsample", &FRSUnpackEvent_s2wfd2_nsample, &b_FRSUnpackEvent_s2wfd2_nsample);
	ftree->SetBranchAddress("FRSUnpackEvent.s2wfd2_timestamp", &FRSUnpackEvent_s2wfd2_timestamp, &b_FRSUnpackEvent_s2wfd2_timestamp);

}

void testTree(TTree* ftree){
	if (!ftree) {
		std::cout<<"Error502!!! No tree. PLease check input file"<<std::endl;
		return;
	}
	ftree->Draw("FRSUnpackEvent.s2wfd2_timestamp");
}

TGraph* getSingleTrace(TTree *ftree, Long64_t jentry,bool fdraw=false){
	if(gVerbose>0)std::cout<<"--->Reading single trace from event "<<jentry<<std::endl;
	if (!ftree) {
		std::cout<<"Error503!!! No tree. PLease check input file"<<std::endl;
		return 0;
	}
	vector<double>yy;//amplitude
	vector<double>xx;//sample
	yy.clear();
	xx.clear();
	ftree->GetEntry(jentry);
	for(int i=0;i<1024;i++){
		yy.push_back(FRSUnpackEvent_s2wfd2[16][i] - FRSUnpackEvent_s2wfd2[17][i]);//subtract noise
		//yy.push_back(FRSUnpackEvent_s2wfd2[20][i]);
		//xx.push_back(i);//samples
		xx.push_back(i*0.2);//every 200ps
	}
	//std::cout<<"Max num of sample = "<<1024<<std::endl;
	//std::cout<<"Reading from ch "<<16<<std::endl;
	TGraph *grtem = new TGraph(yy.size(),&xx[0],&yy[0]);
	if(fdraw)grtem->Draw("AP*");
	grtem->SetTitle("V1742 Traces: 200ps/Sample");
	grtem->GetXaxis()->SetTitle("Time[ns]");
	grtem->GetYaxis()->SetTitle("Amplitude[mV]");
	return grtem;
}

TGraph* getSingleTrace(TTree *ftree, Long64_t jentry, int fch, bool fdraw=false){
	if(gVerbose>0)std::cout<<"--->Reading single trace from event "<<jentry<<std::endl;
	if (!ftree) {
		std::cout<<"Error503!!! No tree. PLease check input file"<<std::endl;
		return 0;
	}
	vector<double>yy;//amplitude
	vector<double>xx;//sample
	yy.clear();
	xx.clear();
	ftree->GetEntry(jentry);
	for(int i=0;i<1024;i++){
		//yy.push_back(FRSUnpackEvent_s2wfd2[16][i] - FRSUnpackEvent_s2wfd2[17][i]);
		yy.push_back(FRSUnpackEvent_s2wfd2[fch][i]);
		xx.push_back(i);//samples
		//xx.push_back(i*0.2);//every 200ps
	}
	//std::cout<<"Max num of sample = "<<1024<<std::endl;
	TGraph *grtem = new TGraph(yy.size(),&xx[0],&yy[0]);
	if(fdraw)grtem->Draw("AP*");
	grtem->SetTitle(Form("V1742_Ch%d: 200ps/Sample",fch));
	grtem->GetXaxis()->SetTitle("Samples");
	grtem->GetYaxis()->SetTitle("Amplitude[mV]");
	return grtem;
}

void getTraces(TTree* ftree){
	if (!ftree) {
		std::cout<<"Error504!!! No tree. PLease check input file"<<std::endl;
		return;
	}

	vector<TGraph*>graphs;
	graphs.clear();
	gCFDtime.clear();
	gRawE.clear();

	if(fSaveAsRoot)initRoot();

	//Long64_t fnentries = 20;
	Long64_t fnentries = ftree->GetEntriesFast();
	std::cout<<"--->Total events: "<<std::endl;
	std::cout<<fnentries<<std::endl;
	for(Long64_t jentry=0;jentry<fnentries;jentry++){
		if(getSingleTrace(ftree,jentry,false) == 0)continue;
		if(gTimeCorr){
			graphs.push_back(timeDriftCorr( baselineCorr(getSingleTrace(ftree,jentry,false),false),false ) );
		}else if(gBaselineCorr && !gTimeCorr){
			graphs.push_back( baselineCorr(getSingleTrace(ftree,jentry,false),false) );
		}
		else{
			graphs.push_back(getSingleTrace(ftree,jentry,false));
		}

		CFD(graphs.back(),false);
		//gRawE[jentry] =  getEdg( MWD(graphs.back()) ) ;//
		gRawE[jentry] =  Integrate(graphs.back()) ;//map<int,double>;calculate energy
		double fEnergy = gCalibPar[0] + gRawE[jentry] * gCalibPar[1];//calibration
		Long64_t fts=-1;
		if(fSaveAsRoot)saveAsRoot(optTree,jentry,fts,gRawE[jentry],fEnergy,graphs.back());
		std::cout<<"Num:"<<jentry<<"/"<<fnentries;
		fflush(stdout);
		printf("\r");

	}
	if( gBaselineCorr && !gTimeCorr )std::cout<<"--->Baseline correction: On."<<std::endl;
	if( gTimeCorr )std::cout<<"--->Time correction: On."<<std::endl;
	std::cout<<"N = "<<graphs.size()<<" traces have been loaded."<<std::endl;
	std::cout<<"complete!"<<std::endl;
	if(fSaveAsRoot)closeRoot(optTree,optfile);
	//TCanvas *mycan = new TCanvas("traces","traces",10,10,1000,1000);
	//mycan->cd(1);
	if(graphs.size()>0){
		graphs.at(0)->Draw("APL");
		graphs.at(0)->SetTitle("V1742 Traces: 200ps/Sample");
		graphs.at(0)->GetXaxis()->SetTitle("Time[ns]");
		graphs.at(0)->GetYaxis()->SetTitle("Amplitude[mV]");
		for(int i=0;i<graphs.size();i++){
			graphs.at(i)->SetLineColor(1+i%40);
			graphs.at(i)->Draw("PL");
		}
	}
}

TTree* loadFromV1742(){
	std::cout<<"--->Load data from "<<gV1742data<<std::endl;
	TFile *iptfile = new TFile(gV1742data);
	if(!iptfile->IsOpen()){
		std::cout<<"Error505!!! Can not open/find data file"<<gV1742data<<std::endl;
		return 0;
	}
	TTree *iptTree = (TTree*)iptfile->Get("UnpackxTree");
	if(!iptTree){
		std::cout<<"Error506!!! Can not find tree"<<std::endl;
		return 0;
	}
	Init(iptTree);
	return iptTree;
}

#endif
