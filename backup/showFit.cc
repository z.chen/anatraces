

double gmean[6]={0.};
double gsigma[6]={0.};
double gParLimit = 0.2;

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
	gmean[5] = fm6;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
	gsigma[5] = fs6;
}

//Error calculation
double calculateResErr(double fres, double fmean, double fmeanErr, double fsigma, double fsigmaErr){

	return fres*sqrt( pow((fmeanErr/fmean),2) + pow((fsigmaErr/fsigma),2) );
}

void showFit(TH1F *fhist){

	if(fhist==0){
		std::cout<<"empty hist."<<std::endl;
		return;

	}

	float xx[6];
	float yy[6];
	yy[0] = 5106;
	yy[1] = 5156;
	yy[2] = 5443;
	yy[3] = 5486;
	yy[4] = 5763;
	yy[5] = 5806;

	//for(int i=0;i<6;i++)gmean[i] = yy[i];

	double temm[6];
	double tems[6];

	TF1* fsum = new TF1("fsum","pol0+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)",1,20000);
	fsum->SetParameter(0,0);
	fsum->SetNpx(1000);
	fsum->SetLineColor(kBlack);
	fsum->SetLineWidth(4);
	for(int i=0;i<6;i++){
		fsum->FixParameter(3*i+2,gmean[i]);
		//fsum->SetParLimits(3*i+2,gmean[i]-gParLimit,gmean[i]+gParLimit);
		fsum->SetParameter(3*i+3,gsigma[i]);
		fsum->SetParLimits(3*i+3,gsigma[i]-gParLimit/2.,gsigma[i]+gParLimit/2.);
		temm[i] = gmean[i];
		tems[i] = gsigma[i];
	}

	fhist->SetTitle("Energy spectrum of 239Pu+241Am+244Cm alpha source");
	fhist->GetXaxis()->SetTitle("Energy[arb.]");
	fhist->GetYaxis()->SetTitle("Counts");
	fhist->Fit("fsum","R");
	double fpars[20];
	fsum->GetParameters(fpars);

	std::cout<<"1111"<<std::endl;
	//draw individually
	TF1* ff[6];
	std::cout<<"2222"<<std::endl;

	for(int i=0;i<6;i++){
		ff[i] = new TF1(TString::Format("ff%d",i),"pol0+gaus(1)",1,20000);
		ff[i]->SetParameter(0,fpars[0]);
		ff[i]->SetParameter(1,fpars[1+i*3]);
		ff[i]->SetParameter(2,fpars[2+i*3]);
		ff[i]->SetParameter(3,fpars[3+i*3]);
		std::cout<<"3333"<<std::endl;
		ff[i]->SetLineColor(1+i);
		ff[i]->SetLineStyle(9);
		ff[i]->SetNpx(1000);
		ff[i]->Draw("same");
	}

	//
	//

	//print out mean and sigma
	double fitres[20];
	fsum->GetParameters(fitres);
	const Double_t *fitresErr = fsum->GetParErrors();//par errors
													 //std::cout<<"test par1 err = "<<fitresErr[1]<<std::endl;
	Double_t meanErr[20],sigErr[20];
	double gEres[6];
	for(int i=0;i<6;i++){
		gmean[i] = fitres[3*i+2];
		gsigma[i] = fitres[3*i+3];
		meanErr[i] = fitresErr[3*i+2];
		sigErr[i] = fitresErr[3*i+3];
		gEres[i] = gsigma[i]*2.355/gmean[i]*100;//%
	}

	for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<gmean[i]<<"; with sigma = "<<gsigma[i]<<" ;deltE/E = ("<<gEres[i]<<" +/- "<<calculateResErr(gEres[i],gmean[i],meanErr[i],gsigma[i],sigErr[i])<<")%"<<std::endl;
	for(int i=0;i<6;i++){
		if(abs(temm[i] - gmean[i])>10)std::cout<<"Warning!!! Please update initial mean for peak"<<i<<" with fit result above and try it again."<<std::endl;
		if(abs(tems[i] - gsigma[i])>10)std::cout<<"Warning!!! Please update initial sigma for peak"<<i<<" with fit result above and try it again."<<std::endl;
	}

}

void showTraces(TTree *ftree){

	ftree->Draw("tracesY:tracesX>>hh(6000,-20000,40000,1000,-80,20)","","colz");
	TH1F *fhist = (TH1F*)gROOT->FindObject("hh")->Clone();
	fhist->SetTitle("scCVD-DD Traces:Recorded by high bandwidth Oscilloscope");
	fhist->GetXaxis()->SetTitle("Time[ns]");
	fhist->GetYaxis()->SetTitle("Amplitude[mV]");
	fhist->Draw("colz");
	gPad->SetLogz();

}
