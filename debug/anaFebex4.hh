//
//last updated@05 Dec,2023
//Z.Chen
//
//Error6xx.
//

//********release notes****************//
//21 Feb, 2023
//add timer to getTracesFebex4();
//some updates on raw traces display
//10 Mar, 2023
//comment those parts related to checkLastPoint();
//export raw traces to root file;(previously, only export traces after filter)
//11 Mar
//add gFebexTraceL to restrict trace length
//add isFebex
//17 Nov, 2023
//add ucesb
//subtract baseline noise
//24 Nov
//optimize tree entry reading
//02 Dec
//optimize display of running speed
//05 Dec
//optimize speed calculation; add total running time
//

#ifndef _ANAFEBEX4_HH_
#define _ANAFEBEX4_HH_

#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>

#include "viewtrace.hh"
#include "anaUcesb.hh"
#include "anaGo4.hh"


TTree* loadFromFebex4(){
	std::cout<<"--->Load data from "<<gFebex4data<<std::endl;
	TFile *iptfile = new TFile(gFebex4data);
	if(!iptfile->IsOpen()){
		std::cout<<"Error602!!! Can not open/find data file"<<gFebex4data<<std::endl;
		return 0;
	}
	TTree *iptTree = 0;
	if(gIsGo4Unpack)iptTree	= (TTree*)iptfile->Get("tree");
	if(gIsUcesbUnpack)iptTree	= (TTree*)iptfile->Get("h101");
	if(!iptTree){
		std::cout<<"Error603!!! Can not find tree"<<std::endl;
		return 0;
	}
	if(gIsGo4Unpack)InitGo4Tree(iptTree);
	if(gIsUcesbUnpack)InitUcesbTree(iptTree);
	return iptTree;
}


void testFebex4Tree(TTree* ftree){
	if (!ftree) {
		std::cout<<"Error604!!! No tree. PLease check input file"<<std::endl;
		return;
	}
	if(gIsGo4Unpack)ftree->Draw("feb4_trace_amp[0]:feb4_trace_pts[0]>>(300,0,3000,1000,0,10000)","","colz");
	if(gIsUcesbUnpack)ftree->Draw("lisa_data1traces1v:lisa_data1traces1I>>(300,0,3000,1000,0,10000)","","colz");
}

TGraph* getSingleTraceFebex4(TTree *ftree, Long64_t jentry, int fch, bool fdraw=false){
	if(gVerbose>0)std::cout<<"--->Reading single trace from event "<<jentry<<std::endl;
	if (!ftree) {
		std::cout<<"Error605!!! No tree. PLease check input file"<<std::endl;
		return 0;
	}
	isFebex=true;
	vector<double>yy;//amplitude
	vector<double>xx;//sample
	yy.clear();
	xx.clear();
	//test
	//ULong64_t nentry = ftree->GetEntriesFast(); 
	//cout<<"N entry = "<<nentry<<std::endl;
	ftree->GetEntry(jentry);
	//cout<<"febex ch = "<<gFebexCh<<std::endl;
	for(int i=0;i<gFebexTraceL;i++){
		//yy.push_back(tracesY[i]);
		//xx.push_back(tracesX[i]*10);//samples to ns
		//yy.push_back(feb4_trace_raw[gFebexCh][i]);
		//yy.push_back(feb4_trace_raw[0][i]);
		//test
		//if(yy.back()>1000)std::cout<<"yy = "<<yy.back()<<std::endl;
		//if()std::cout<<"yy = "<<yy.back()<<std::endl;
		//  **** Go4 ****   //
		if(gIsGo4Unpack)yy.push_back(feb4_trace_amp[gFebexCh][i]);
		if(gIsGo4Unpack)xx.push_back(i*10);//samples to ns
										   //  **** Ucesb ****   //
		if(gIsUcesbUnpack){
			switch(gFebexCh){
				case 0:
					if(gSubNoise){
						yy.push_back(lisa_data1traces1v[i]-lisa_data1traces3v[i]);//use ch2 to subtract baseline noise.
					}else{
						yy.push_back(lisa_data1traces1v[i]);
					}
					xx.push_back(lisa_data1traces1I[i]*10);//samples to ns
					break;
				case 1:
					yy.push_back(lisa_data1traces2v[i]);
					xx.push_back(lisa_data1traces2I[i]*10);//samples to ns
					break;
				case 2:
					yy.push_back(lisa_data1traces3v[i]);
					xx.push_back(lisa_data1traces3I[i]*10);//samples to ns
					break;
				case 3:
					yy.push_back(lisa_data1traces4v[i]);
					xx.push_back(lisa_data1traces4I[i]*10);//samples to ns
					break;
				case 4:
					yy.push_back(lisa_data1traces5v[i]);
					xx.push_back(lisa_data1traces5I[i]*10);//samples to ns
					break;
				case 5:
					yy.push_back(lisa_data1traces6v[i]);
					xx.push_back(lisa_data1traces6I[i]*10);//samples to ns
					break;
				case 6:
					yy.push_back(lisa_data1traces7v[i]);
					xx.push_back(lisa_data1traces7I[i]*10);//samples to ns
					break;
				case 7:
					yy.push_back(lisa_data1traces8v[i]);
					xx.push_back(lisa_data1traces8I[i]*10);//samples to ns
					break;
				case 8:
					yy.push_back(lisa_data1traces9v[i]);
					xx.push_back(lisa_data1traces9I[i]*10);//samples to ns
					break;
				case 9:
					yy.push_back(lisa_data1traces10v[i]);
					xx.push_back(lisa_data1traces10I[i]*10);//samples to ns
					break;
				case 10:
					yy.push_back(lisa_data1traces11v[i]);
					xx.push_back(lisa_data1traces11I[i]*10);//samples to ns
					break;
				case 11:
					yy.push_back(lisa_data1traces12v[i]);
					xx.push_back(lisa_data1traces12I[i]*10);//samples to ns
					break;
				case 12:
					yy.push_back(lisa_data1traces13v[i]);
					xx.push_back(lisa_data1traces13I[i]*10);//samples to ns
					break;
				case 13:
					yy.push_back(lisa_data1traces14v[i]);
					xx.push_back(lisa_data1traces14I[i]*10);//samples to ns
					break;
				case 14:
					yy.push_back(lisa_data1traces15v[i]);
					xx.push_back(lisa_data1traces15I[i]*10);//samples to ns
					break;
				case 15:
					yy.push_back(lisa_data1traces16v[i]);
					xx.push_back(lisa_data1traces16I[i]*10);//samples to ns
					break;
				default:
					std::cout<<"Error607!!! Febex channel number is not valid. Please check your setting file."<<std::endl;
					return 0;
			}
		}
	}
	//std::cout<<"Max num of sample = "<<1024<<std::endl;
	TGraph *grtem = new TGraph(yy.size(),&xx[0],&yy[0]);
	if(fdraw)grtem->Draw("AP*");
	grtem->SetTitle(Form("Febex4_Ch%d: 10ns/Sample",0));
	grtem->GetXaxis()->SetTitle("Samples");
	grtem->GetYaxis()->SetTitle("Amplitude[arb.]");
	return grtem;
}

void getTracesFebex4(TTree* ftree){
	if (!ftree) {
		std::cout<<"Error606!!! No tree. PLease check input file"<<std::endl;
		return;
	}

	isFebex=true;
	vector<TGraph*>graphs;
	graphs.clear();
	gCFDtime.clear();
	gRawE.clear();
	gRawTraces.clear();
	double rawE_tem = -1;
	TGraph *rawTraces_tem = 0;


	if(fSaveAsRoot)initRoot();

	std::cout<<"--->Read from Febex channel(0-15): "<<gFebexCh<<std::endl;
	if(fsaveTraces && fSaveAsRoot)std::cout<<"--->Export traces to a new root file. "<<std::endl;
	if(fSaveAsRoot)std::cout<<"--->Export results to a new root file: "<<gOptRootFileName<<std::endl;
	//Long64_t fnentries = 20;
	Long64_t fnentries = ftree->GetEntriesFast();
	std::cout<<"--->Total events: "<<std::endl;
	std::cout<<fnentries<<std::endl;
	if(gTreeAllEntry)std::cout<<"--->Read all entries: enable"<<std::endl;
	if(!gTreeAllEntry)std::cout<<"--->Read entries from:"<<gTreeEntry[0]<<" to "<<gTreeEntry[1]<<std::endl;
	TGraph *fgrsin=0;
	TStopwatch timer;
	TStopwatch timer2;//start before the loop; to calculate total running time
	vector<float>v_averageSpeed;
	v_averageSpeed.clear();

	for(Long64_t jentry=gTreeEntry[0];jentry<fnentries;jentry++){
		//stopwatch
		timer.Reset();
		timer.Start();
		//clear;11 Dec, 2023
		//gRawE[jentry],fEnergy,gCFDtime.back(),gRawTraces.back()
		//gRawE.clear();
		//gRawTraces.clear();
		//gCFDtime.clear();
		//fEnergy = -1;
		rawE_tem = -1;
		rawTraces_tem = 0;

		//if(jentry<gTreeEntry[0])continue;
		if(getSingleTraceFebex4(ftree,jentry,false) == 0)continue;
		if(gTimeCorr){
			fgrsin	= timeDriftCorr( baselineCorr(getSingleTraceFebex4(ftree,jentry,false),false),false );
		}else if(gBaselineCorr && !gTimeCorr){
			fgrsin = baselineCorr(getSingleTraceFebex4(ftree,jentry,false),false);
			//std::cout<<"lalal"<<std::endl;
		}
		else{
			fgrsin = getSingleTraceFebex4(ftree,jentry,false);
		}
		if(!fgrsin)continue;

		//*****get time info*****//
		CFD(fgrsin,false);
		//std::cout<<"cfd = "<<gCFDtime.back()<<std::endl;
		//*** save raw traces to a vector  ***//
		rawTraces_tem = fgrsin;
		//gRawTraces.push_back(fgrsin);
		if(jentry%6000==0){
			graphs.clear();//buffer protection;
			std::cout<<"--->Current limit: "<<6000<<". Graph is full. Empty graph."<<std::endl;}
		if(gIntegral){
			//****************** determine energy**************//
			graphs.push_back(fgrsin);rawE_tem = Integrate(graphs.back());
		}
		else if(gMWD){
			//if(checkLastPoint(MWD(fgrsin))){
			graphs.push_back(MWD(fgrsin));
			rawE_tem = getEdg3(graphs.back());
			//gRawE[jentry] = getEdg2(graphs.back());
			//gRawE[jentry] = getEdg(graphs.back());
			if(rawE_tem<0)rawE_tem*=(-1);
			//}
		}else{
			std::cout<<"Warning607!!! Please select energy calculation method."<<std::endl;return;

		}
		//std::cout<<"***energy = "<<gRawE[jentry]<<std::endl;
		//gRawE[jentry] =  Integrate(graphs.back()) ;//map<int,double>;calculate energy
		double fEnergy = gCalibPar[0] + rawE_tem * gCalibPar[1];//calibration
		Long64_t fts=-1;
		//timer.Stop();
		double fspeed = timer.RealTime();
		v_averageSpeed.push_back(1.0/fspeed);
		double ftimeToGo = 0;
		if(gTreeAllEntry){
			ftimeToGo = (fnentries-jentry)*fspeed;
		}else{
			ftimeToGo = (gTreeEntry[1]-jentry)*fspeed;
		}
		//if(fSaveAsRoot)saveAsRoot(optTree,jentry,fts,gRawE[jentry],fEnergy,graphs.back());
		gRawE[jentry] = rawE_tem;
		gRawTraces.push_back(rawTraces_tem);
		if(fSaveAsRoot)saveAsRoot(optTree,jentry,fts,rawE_tem,fEnergy,gCFDtime.back(),rawTraces_tem);
		if(jentry%100==0)std::cout<<"--->Current speed: "<<1.0/fspeed<<"[evt/s]; Num:"<<jentry<<"/"<<fnentries<<"  "<<ftimeToGo<<"s to go.";
		fflush(stdout);
		printf("\r");

		if(jentry>=gTreeEntry[1] && !gTreeAllEntry)break;
	}
	//float faverageSpeed = std::reduce(std::execution::par, v_averageSpeed.begin(),v_averageSpeed.end())/v_averageSpeed.size();//c++17
	gAverageSpeed = std::accumulate(v_averageSpeed.begin(),v_averageSpeed.end(),0.0)/v_averageSpeed.size();
	gTotalRunningTime = timer2.RealTime();
	std::cout<<std::endl;
	std::cout<<"--->Total running time: "<<gTotalRunningTime<<" s."<<std::endl;
	std::cout<<"--->Average speed     : "<<gAverageSpeed<<" [evt/s]."<<std::endl;
	if( gBaselineCorr && !gTimeCorr )std::cout<<"--->Baseline correction: On."<<std::endl;
	if( gTimeCorr )std::cout<<"--->Time correction: On."<<std::endl;
	std::cout<<"N = "<<graphs.size()<<" trapez have been added to TGraphs."<<std::endl;
	std::cout<<"complete!"<<std::endl;
	if(fSaveAsRoot)closeRoot(optTree,optfile);
	//TCanvas *mycan = new TCanvas("traces","traces",10,10,1000,1000);
	//mycan->cd(1);
	if(graphs.size()>0){
		graphs.at(0)->Draw("APL");
		graphs.at(0)->SetTitle("Febex Traces: 10ns/Sample");
		graphs.at(0)->GetXaxis()->SetTitle("Time[ns]");
		graphs.at(0)->GetYaxis()->SetTitle("Amplitude[arb.]");
		graphs.at(0)->SetMaximum(gTraceAmp[1]);
		graphs.at(0)->SetMinimum(gTraceAmp[0]);
		for(int i=0;i<graphs.size();i++){
			graphs.at(i)->SetLineColor(1+i%40);
			graphs.at(i)->Draw("PL");
		}
	}
}

#endif
