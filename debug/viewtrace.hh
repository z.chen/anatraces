//
//last updated@ 06 Dec,2023
//Z.Chen
//Email: z.chen@gsi.de
//
//Error4xx
//*********************************************************************************//
//*********************************release notes***********************************//
//15Jan,2023
//add "welcome()" and "anaHelp()"
//16Jan,2023
//add uncertainty calculation for resolution; only consider uncertainty from fit
//22Jan,2023
//some corrections are added to uncertainty calculation
//some updates on energy calibration
//29Jan,2023
//some updates related to trapez
//10 Feb,2023
//some updates related to febex4
//21 Feb
//some updates related to display
//06 Mar, 2023
//some updates on energy calibration; now the max of peaks for calib is 6
//10 Mar, 2023
//comment those parts related to checkLastPoint();
//11 Mar
//add gFebexTraceL to restrict trace length
//23 Mar
//some updates on baselineCorr()
//25 Mar
//Some updates on fit display
//simplify getEnergyResolution();
//17 Apr, 2023
//optimize data loading
//12 Nov, 2023
//some updates on baselineCorr(); add 200 fake points(y==0) at the beginning of the baseline for MWD analysis.
//some updates on MWD(): change moving window starting point to fit the newly added 200 fake points
//17 Nov, 2023
//add ucesb
//subtract baseline noise
//22 Nov,
//fix typo on Scope.data.runnumber
//26 Nov
//update energy calibration
//02 Dec
//add T_cfd
//06 Dec
//add random number to T_cfd
//
//**********************************************************************************//

#ifndef _VIEWTRACE_HH_
#define _VIEWTRACE_HH_

#include "Env.hh"
#include "TEnv.h"

inline void welcome(){

	std::cout<<"****************************************************"<<std::endl;
	std::cout<<"**             Welcome to anaTraces.              **"<<std::endl;
	std::cout<<"** This toolkit has been tested under ROOT6.24.   **"<<std::endl;
	std::cout<<"** A brief introduction of how to perform simple  **"<<std::endl;
	std::cout<<"** analysis with this toolkit can be called by    **"<<std::endl;
	std::cout<<"** entering 'anaHelp()' in your ROOT interface.   **"<<std::endl;
	std::cout<<"** e.g. root [1] anaHelp()                        **"<<std::endl;
	std::cout<<"** To generate a hotstart, please run             **"<<std::endl;
	std::cout<<"** root [1] createHotstart()                      **"<<std::endl;
	std::cout<<"****************************************************"<<std::endl;
}

inline void anaHelp(){

	std::cout<<"****************************************************"<<std::endl;
	std::cout<<"**             Welcome to anaHelp().              **"<<std::endl;
	std::cout<<"** 1. Scope data analysis.                        **"<<std::endl;
	std::cout<<"** 1.1. Two major functions:                      **"<<std::endl;
	std::cout<<"** 1.1.1 single trace analysis:                   **"<<std::endl;
	std::cout<<"** anaTxt_checkSingleTrace(ID_trace)              **"<<std::endl;
	std::cout<<"** 1.1.2 traces and energy spectrum analysis:     **"<<std::endl;
	std::cout<<"** anaTxt_checkTraces()                           **"<<std::endl;
	std::cout<<"** 1.2. General steps:                            **"<<std::endl;
	std::cout<<"** st1. Check if you have 'setting.set' file;     **"<<std::endl;
	std::cout<<"** if not, downlad from gitLab.                   **"<<std::endl;
	std::cout<<"** st2. Adjust all the parameters and paths in    **"<<std::endl;
	std::cout<<"** your 'setting.set' file.                       **"<<std::endl;
	std::cout<<"** st3. root -l Viewtrace.cc                      **"<<std::endl;
	std::cout<<"** st4. setSettingFile('SETTING_FILE_NAME')       **"<<std::endl;
	std::cout<<"** st5. anaTxt_checkSingleTrace(ID_trace)         **"<<std::endl;
	std::cout<<"** st6. anaTxt_checkTraces()                      **"<<std::endl;
	std::cout<<"** st7. setMeans(MEAN1,MEAN2,MEAN3)               **"<<std::endl;
	std::cout<<"** st8. setSigmas(SIGMA1,S2,S3)                   **"<<std::endl;
	std::cout<<"** st9. energyCalib()                             **"<<std::endl;
	std::cout<<"** st10. energyCalib(1)                           **"<<std::endl;
	std::cout<<"** st11. enableSaveTraces()                       **"<<std::endl;
	std::cout<<"** st12. enableSaveAsRoot()                       **"<<std::endl;
	std::cout<<"** st13. exportParameters()                       **"<<std::endl;
	std::cout<<"** st14. anaTxt_checkTraces()                     **"<<std::endl;
	std::cout<<"****************************************************"<<std::endl;
	std::cout<<"** 2. V1742 data analysis.                        **"<<std::endl;
	std::cout<<"** Please refer to 1.Scope data analysis.         **"<<std::endl;
	std::cout<<"** Using 'anaV1742_checkSingleTrace()'            **"<<std::endl;
	std::cout<<"** and 'anaV1742_checkTraces()' instead.          **"<<std::endl;
}

inline bool loadSettings(){
	if(!isOpen(gSettings)){
		std::cout<<"Error401!!! Can not find/open setting file "<<gSettings<<std::endl;
		return false;
	}
	if(gcnt_loadSetting>0)return true;
	std::cout<<"--->Load "<<gSettings<<std::endl;
	TEnv env(gSettings);
	gV1742data = env.GetValue("V1742.data.filename","../V1742/output_0018.root");
	gFebex4data = env.GetValue("Febex4.data.filename","./LISA_root_tree.root");
	gTxtDir = env.GetValue("Scope.data.directory","../fD_fS_3.5V/");
	gTsfilename = env.GetValue("Scope.timestamp.filename","timestamp.log");
	gSettings_rename = env.GetValue("New.setting.file.name","settings_Febex_export.set");
	gIsGo4Unpack = env.GetValue("Unpack.with.go4",false);
	gIsUcesbUnpack = env.GetValue("Unpack.with.ucesb",false);
	gSubNoise = env.GetValue("Subtract.noise.with.baseline",false);
	gTreeAllEntry = env.GetValue("Enable.read.all.entry",false);
	gRun[0] = env.GetValue("Scope.data.runnumber.0",0);
	gRun[1] = env.GetValue("Scope.data.runnumber.1",2000);
	gTreeEntry[0] = env.GetValue("TreeEntry.read.from.0",0);
	gTreeEntry[1] = env.GetValue("TreeEntry.read.from.1",2000);
	gVerbose = env.GetValue("Verbose",0);
	gSampleRate = env.GetValue("Sample.rate[MHz]",62.5);
	gFebexCh = env.GetValue("Febex4.read.from.ch",0);
	gFebexTraceL = env.GetValue("Febex4.trace.length",2000);
	window[0] = env.GetValue("Integral.window.0",-0.2);
	window[1] = env.GetValue("Integral.window.1",2.);
	gScopeDataLoading[0] = env.GetValue("Scope.Data.From.0",10000);
	gScopeDataLoading[1] = env.GetValue("Scope.Data.From.1",20000);
	gtrapezSampleWindow[0] = env.GetValue("Trapez.Sample.window.0",700);
	gtrapezSampleWindow[1] = env.GetValue("Trapez.Sample.window.1",1500);
	gTZAmp[0] = env.GetValue("Trapez.Amp.Calc.window.0",-200);
	gTZAmp[1] = env.GetValue("Trapez.Amp.Calc.window.1",200);
	gtrapezMovingWindowLength = env.GetValue("Trapez.Moving.Window.Length[ns]",3200);
	gRisingTime = env.GetValue("Rising.time[ns]",100);
	gDecaytime = env.GetValue("Decaytime[ns]",48000);
	gNbins = env.GetValue("Histogram.bins",4000);
	gParLimit = env.GetValue("Fit.Pars.Limit",12.0);
	gIntegral = env.GetValue("Enable.energyCalculation.integral",false);
	gMWD = env.GetValue("Enable.energyCalculation.algorithm",false);
	ghMIN = env.GetValue("Energy.histogram.range.0",0);
	ghMAX = env.GetValue("Energy.histogram.range.1",10000);
	ghTMIN = env.GetValue("Time.histogram.range.0",-2000);
	ghTMAX = env.GetValue("Time.histogram.range.1",2000);
	gESrange[0] = env.GetValue("Energy.spectrum.range.0",0);
	gESrange[1] = env.GetValue("Energy.spectrum.range.1",20000);
	gTSrange[0] = env.GetValue("Time.spectrum.range.0",-200.);
	gTSrange[1] = env.GetValue("Time.spectrum.range.1",200.);
	gTraceAmp[0] = env.GetValue("Trace.graph.Y.min",-2000);
	gTraceAmp[1] = env.GetValue("Trace.graph.Y.max",2000);
	gBaselineCorr = env.GetValue("Enable.baseline.correction",false);
	gTimeCorr = env.GetValue("Enable.time.correction",false);
	gCFDref = env.GetValue("CFD.reference",0.0);
	gCFDwin[0] = env.GetValue("CFD.window.0",140.);
	gCFDwin[1] = env.GetValue("CFD.window.1",160.);
	gFra = env.GetValue("CFD.pars.fraction",0.9);
	gDel = env.GetValue("CFD.pars.delay",1.2);
	gCalibPar[0] = env.GetValue("Energy.calibration.par.bias",0.0);
	gCalibPar[1] = env.GetValue("Energy.calibration.par.gradient",1.0);
	gmean[0] = env.GetValue("Mean.value.0",11141.);
	gmean[1] = env.GetValue("Mean.value.1",11887.);
	gmean[2] = env.GetValue("Mean.value.2",12573.);
	gmean[3] = env.GetValue("Mean.value.3",12573.);
	gmean[4] = env.GetValue("Mean.value.4",12573.);
	gmean[5] = env.GetValue("Mean.value.5",12573.);
	gsigma[0] = env.GetValue("Sigma.value.0",173.);
	gsigma[1] = env.GetValue("Sigma.value.1",156.);
	gsigma[2] = env.GetValue("Sigma.value.2",137.);
	gsigma[3] = env.GetValue("Sigma.value.3",137.);
	gsigma[4] = env.GetValue("Sigma.value.4",137.);
	gsigma[5] = env.GetValue("Sigma.value.5",137.);
	gamp[0] = env.GetValue("Amplitude.value.0",70.);
	gamp[1] = env.GetValue("Amplitude.value.1",70.);
	gamp[2] = env.GetValue("Amplitude.value.2",70.);
	gamp[3] = env.GetValue("Amplitude.value.3",70.);
	gamp[4] = env.GetValue("Amplitude.value.4",70.);
	gamp[5] = env.GetValue("Amplitude.value.5",70.);
	gOptRootFileName = env.GetValue("SaveAsRoot.filename","sCVD.root");
	fsaveTraces = env.GetValue("Enable.save.traces",false);
	fSaveAsRoot = env.GetValue("Enable.save.as.root",false);

	std::cout<<"--->settings loaded!"<<std::endl;
	std::cout<<"--->Start a new recoding..."<<std::endl;
	TTimeStamp ts;
	optPar<<"-----------Start a new recording-----------"<<std::endl;
	optPar<<ts.AsString("l")<<std::endl;
	optPar<<std::endl;
	optPar<<std::endl;
	gcnt_loadSetting++;
	return true;
}

//Error calculation
double calculateResErr(double fres, double fmean, double fmeanErr, double fsigma, double fsigmaErr){

	return fres*sqrt( pow((fmeanErr/fmean),2) + pow((fsigmaErr/fsigma),2) );
}

//NIMA 58(2):253(1968)
//delay ~ 1/4 * total trace width
//fraction ~ 0.9
TGraph* CFD(TGraph *fgr,bool fdraw=false){
	if(gVerbose>0)std::cout<<"--->Time discrimination with CFD."<<std::endl;
	int length = 0;
	//std::cout<<"lalala"<<std::endl;
	//std::cout<<"verbose  = "<<gVerbose<<std::endl;
	if(fgr == 0 || fgr->GetN() < 1){
		std::cout<<"Error402. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;
	vector<double>yyf;//with fraction
	vector<double>xxd;//with delay
	vector<double>yyCFD;//after CFD
	xx.clear();
	yy.clear();
	yyf.clear();
	xxd.clear();
	yyCFD.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(-y);
		yyf.push_back(y*gFra);
		xxd.push_back(x+gDel);
	}
	TGraph *ftem1 = new TGraph(xx.size(),&xx[0],&yyf[0]);
	//TGraph *fMWD = new TGraph(MWD.size(),&MWDx[0],&MWD[0]);
	for(int i=0;i<length;i++){
		double ytem=0;
		ytem = ftem1->Eval(xxd[i]);
		yyCFD.push_back(ytem+yy[i]);
	}
	TGraph *ftem2 = new TGraph(xxd.size(),&xxd[0],&yyCFD[0]);
	ftem2->SetTitle("Constant-fraction discrimination(CFD)");
	ftem2->GetXaxis()->SetTitle("Time[ns]");
	if(fdraw)ftem2->Draw("AP*");
	//calculate zero
	double ymax_y,ymax_x,ymin_y,ymin_x;
	ymax_y = ymin_y = 0;
	ymax_x = ymin_x = -9999;
	double fx1,fx2;
	fx1 = fx2 = -9999;
	for(int i=0;i<length;i++){
		if(xxd[i]>gCFDwin[0] && xxd[i]<gCFDwin[1]){
			if(yyCFD[i]>ymax_y){
				ymax_y = yyCFD[i];
				ymax_x = xxd[i];
			}
			if(yyCFD[i]<ymin_y){
				ymin_y = yyCFD[i];
				ymin_x = xxd[i];
			}
		}
	}
	fx1 = ymax_x;
	fx2 = ymin_x;
	//swap
	if(fx2<fx1){
		double tem = fx1;
		fx1 = fx2;
		fx2 = tem;
	}
	vector<double>xxz;//to calculate zero
	vector<double>yyz;//to calculate zero
	xxz.clear();
	yyz.clear();
	//TRandom3 *frd = new TRandom3();
	for(int i=0;i<length;i++){
		if(xxd[i]>fx1 && xxd[i]<fx2){
			xxz.push_back(yyCFD[i]);
			yyz.push_back(xxd[i]);//int time related to the sample rate;
			//yyz.push_back(xxd[i]+frd->Uniform(-1,1));//float time which is nice for display;
		}
	}
	TGraph *ftem3 = new TGraph(xxz.size(),&xxz[0],&yyz[0]);
	//std::cout<<"Eval0 "<<ftem3->Eval(0)<<std::endl;
	//std::cout<<"Uniform: "<<gRandom->Uniform(-0.5,0.5)<<std::endl;
	gCFDtime.push_back(ftem3->Eval(0,0)+gRandom->Uniform(-5,5));//use spline interpolation to get the "0" point;float time
	if(gVerbose>0)std::cout<<"--->CFD result: zero point @ "<<gCFDtime.back()<<" ns."<<std::endl;
	//delete frd;
	//frd=NULL;
	return ftem2;
}

TGraph* baselineCorr(TGraph *fgr, bool plot=false){

	int length=0;
	if(fgr==0 || fgr->GetN()<1 ){
		std::cout<<"Error403. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double *fxx = fgr->GetX();
	double *fyy = fgr->GetY();
	//update, 12 Nov, 2023
	//const int fcnt=200;//add 200 fake points
	//double fxx2[length+fcnt];
	//double fyy2[length+fcnt];

	//double fshift = 0;
	//first 100 points
	//updates:only use 10 points
	//for(int i=20;i<30;i++){
	//	fshift+=fyy[i];
	//}
	//for(int i = 0;i<length;i++){
	//	fyy[i] -= fshift/10.0;
	//	fyy2[i] = fyy[i];
	//	fxx2[i] = fxx[i];
	//}
	//update, 12 Nov, 2023
	/*
	for(int ii=0;ii<fcnt;ii++){
		fxx2[ii+length] = (ii-fcnt+1)*10;
		fyy2[ii+length] = fyy[ii%30];//fake points are taken from the original first 30 points
	}
	*/
	//length+=fcnt;
	//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
	//TGraph *fbaseGr2 = new TGraph(length,fxx2,fyy2);
	TGraph *fbaseGr2 = new TGraph(length,fxx,fyy);
	fbaseGr2->SetTitle("Trace: With baseline correction");
	fbaseGr2->GetXaxis()->SetTitle("Time[ns]");
	fbaseGr2->GetYaxis()->SetTitle("Amplitude[mV]");
	//std::cout<<"*With Baseline correction."<<std::endl;
	if(plot)fbaseGr2->Draw("AP*");
	return fbaseGr2;
}

TGraph* timeDriftCorr(TGraph* fgr,bool fdraw=false){
	if(gVerbose>0)std::cout<<"--->start time drift corr"<<std::endl;
	int length=0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error404. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	CFD(fgr,false);
	//std::cout<<"CFD time test: "<<gCFDtime.back()<<std::endl;
	float fdelt = gCFDtime.back()-gCFDref;
	//std::cout<<"CFD time test1: "<<gCFDtime.back()<<std::endl;
	gCFDtime.pop_back();
	//std::cout<<"CFD time test2: "<<gCFDtime.back()<<std::endl;
	vector<double>xx;
	vector<double>yy;
	xx.clear();
	yy.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x-fdelt);
		yy.push_back(y);
	}

	TGraph *ftem1 = new TGraph(xx.size(),&xx[0],&yy[0]);
	if(fdraw)ftem1->Draw("AP*");
	ftem1->GetXaxis()->SetTitle("Time[ns]");
	ftem1->GetYaxis()->SetTitle("Amplitude[mV]");
	return ftem1;
}

void showRawTraces(){
	if(gRawTraces.size()<1){
		std::cout<<"Error415! No raw trace data, please check loaded traces."<<std::endl;
		return;
	}
	std::cout<<gRawTraces.size()<<" raw traces loaded."<<std::endl;
	int cnt=0;
	if(gRawTraces.size()>0){
		gRawTraces.at(0)->Draw("APL");
		gRawTraces.at(0)->SetTitle(TString::Format("Raw Traces_%dns/Simple",(int)(1000/gSampleRate) ) );
		gRawTraces.at(0)->GetXaxis()->SetTitle("Time[ns]");
		gRawTraces.at(0)->GetYaxis()->SetTitle("Amplitude[arb.]");
		gRawTraces.at(0)->SetMaximum(gTraceAmp[1]);
		gRawTraces.at(0)->SetMinimum(gTraceAmp[0]);
		for(int i=0;i<gRawTraces.size();i++){
			gRawTraces.at(i)->SetLineColor(1+i%40);
			gRawTraces.at(i)->Draw("PL");
			if(i>=6000)break;
			cnt++;
		}
	std::cout<<cnt<<" raw traces are added to TGraph."<<std::endl;
	}

	std::cout<<"***********************"<<std::endl;
}

void showEnergySpectrum(){
	if(gRawE.size()<1){
		std::cout<<"Error405! No energy data, please check loaded traces."<<std::endl;
		return;
	}
	//std::cout<<gRawE.size()<<" traces loaded."<<std::endl;
	//TCanvas *EScan = new TCanvas();
	TH1F* fhES = new TH1F("Energy spectrum","Energy spectra",gNbins,ghMIN,ghMAX);
	for(map<int,double>::iterator iter = gRawE.begin();iter!=gRawE.end();++iter)fhES->Fill( gCalibPar[0] + iter->second * gCalibPar[1]);
	fhES->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);
	if(gCalibPar[1]==1){
		fhES->GetXaxis()->SetTitle("Without calibration[arb.]");
	}
	else{
		fhES->GetXaxis()->SetTitle("After calibration[keV]");
	}
	fhES->SetTitle("Energy Spectrum");
	fhES->Draw();
	// ***** FAIR style ************//
	//setFAIRstyle(fhES);
	//fhES->SetFillColor(796);
	//fhES->SetFillStyle(1001);
	//fhES->SetLineColor(kBlack);
	//fhES->SetLineWidth(1);
	ghist = (TH1F*)fhES->Clone();
	std::cout<<"*To adjust display range on energy spectrum, please run 'setEnergySpecRange(range1,range2)'and 'showEnergySpectrum()',respectively."<<std::endl;
	std::cout<<"***********************"<<std::endl;
}

void showTimeSpectrum(){
	if(gCFDtime.size()<1){
		std::cout<<"Error406! No time data, please check time calculation."<<std::endl;
		return;
	}
	std::cout<<"Time info of "<<gCFDtime.size()<<" traces has been calculated."<<std::endl;
	TH1F* fhTS = new TH1F("Time spectrum","Time spectum",gNbins,ghTMIN,ghTMAX);
	for(int i=0;i<gCFDtime.size();i++)fhTS->Fill(gCFDtime[i]);
	fhTS->GetXaxis()->SetRangeUser(gTSrange[0],gTSrange[1]);
	fhTS->GetXaxis()->SetTitle("Time[ns]");
	fhTS->SetTitle("Time Spectrum");
	fhTS->Draw();
}

double Integrate(TGraph* fgr){

	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error407. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double sum =0;
	double bkg=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(x>window[0] && x<window[1])sum+=y;
		//if(gBaselineCorr && x>bwindow[0] && x<bwindow[1])bkg+=y;
	}
	/*
	   if(gBaselineCorr){
	   double ratio = 1.0*(window[1]-window[0])/(bwindow[1]-bwindow[0]);
	   std::cout<<"ratio = "<<ratio<<"; bkg = "<<bkg<<std::endl;
	   sum = sum - bkg*ratio;
	   }
	   */
	if(sum<0)sum = -sum;
	//std::cout<<"Energy = "<<sum<<std::endl;
	return sum;
}
//find edge(max/min) within the given window
double getEdg(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error408. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double xtem,ytem;
	xtem=-9999;
	ytem=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(gTZAmp[0] > gTZAmp[1]){
			std::cout<<"Error409. Please check trapez amp calc window."<<std::endl;
			return 0;
		}
		if(x>gTZAmp[0] && x<gTZAmp[1] && ytem>y){
			xtem = x;
			ytem = y;
			//std::cout<<"here!"<<std::endl;
		}
	}
	//std::cout<<"min = "<<ytem<<" @ x = "<<xtem<<std::endl;
	return ytem;
}

//find edge within the given window by gradient
//center of the rise edge must be around 0!!!
double getEdg2(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error408. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double fx[3]={-9999,-9999,-9999};
	double fy[3]={-9999,-9999,-9999};
	double fgrad[2] = {-9999,-9999};
	double dgrad = -9999;
	double xtem,ytem;
	xtem=-9999;
	ytem=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(gTZAmp[0] > gTZAmp[1]){
			std::cout<<"Error409. Please check trapez amp calc window."<<std::endl;
			return 0;
		}
		if(x>gTZAmp[0] && x<gTZAmp[1]){
			fx[0]=fx[1];
			fx[1]=fx[2];
			fx[2]=x;
			fy[0]=fy[1];
			fy[1]=fy[2];
			fy[2]=y;
			if(fx[0]!=-9999 && fx[1]!=-9999 && fx[2]!=-9999){
				fgrad[0] = (fy[1]-fy[0])/(fx[1]-fx[0]);
				fgrad[1] = (fy[2]-fy[1])/(fx[2]-fx[1]);
				dgrad = fgrad[1] - fgrad[0];
				//std::cout<<"gradient0= "<<fgrad[0]<<" @ x = "<<fx[0]<<std::endl;
				//std::cout<<"gradient1= "<<fgrad[1]<<" @ x = "<<fx[1]<<std::endl;
				//std::cout<<"delta = "<<dgrad<<std::endl;
				if(fx[1]>0 && abs(fgrad[1])<0.1 && dgrad>0.1)
				{
					xtem = fx[1];
					ytem = fy[1];
				}
			}
		}
	}
	//std::cout<<"min = "<<ytem<<" @ x = "<<xtem<<std::endl;
	return ytem;
}

//find amplitude within the given window by average
double getEdg3(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error408. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double ytem;
	ytem=0;
	int cnt=0;
	double fsum=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(gTZAmp[0] > gTZAmp[1]){
			std::cout<<"Error409. Please check trapez amp calc window."<<std::endl;
			return 0;
		}
		if(x>gTZAmp[0] && x<gTZAmp[1]){//ns
									   //std::cout<<"*** x and y"<<x<<" "<<y<<std::endl;
			fsum+=y;
			cnt++;
		}
	}
	if(gVerbose>4)std::cout<<"min = "<<fsum/cnt<<std::endl;
	return ytem=fsum/cnt;
}

bool checkLastPoint(TGraph *fgr){
	int length = fgr->GetN();
	double *fxx = fgr->GetX();
	double *fyy = fgr->GetY();
	//std::cout<<"last point = "<<fyy[length-1]<<" "<<fxx[length-1]<<std::endl;
	bool fcheck = false;
	if(fyy[length-1]<100) fcheck = true;
	return fcheck;
}
/*
void energyCalib(int nCal = 3, bool fSAVE = false){

	showEnergySpectrum();
	ghist->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);
	float xx[6];
	float yy[6];
	if(nCal==3){
		yy[0]= 5156;
		yy[1]= 5486;
		yy[2]= 5806;
	}
	else if(nCal == 6){
		yy[0] = 5106;
		yy[1] = 5156;
		yy[2] = 5443;
		yy[3] = 5486;
		yy[4] = 5763;
		yy[5] = 5806;
	}
	else{
		std::cout<<"Error416!!! nCal = illegal value. Only 3-peaks OR 6-peaks calibration is allowed now."<<std::endl;
		return;
	}
	//float fpeak[6] = {5106,5156,5443,5486,5763,5806};
	//int flimit = 12;//old version;now is global gParLimit
	double temm[6];
	double tems[6];

	TCanvas *cCalib = new TCanvas("Energy calibration","Energy calibration",10,40,900,900);
	cCalib->Divide(2,2);
	cCalib->cd(1);
	ghist->Draw();
	TH1F *hcalib = (TH1F*)ghist->Clone();
	cCalib->cd(2);
	hcalib->Draw();
	hcalib->SetTitle("Fit");
	//gaus fit
	TF1 *fsum;
	vector<char*> fitFuncs;
	fitFuncs.clear();
	fitFuncs.push_back((char*)"pol0+gaus(1)+gaus(4)+gaus(7)");
	fitFuncs.push_back((char*)"pol0+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)");
	fsum = new TF1("fsum",fitFuncs[nCal/3-1],1,20000);
	for(int i=0;i<nCal;i++){
		fsum->SetParameter(3*i+2,gmean[i]);
		fsum->SetParLimits(3*i+2,gmean[i]-gParLimit,gmean[i]+gParLimit);
		fsum->SetParameter(3*i+3,gsigma[i]);
		fsum->SetParLimits(3*i+3,gsigma[i]-gParLimit,gsigma[i]+gParLimit);
		temm[i] = gmean[i];
		tems[i] = gsigma[i];
	}
	hcalib->Fit("fsum","R");

	//print out mean and sigma
	double fitres[20];
	fsum->GetParameters(fitres);
	const Double_t *fitresErr = fsum->GetParErrors();//par errors
													 //std::cout<<"test par1 err = "<<fitresErr[1]<<std::endl;
	Double_t meanErr[20],sigErr[20];
	for(int i=0;i<nCal;i++){
		gamp[i] = fitres[3*i+1];
		gmean[i] = fitres[3*i+2];
		gsigma[i] = fitres[3*i+3];
		meanErr[i] = fitresErr[3*i+2];
		sigErr[i] = fitresErr[3*i+3];
		//std::cout<<"test "<<meanErr[i]<<std::endl;
		gEres[i] = gsigma[i]*2.355/gmean[i]*100;//%
		xx[i]=gmean[i];
	}
	std::cout<<"Initialized/Expected mean and sigma:"<<std::endl;
	if(nCal==3)for(int i=0;i<3;i++)std::cout<<"peak"<<i<<" = "<<temm[i]<<"; with sigma = "<<tems[i]<<std::endl;
	if(nCal==6)for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<temm[i]<<"; with sigma = "<<tems[i]<<std::endl;
	std::cout<<"*Limit for mean and sigma fit now is "<<gParLimit<<"."<<std::endl;
	std::cout<<"*To adjust this limit, please run 'setFitParLimit(NEW_VALUE)'and 'energyCalib()',respectively.."<<std::endl;
	std::cout<<"Fit results(from left to right;only errors of fit pars were considered):"<<std::endl;
	if(nCal==3)for(int i=0;i<3;i++)std::cout<<"peak"<<i<<" = "<<gmean[i]<<"; with sigma = "<<gsigma[i]<<" ;deltE/E = ("<<gEres[i]<<" +/- "<<calculateResErr(gEres[i],gmean[i],meanErr[i],gsigma[i],sigErr[i])<<")%"<<std::endl;
	if(nCal==3)for(int i=0;i<3;i++){
		if(abs(temm[i] - gmean[i])>10)std::cout<<"Warning!!! Please update initial mean for peak"<<i<<" with fit result above and try it again."<<std::endl;
		if(abs(tems[i] - gsigma[i])>10)std::cout<<"Warning!!! Please update initial sigma for peak"<<i<<" with fit result above and try it again."<<std::endl;
	}
	if(nCal==6)for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<gmean[i]<<"; with sigma = "<<gsigma[i]<<" ;deltE/E = ("<<gEres[i]<<" +/- "<<calculateResErr(gEres[i],gmean[i],meanErr[i],gsigma[i],sigErr[i])<<")%"<<std::endl;
	if(nCal==6)for(int i=0;i<6;i++){
		if(abs(temm[i] - gmean[i])>10)std::cout<<"Warning!!! Please update initial mean for peak"<<i<<" with fit result above and try it again."<<std::endl;
		if(abs(tems[i] - gsigma[i])>10)std::cout<<"Warning!!! Please update initial sigma for peak"<<i<<" with fit result above and try it again."<<std::endl;
	}
	//calibration parameters
	cCalib->cd(3);
	TGraph *fCalib;
	if(nCal==3)fCalib = new TGraph(3,xx,yy);
	if(nCal==6)fCalib = new TGraph(6,xx,yy);
	fCalib->SetTitle("Calibration");
	fCalib->GetYaxis()->SetTitle("Energy[keV]");
	fCalib->GetXaxis()->SetTitle("Integral");
	fCalib->SetMarkerSize(2);
	fCalib->Draw("AP*");
	TFitResultPtr fpar = fCalib->Fit("pol1","S");
	if(fSAVE){
		gCalibPar[0] = fpar->Parameter(0);
		gCalibPar[1] = fpar->Parameter(1);
		std::cout<<"energy calibration parameters updated."<<std::endl;
	}
	std::cout<<"Fit result:"<<std::endl;
	std::cout<<"par0 = "<<gCalibPar[0]<<std::endl;
	std::cout<<"par1 = "<<gCalibPar[1]<<std::endl;
	std::cout<<"Calibration complete!"<<std::endl;
}
*/
void energyCalib(const int npoints=6, bool fSAVE = false){
	//setCalibEnergies();
	//setMeans();
	//max is 6, use array;
	//solution2:
	//exportCalibEnergy and means(Eraw,Estand)
	//unlimit max, use vector
	//read from txt
	//const int npoints = xxx.size();

	//float xx[6];//Eraw; [MeV]
	//float yy[6];//Ecal
	vector<float>xx; //Eraw; [MeV]
	vector<float>yy; //Ecal; [MeV]
	xx.clear();
	yy.clear();
	for(int ii=0;ii<npoints;ii++){
		xx.push_back(gmean[ii]);
		yy.push_back(gEref[ii]);
	}

	TGraph *fCalib;
	//fCalib = new TGraph(npoints,xx,yy);
	fCalib = new TGraph(npoints,&xx[0],&yy[0]);
	fCalib->SetTitle("Calibration");
	fCalib->GetYaxis()->SetTitle("Energy[MeV]");
	fCalib->GetXaxis()->SetTitle("Eraw[arb.]");
	fCalib->SetMarkerSize(2);
	fCalib->Draw("AP*");
	TFitResultPtr fpar = fCalib->Fit("pol1","S");
	if(fSAVE){
		gCalibPar[0] = fpar->Parameter(0);
		gCalibPar[1] = fpar->Parameter(1);
		std::cout<<"energy calibration parameters updated."<<std::endl;
	}
	std::cout<<"Fit result:"<<std::endl;
	std::cout<<"par0 = "<<gCalibPar[0]<<std::endl;
	std::cout<<"par1 = "<<gCalibPar[1]<<std::endl;
	std::cout<<"Calibration complete!"<<std::endl;
}

void getEnergyResolution(int fnpeaks = 3){

	std::cout<<"*Num of Peaks is using for fitting is "<<fnpeaks<<std::endl;
	showEnergySpectrum();
	ghist->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);

	ghist->Draw();
	//gaus fit
	TF1 *fgaus;
	vector<char*> fitFuncs;
	fitFuncs.clear();
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)+gaus(10)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)");
	std::cout<<"N of funs = "<<fitFuncs.size()<<std::endl;
	if(fnpeaks <= fitFuncs.size()){
		fgaus = new TF1("fgaus",fitFuncs[fnpeaks-1],0,4000);
		for(int i=0;i<fnpeaks;i++){
			fgaus->SetParameter(1+3*i,gamp[i]);
			fgaus->SetParameter(2+3*i,gmean[i]);
			fgaus->SetParameter(3+3*i,gsigma[i]);
			fgaus->SetParLimits(1+3*i,gamp[i]-5,gamp[i]+5);
			fgaus->SetParLimits(2+3*i,gmean[i]-gParLimit,gmean[i]+gParLimit);
			fgaus->SetParLimits(3+3*i,gsigma[i]-gParLimit/10.,gsigma[i]+gParLimit/10.);
		}
	}
	else{
		std::cout<<"Error414!!! Please offer correct number of peaks to fit(Now the max is "<<fitFuncs.size()<<" )."<<std::endl;
		return;
	}

	ghist->Fit("fgaus","R");


	double fitres[30];
	fgaus->GetParameters(fitres);
	const Double_t *fitresErr = fgaus->GetParErrors();//par errors
	Double_t meanErr[30],sigErr[30];
	//individual plots
	vector <TF1*> fdrawGaus;
	fdrawGaus.clear();
	for(int i=0;i<fnpeaks;i++){
		gamp[i] = fitres[1+i*3];
		gmean[i] = fitres[2+i*3];
		gsigma[i] = fitres[3+i*3];
		meanErr[i] = fitresErr[2+i*3];
		sigErr[i] = fitresErr[3+i*3];
		gEres[i] = gsigma[i]*2.355/gmean[i]*100;//%
		fdrawGaus.push_back(new TF1(TString::Format("fgaus%d",i),"gaus",0,2000));
		for(int ii=0;ii<3;ii++)fdrawGaus.back()->SetParameter(ii,fitres[1+i*3+ii]);
		fdrawGaus.back()->Draw("same");
		fdrawGaus.back()->SetLineColor(1);
	}
	//*** legend ***//
	TText text1;
	text1.SetTextAlign(31);
	text1.SetTextFont(43);
	text1.SetTextSize(20);
	text1.DrawTextNDC(XtoPad(0.5),YtoPad(0.8),"Energy resolution:");

	std::cout<<"*Limit for pars fitting now is "<<gParLimit<<" for Mean and "<<gParLimit/10.<<" for Sigma."<<std::endl;
	std::cout<<"*To adjust this limit, please run 'setFitParLimit(NEW_VALUE)'and 'energyCalib()',respectively.."<<std::endl;
	std::cout<<"*Fit results(only errors of fit pars were considered):"<<std::endl;
	std::cout<<"================================="<<std::endl;
	TString E_res;
	for(int ii=0;ii<fnpeaks;ii++){
		E_res = Form("dE/E = (%.2f +/- %.2f)%%.",gEres[ii],calculateResErr(gEres[ii],gmean[ii],meanErr[ii],gsigma[ii],sigErr[ii]));
		std::cout<<"peak"<<ii<<" = "<<gmean[ii]<<"; with sigma = "<<gsigma[ii]<<" ;"<<E_res<<std::endl;
		//std::cout<<"peak"<<ii<<" = "<<gmean[ii]<<"; with sigma = "<<gsigma[ii]<<" ;deltE/E = ("<<gEres[ii]<<" +/- "<<calculateResErr(gEres[ii],gmean[ii],meanErr[ii],gsigma[ii],sigErr[ii])<<")%"<<std::endl;
		text1.DrawTextNDC(XtoPad(0.7),YtoPad(0.75-0.05*ii),E_res);
	}

}

void fineEnergyCalib(){

	showEnergySpectrum();
	float fpeak[6] = {5106,5156,5443,5486,5763,5806};
	//int gParLimit = 12;
	double tema[6];//amp
	double temm[6];//mean
	double tems[6];//sigma
	if(gcnt_FEC==0)for(int i =0;i<3;i++)gsigma[i]/=2.;
	gcnt_FEC++;

	TCanvas *cCalib2 = new TCanvas("Fine Energy calibration with 6 peaks","Fine Energy calibration",10,80,900,900);
	cCalib2->Divide(2,2);
	cCalib2->cd(1);
	ghist->Draw();
	TH1F *hcalib = (TH1F*)ghist->Clone();
	cCalib2->cd(2);
	hcalib->Draw();
	hcalib->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);
	//hcalib->GetXaxis()->SetRangeUser(4000,7000);
	hcalib->SetTitle("6 peaks Fit");
	//gaus fit

	TF1 *fsum2 = new TF1("fsum2","pol0+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)",4000,7000);
	fsum2->SetParameter(0,0);
	//source 1
	fsum2->SetParameter(1,gamp[0]/2.);
	fsum2->SetParameter(2,fpeak[0]);
	fsum2->SetParameter(3,gsigma[0]);
	fsum2->SetParameter(5,fpeak[1]);
	fsum2->SetParameter(6,gsigma[0]);
	//source 2
	fsum2->SetParameter(7,gamp[1]/2.);
	fsum2->SetParameter(8,fpeak[2]);
	fsum2->SetParameter(9,gsigma[1]);
	fsum2->SetParameter(11,fpeak[3]);
	fsum2->SetParameter(12,gsigma[1]);
	//source 3
	fsum2->SetParameter(13,gamp[2]/2.);
	fsum2->SetParameter(14,fpeak[4]);
	fsum2->SetParameter(15,gsigma[2]);
	fsum2->SetParameter(17,fpeak[5]);
	fsum2->SetParameter(18,gsigma[2]);

	fsum2->SetParLimits(1,gamp[0]/6.,0.5*gamp[0]);
	fsum2->SetParLimits(2,fpeak[0]-gParLimit,fpeak[0]+gParLimit);
	fsum2->SetParLimits(3,gsigma[0]-gParLimit/10.,gsigma[0]+gParLimit/10.);
	fsum2->SetParLimits(5,fpeak[1]-gParLimit,fpeak[1]+gParLimit);
	fsum2->SetParLimits(6,gsigma[0]-gParLimit/10.,gsigma[0]+gParLimit/10.);

	fsum2->SetParLimits(7,gamp[1]/6.,0.5*gamp[1]);
	fsum2->SetParLimits(8,fpeak[2]-gParLimit,fpeak[2]+gParLimit);
	fsum2->SetParLimits(9,gsigma[1]-gParLimit/10.,gsigma[1]+gParLimit/10.);
	fsum2->SetParLimits(11,fpeak[3]-gParLimit,fpeak[3]+gParLimit);
	fsum2->SetParLimits(12,gsigma[1]-gParLimit/10.,gsigma[1]+gParLimit/10.);

	fsum2->SetParLimits(13,gamp[2]/6.,0.5*gamp[2]);
	fsum2->SetParLimits(14,fpeak[4]-gParLimit,fpeak[4]+gParLimit);
	fsum2->SetParLimits(15,gsigma[2]-gParLimit/10.,gsigma[2]+gParLimit/10.);
	fsum2->SetParLimits(17,fpeak[5]-gParLimit,fpeak[5]+gParLimit);
	fsum2->SetParLimits(18,gsigma[2]-gParLimit/10.,gsigma[2]+gParLimit/10.);

	fsum2->SetLineColor(kBlack);
	hcalib->GetXaxis()->SetTitle("After calibration[keV]");
	//for(int i=0;i<6;i++)temm[i] = fpeak[i];

	hcalib->Fit("fsum2","R");

	//print out mean and sigma
	double fitres[20];
	fsum2->GetParameters(fitres);
	for(int i=0;i<6;i++)tema[i] = fitres[1+i*3];
	for(int i=0;i<6;i++)temm[i] = fitres[2+i*3];
	for(int i=0;i<6;i++)tems[i] = fitres[3+i*3];
	std::cout<<"*Limit for pars fitting now is "<<gParLimit<<" for Mean and "<<gParLimit/10.<<" for Sigma."<<std::endl;
	std::cout<<"Initialized/Expected mean:"<<std::endl;
	for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<fpeak[i]<<std::endl;
	std::cout<<"Fit results(from left to right):"<<std::endl;
	for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<temm[i]<<std::endl;
	for(int i=0;i<6;i++)std::cout<<"sigma"<<i<<" = "<<tems[i]<<std::endl;
	std::cout<<"Calibration complete!"<<std::endl;

	//draw 6 peaks
	vector<TF1*> drPeaks;
	drPeaks.clear();

	for(int i=0;i<6;i++){
		drPeaks.push_back( new TF1(Form("peak%d",i),"gaus",4000,7000) );
		drPeaks.at(i)->SetTitle(Form("peaks%d",i) );
		drPeaks.at(i)->SetParameter(0,tema[i]);
		drPeaks.at(i)->SetParameter(1,temm[i]);
		drPeaks.at(i)->SetParameter(2,tems[i]);
		drPeaks.at(i)->SetLineColor(i+1);
		drPeaks.at(i)->SetLineStyle(3);
		drPeaks.at(i)->SetLineWidth(5);
		drPeaks.at(i)->Draw("same");
	}
	//drPeaks.at(0)->Draw("same");

	//linear fit with 6 points
	//to be continued
	//calibration parameters
	/*
	   cCalib2->cd(3);
	   TGraph *gCalib = new TGraph(6,xx,yy);
	   gCalib->SetTitle("Calibration");
	   gCalib->GetYaxis()->SetTitle("Energy[keV]");
	   gCalib->GetXaxis()->SetTitle("Integral");
	   gCalib->SetMarkerSize(2);
	   gCalib->Draw("AP*");
	   TFitResultPtr fpar = gCalib->Fit("pol1","S");
	   if(fSAVE){
	   gCalibPar[0] = fpar->Parameter(0);
	   gCalibPar[1] = fpar->Parameter(1);
	   std::cout<<"energy calibration parameters updated."<<std::endl;
	   }
	   std::cout<<"Fit result:"<<std::endl;
	   std::cout<<"par0 = "<<gCalibPar[0]<<std::endl;
	   std::cout<<"par1 = "<<gCalibPar[1]<<std::endl;
	   std::cout<<"Calibration complete!"<<std::endl;
	   */
}
void initRoot(){
	optfile = new TFile(gOptRootFileName,"RECREATE");
	optTree = new TTree("tree","tree");

	//optTree->Branch("runnum",&runnum,"runnum/I");
	optTree->Branch("entry",&entry,"entry/I");
	optTree->Branch("ts",&runts,"runts/L");
	optTree->Branch("tsS",&runts_S,"runts_S/L");
	optTree->Branch("Eraw",&Eraw,"Eraw/D");
	optTree->Branch("Ecal",&Ecal,"Ecal/D");
	optTree->Branch("T_cfd",&T_cfd,"T_cfd/D");
	optTree->Branch("tracesX",&tracesX);
	optTree->Branch("tracesY",&tracesY);
	std::cout<<"--->init output root file."<<std::endl;
}

//void root_Clear(&Long64_t fts, &double fenergyr, &double fenergyc, &double fTime_cfd){
//	fts = -1;
//	fenergyr = -1;
//	fenergyc = -1;
//	fTime_cfd = -9999;
//}

//export root file
void saveAsRoot(TTree *fopttree, int frun, Long64_t fts, double fenergyr, double fenergyc, double fTime_cfd, TGraph *fgr){
	//clear
	//runnum = -1;
	entry = -1;
	Eraw = Ecal = -1;
	T_cfd = -9999;
	runts = -1;
	runts_S = -1;
	tracesX.clear();
	tracesY.clear();

	//runnum = frun;
	entry = frun;
	runts = fts;
	runts_S = fts%1000000000;
	Eraw = fenergyr;
	Ecal = fenergyc;
	T_cfd = fTime_cfd;
	if(gVerbose>0)std::cout<<"test: "<<Eraw<<" "<<Ecal<<std::endl;
	if(fsaveTraces){
		int length = fgr->GetN();
		double sum =0;
		double bkg=0;
		for(int i=0;i<length;i++){
			double x,y;
			fgr->GetPoint(i,x,y);
			tracesX.push_back(x);
			tracesY.push_back(y);
		}
	}
	fopttree->Fill();
}

/*
   double getZERO(){

   }
   */
//https://doi.org/10.1016/j.apradiso.2020.109171
//B.J.Kim
TGraph* MWD(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error409. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;

	vector<double>MWD;
	vector<double>MWDx;
	int k0,MM,LL;
	double sum0=0;
	double tau = gDecaytime*gSampleRate/MHZtoNS;//3000 sample points
	double DM=0;
	double mwd=0;
	//********************//
	//---            -----//
	//  a-          -d    //
	//    -        -      //
	//     -      -       //
	//     b------c       //
	//********************//
	//
	MM=(int)gtrapezMovingWindowLength*gSampleRate/MHZtoNS;//length from b to d; [sample point]
	LL=(int)gRisingTime*gSampleRate/MHZtoNS;//length from a to b; [sample point]
											//flat on top/bottom: MM-LL; [sample point]
	k0=gtrapezSampleWindow[0];//[sample point]
	xx.clear();
	yy.clear();
	//test
	//std::cout<<"kk = "<<k0<<" MM = "<<MM<<" LL = "<<LL<<std::endl;

	MWD.clear();
	MWDx.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(y);
	}
	//std::cout<<"start from "<<Sx[0]<<" ns"<<std::endl;
	int fend = (int)gtrapezSampleWindow[1];
	if(isFebex && fend>=gFebexTraceL){
		std::cout<<"Error417!!!"<<std::endl;
		std::cout<<"Current trace length = "<<gFebexTraceL<<std::endl;
		std::cout<<"Trapez window should be < trace length. Please reduce your trapez sample window length."<<std::endl;
		return 0;
	}
	for(int kk = k0;kk < fend;kk=kk+1){
		DM=0;
		for(int j=kk-LL;j<=kk-1;j=j+1){
			if(j<1){
				std::cout<<"Error410!!! Please decrease your rising time OR shift the start point of sample window to the right."<<std::endl;
				return 0;
			}
			sum0=0;
			for(int i=kk-MM;i<=kk-1;i++){
				if(i<-200+1){
					std::cout<<"*kk-MM = "<<i<<std::endl;
					std::cout<<"Error411!!! Please decrease your Trapezoidal moving window length OR shift the start point of sample window to the right."<<std::endl;
					return 0;
				}
				sum0 += yy[i];
			}
			DM += (yy[kk] - yy[kk-MM] + 1.0*sum0/tau);
		}
		mwd = DM/LL;

		MWD.push_back(mwd);
		MWDx.push_back(xx[kk]);
	}

	TGraph *fMWD = new TGraph(MWD.size(),&MWDx[0],&MWD[0]);
	fMWD->SetTitle("Trapezoidal Filter with Moving Window Deconvolution");
	return fMWD;
}

TGraph* MWD2(TGraph *fgr){
	int length = 0 ;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error410. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;
	vector<double>SS;
	vector<double>Sx;
	int kk,LL,GG,k0;
	//30 and 2, read energy from max: better but not good enough.
	//3 and 30, read energy from max in (1.5,3): only single peak.
	LL=16;
	GG=5;
	xx.clear();
	yy.clear();
	SS.clear();
	Sx.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(y);
	}
	//TFA-MWD
	SS.push_back(0);
	Sx.push_back(xx[494]);
	//std::cout<<"start from "<<Sx[0]<<" ns"<<std::endl;
	//for(int i=1;i<50;i++){
	for(int i=1;i<(2*LL+GG);i++){
		kk=i+494;
		SS.push_back( SS[i-1] + yy[kk] - yy[kk-LL] + yy[kk-2*LL-GG] - yy[kk-LL-GG] );
		Sx.push_back(xx[kk]);
	}
	TGraph *fMWD = new TGraph(SS.size(),&Sx[0],&SS[0]);
	fMWD->SetTitle("Trapezoidal Filter with Moving Window Deconvolution");
	return fMWD;
}

TGraph* CRRC(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error412. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;//raw data
	vector<double>SS;
	vector<double>Sx;
	float b1,b2,a0,a1,a2;
	int kk=0;
	b1=0.1;
	b2=0.1;
	a0=0.3;
	a1=0.3;
	a2=0.3;
	const int FSP = 400;//first sample point
	xx.clear();
	yy.clear();
	SS.clear();
	Sx.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(y);
	}
	//
	SS.push_back(0);
	Sx.push_back(xx[FSP]);
	SS.push_back(0);
	Sx.push_back(xx[FSP+1]);
	//std::cout<<"start from "<<Sx[0]<<" ns"<<std::endl;
	for(int i=2;i<300;i++){
		kk=i+FSP;
		SS.push_back( b1*SS[i-1] + b2*SS[i-2] + a0*yy[kk] + a1*yy[kk-1] + a2*yy[kk-2] );
		Sx.push_back(xx[kk]);
	}
	TGraph *fCRRC = new TGraph(SS.size(),&Sx[0],&SS[0]);
	fCRRC->SetTitle("CR-RC filter");
	return fCRRC;
}

TGraph* graphAdd(TGraph *fgr1, TGraph *fgr2, double fpar1, double fpar2){

	int length1 = 0;
	int length2 = 0;
	if(fgr1 == 0 || fgr1->GetN()<1 || fgr2 == 0 || fgr2->GetN()<1 ){
		std::cout<<"Error412. Empty graph."<<std::endl;
		return 0;
	}
	length1 = fgr1->GetN();
	length2 = fgr2->GetN();
	if(length1 != length2){
		std::cout<<"Error413. Different graph format."<<std::endl;
		return 0;
	}
	vector<double>xx;
	vector<double>yy;
	xx.clear();
	yy.clear();
	for(int i=0;i<length1;i++){
		double x1,y1;
		double x2,y2;
		fgr1->GetPoint(i,x1,y1);
		fgr2->GetPoint(i,x2,y2);
		xx.push_back(x1);
		yy.push_back(y1*fpar1+y2*fpar2);
	}
	TGraph *fadd = new TGraph(xx.size(),&xx[0],&yy[0]);
	fadd->SetTitle("Add");
	return fadd;
}
/*
   TGraph* graphDrawSame(TGraph *fgr1, TGraph *fgr2){

   }
   */

inline void closeRoot(TTree *fopttree, TFile *foptfile){
	fopttree->Write();
	foptfile->Close();
	std::cout<<"******************************************"<<std::endl;
	std::cout<<"Data has been saved to "<<gOptRootFileName<<"."<<std::endl;
	//std::cout<<"*"<<tracesX.size()<<" Samples have been kept in the data."<<std::endl;
	std::cout<<"Energy calibration parameters:"<<gCalibPar[0]<<"  "<<gCalibPar[1]<<std::endl;
	std::cout<<"******************************************"<<std::endl;
}

#endif
