//
//last updated@ 17 Jan,2024
//Z.Chen
//E-mail: z.chen@gsi.de
//

//*********************************************************************************//
//*********************************release notes***********************************//
//22Jan,2023
//some updates on energy calibration
//29Jan,2023
//some updates related to trapez
//10Feb, 2023
//some updates related to Febex4
//21 Feb
//some updates related to display
//6 Mar
//extend num of peaks to fit from 3 to 6
//11 Mar
//add gFebexTraceL to restrict trace length
//add isFebex and isScope
//26 Mar
//optimize setMeans and setSigmas
//17 Apr, 2023
//optimize data loading
//12 Nov, 2023
//exportFebex4Data();
//13 Nov, 2023
//add a new function to create a hotstart. createHotstart()
//17 Nov, 2023
//add ucesb
//subtract baseline noise
//22 Nov
//add more export funcs
//02 Dec
//Optimize createNewSettingFile_Febex()
//runnum changed to entry
//05 Dec
//add running time and speed to export file
//21 Dec
//optimize gref 
//24 Dec
//some updates on createNewSettingFile_Febex() 
//add new branch "entry_empty" for output root file
//define a structure to deliver variables.
//26 Dec
//add gSingleChAnaMode, gMultiChAnaMode
//add TGraph *gSingleTraceGraph;
//add TGraph *gMWDGraph, *gCFDGraph, *gBaselineCorrGraph, *gTimeDriftCorrGraph;
//30 Dec
//optimize energy calibration to support up to 10 peaks;
//change the format of calibration parameters to array.
//17 Jan, 2024
//add EVENTNO_c4
//optimize multimode reading
//add new branch overflow
//**********************************************************************************//
#ifndef _ENV_HH_
#define _ENV_HH_

#include <sys/stat.h>
#include <iostream>
#define TRACE_LENGTH 3000
#define MAX_SIZE 16
#define OVERFLOW_thre 8100

const char* gSettings = "settings.set";
TString gSettings_rename = "settings_Febex_export.set";
TString gV1742data = "../V1742/output_0018.root";
TString gFebex4data = "./LISA_root_tree.root";
TString gTxtDir = "../fD_fS_3.5V/";
TString gTsfilename = "timestamp.log";
ofstream optPar("export_pars.txt",ios::app);//not overwrite

struct dataCluster {

	//TTree *ftree = 0;
	UInt_t trigger = 0;
	UInt_t evtno = 0;
	int fch = -1;
	int fentry = -1;
	int fentry_empty = -1;
	Long64_t fts = -1;
	double fenergy_raw = -1;
	double fenergy_cal = -1;
	double ftime_cfd = -9999;
	UInt_t foverflow = 0;
	UInt_t flength = 0;
	TGraph *fgr = 0;

};

const int mV = 1000;
const int ns = 1e9;
//const int hMIN = 0;
//const int hMAX = 40000;
int ghMIN = 0;
int ghMAX = 40000;
//const int hTMIN = -400;
//const int hTMAX = 400;
int ghTMIN = -2000;
int ghTMAX = 2000;
const int MHZtoNS = 1000;// unit:  MHz/ns

double window[2] = {-0.2,2.};//Integral window;[ns]
int gScopeDataLoading[2] = {10000,30000};//load scope(.txt)data from sample_point_i to sample_point_j;[sample point;pts]
int gtrapezSampleWindow[2] = {700,1500};////Trapez Sample Window;[sample point;pts]
int gtrapezMovingWindowLength = 3200;//[ns]
int gRisingTime = 100;//[ns]
int gDecaytime = 48000;//[ns]
double bwindow[2] = {-40.,-10.};
int gRun[2] = {1,1000};
int gTreeEntry[2] = {0,3000};
int gNbins = 4000;
double gSampleRate = 62.5;// MHz
int gESrange[2] = {0,20000};
int gTraceAmp[2] ={-2000,2000};
double gTSrange[2] = {-200.,200.};
double gCFDwin[2] = {140.,160.};
map<int,double>gRawE;
vector <TGraph*>gRawTraces;
//map<int,TGraph*>gRawTraces;
map<int,Long64_t>gTs;
//map<int,Long64_t>gTs_S;//short
double gCalibPar_b[MAX_SIZE];//bias
double gCalibPar_s[MAX_SIZE];//slop
double gamp[MAX_SIZE];
double gmean[MAX_SIZE];
double gsigma[MAX_SIZE];
double gEref[MAX_SIZE];//energy reference for calibration; [MeV]
double gEres[MAX_SIZE];
double gTZAmp[2] = {-200,200};//140,-200,1700
							  //double gTZ2 = 200;//150,+200,1800
double gFra = 0.9;
double gDel = 1.2;//ns
double gParLimit = 12;
int gVerbose = 0;
int gFebexCh = 0;//febex channel ID
//int gMultiCh[16] = {-1};//by default -1
bool gMultiCh[16] = {false};//by default false
double gThreshold[16] = {-1};//threshold to select hit
int gFebexTraceL = 2000;//febex trace length
float gAverageSpeed = 0;
float gTotalRunningTime = 0;
TGraph *gSingleTraceGraph = 0;
TGraph *gMWDGraph = 0;
TGraph *gCFDGraph = 0;
TGraph *gBaselineCorrGraph = 0;
TGraph *gTimeDriftCorrGraph = 0;

bool isFebex = false;
bool isScope = false;
bool gIsUcesbUnpack = false;
bool gIsGo4Unpack = false;
bool gBadEntry[16] = {0};
bool gSingleChAnaMode = true;//by default, use single mode
bool gMultiChAnaMode = false;
vector <double> gCFDtime;
double gCFDref = 0.0;
TH1F *ghist;
int gcnt_FEC = 0;
int gcnt_loadSetting = 0;
bool gBaselineCorr = false;
bool gTimeCorr = false;
bool glTs = false;
bool gMWD = false;
bool gIntegral = false;
bool gSubNoise = false;
bool gTreeAllEntry = false;
vector<double> gtracesX_tem;
vector<double> gtracesY_tem;
//*************tree***************//
TString gOptRootFileName = "sCVD.root";
UInt_t TRIGGER_c4; //copy from unpacked data
UInt_t EVENTNO_c4; //copy of EVENTNO from unpacked data
UInt_t hit;//max is 16
Int_t febCh[16]; //0-15 
Int_t entry;
Int_t entry_empty[16];
Long64_t runts[16];
Long64_t runts_S[16];
Double_t Eraw[16];
Double_t Ecal[16];
Double_t T_cfd[16]; //time determined by CFD
UInt_t if_overflow[16]; //overflow; 0, no; 1 yes
UInt_t traceLength[16];
Double_t tracesX[16][TRACE_LENGTH];
Double_t tracesY[16][TRACE_LENGTH];
//vector<double> tracesX;
//vector<double> tracesY;
bool fSaveAsRoot = false;
bool fsaveTraces = false;
TFile *optfile;
TTree *optTree;

inline void createHotstart(){
	ofstream hotStart("hotStart.sh",ios::out);//overwrite
	hotStart<<"#!/bin/bash"<<std::endl;
	hotStart<<"root -l 'anaTraces.cc(\""<<gSettings<<"\")'"<<std::endl;
}

inline void setSettingFile(const char* fstring){
	gSettings = fstring;
}

inline void renameSettingFile(const char* fstring){
	gSettings_rename = fstring;
}

inline void showSettingFile(){
	std::cout<<"setting file: "<<gSettings<<std::endl;
}

inline void exportSettingFile(){
	optPar<<"Settings.filename:   "<<gSettings<<std::endl;
}

inline void setScopeDataDir(const char* fstring){
	gTxtDir = fstring;
}

inline void showScopeDataDir(){
	std::cout<<"Scope data directory: "<<gTxtDir<<std::endl;
}

inline void exportScopeDataDir(){
	optPar<<"Scope.data.directory:   "<<gTxtDir<<std::endl;
}

inline void setTsFile(const char* fstring){
	gTsfilename = fstring;
}

inline void showTsFile(){
	std::cout<<"Timestamp filename: "<<gTsfilename<<std::endl;
}

inline void exportTsFile(){
	optPar<<"Scope.timestamp.filename:   "<<gTsfilename<<std::endl;
}

inline void exportV1742Data(){
	optPar<<"V1742.data.filename:   "<<gV1742data<<std::endl;
}

inline void exportFebex4Data(){
	optPar<<"Febex4.data.filename:   "<<gFebex4data<<std::endl;
}

inline void exportIsUcesb(){
	optPar<<"Unpack.with.ucesb:   "<<gIsUcesbUnpack<<std::endl;
}

inline void exportIsGo4(){
	optPar<<"Unpack.with.go4:   "<<gIsGo4Unpack<<std::endl;
}

inline void exportSpeed(){
	optPar<<"Average.speed:   "<<gAverageSpeed<<" #[evt/s]"<<std::endl;
}

inline void exportTotalRunningTime(){
	optPar<<"Total.running.time:   "<<gTotalRunningTime<<" #[s]"<<std::endl;
}

inline void enableSingleMode(){
	gSingleChAnaMode = true;
	gMultiChAnaMode = false;
}

inline void enableMultiMode(){
	gMultiChAnaMode = true;
	gSingleChAnaMode = false;
}

inline void exportAnaMode(){
	optPar<<"Enable.single.channel.analysis.mode:   "<<gSingleChAnaMode<<std::endl;
	optPar<<"Enable.multi.channel.analysis.mode:   "<<gMultiChAnaMode<<std::endl;
}

inline void exportMultiModeCh(){
	for(int i=0;i<16;i++)optPar<<TString::Format("Multimode.read.from.ch.%d:   ",i)<<gMultiCh[i]<<std::endl;
}

inline void enableSubNoise(){
	gSubNoise = true;
}

inline void disableSubNoise(){
	gSubNoise = false;
}

inline void exportSubNoise(){
	optPar<<"Subtract.noise.with.baseline:   "<<gSubNoise<<std::endl;
}

inline void enableReadAllEntry(){
	gTreeAllEntry = true;
}

inline void disableReadAllEntry(){
	gTreeAllEntry = false;
}

inline void exportReadAllEntry(){
	optPar<<"Enable.read.all.entry:   "<<gTreeAllEntry<<std::endl;
}

inline void setIntergralWindow(double fval1, double fval2){
	window[0] = fval1;
	window[1] = fval2;
}

inline void showIntergralWindow(){
	std::cout<<"win0 = "<<window[0]<<" win1 = "<<window[1]<<std::endl;
}

inline void exportIntergralWindow(){
	optPar<<"Integral.window.0:   "<<window[0]<<std::endl;
	optPar<<"Integral.window.1:   "<<window[1]<<std::endl;
}

inline void setTrapezWindow(int fval1, int fval2){
	gtrapezSampleWindow[0] = fval1;
	gtrapezSampleWindow[1] = fval2;
}

inline void showTrapezWindow(){
	std::cout<<"win0 = "<<gtrapezSampleWindow[0]<<" win1 = "<<gtrapezSampleWindow[1]<<std::endl;
}

inline void exportTrapezWindow(){
	optPar<<"Trapez.Sample.window.0:   "<<gtrapezSampleWindow[0]<<std::endl;
	optPar<<"Trapez.Sample.window.1:   "<<gtrapezSampleWindow[1]<<std::endl;
}

inline void setScopeDataLoading(int fval1, int fval2){
	gScopeDataLoading[0] = fval1;
	gScopeDataLoading[1] = fval2;
}

inline void showScopeDataLoading(){
	std::cout<<"win0 = "<<gScopeDataLoading[0]<<" win1 = "<<gScopeDataLoading[1]<<std::endl;
}

inline void exportScopeDataLoading(){
	optPar<<"Scope.Data.From.0:   "<<gScopeDataLoading[0]<<std::endl;
	optPar<<"Scope.Data.From.1:   "<<gScopeDataLoading[1]<<std::endl;
}

inline void setFebexChannel(UInt_t fch){
	gFebexCh = fch;
}

inline void showFebexChannel(){
	std::cout<<"current febex channel = "<<gFebexCh<<std::endl;
}

inline void exportFebexChannel(){
	optPar<<"Febex4.read.from.ch:   "<<gFebexCh<<std::endl;
}

inline void showFebexTraceLength(){
	std::cout<<"Trace Length = "<<gFebexTraceL<<std::endl;
}

inline void exportFebexTraceLength(){
	optPar<<"Febex4.trace.length:   "<<gFebexTraceL<<std::endl;
}

inline void setTrapezAmpCalcWindow(int fval1, int fval2){
	gTZAmp[0] = fval1;
	gTZAmp[1] = fval2;
}

inline void showTrapezAmpCalcWindow(){
	std::cout<<"win0 = "<<gTZAmp[0]<<"[ns]; win1 = "<<gTZAmp[1]<<std::endl;
}

inline void exportTrapezAmpCalcWindow(){
	optPar<<"Trapez.Amp.Calc.window.0:   "<<gTZAmp[0]<<std::endl;
	optPar<<"Trapez.Amp.Calc.window.1:   "<<gTZAmp[1]<<std::endl;
}

inline void exportScopeRun(){
	optPar<<"Scope.data.runnummber.0:   "<<gRun[0]<<std::endl;
	optPar<<"Scope.data.runnummber.1:   "<<gRun[1]<<std::endl;
}

inline void exportTreeEntry(){
	optPar<<"TreeEntry.read.from.0:   "<<gTreeEntry[0]<<std::endl;
	optPar<<"TreeEntry.read.from.1:   "<<gTreeEntry[1]<<std::endl;
}

inline void setEnergySpecRange(int fval1, int fval2){
	gESrange[0] = fval1;
	gESrange[1] = fval2;
}

inline void showEnergySpecRange(){
	std::cout<<"ESrange0 = "<<gESrange[0]<<" ESrange1 = "<<gESrange[1]<<std::endl;
}

inline void exportEnergySpecRange(){
	optPar<<"Energy.spectrum.range.0:   "<<gESrange[0]<<std::endl;
	optPar<<"Energy.spectrum.range.1:   "<<gESrange[1]<<std::endl;
}

inline void setTimeSpecRange(int fval1, int fval2){
	gTSrange[0] = fval1;
	gTSrange[1] = fval2;
}

inline void showTimeSpecRange(){
	std::cout<<"TSrange0 = "<<gTSrange[0]<<" TSrange1 = "<<gTSrange[1]<<std::endl;
}

inline void exportTimeSpecRange(){
	optPar<<"Time.spectrum.range.0:   "<<gTSrange[0]<<std::endl;
	optPar<<"Time.spectrum.range.1:   "<<gTSrange[1]<<std::endl;
}

inline void exportTimeHistoRange(){
	optPar<<"Time.histogram.range.0:   "<<ghTMIN<<std::endl;
	optPar<<"Time.histogram.range.1:   "<<ghTMAX<<std::endl;
}

inline void setTraceAmplitude(int fval1, int fval2){
	gTraceAmp[0] = fval1;
	gTraceAmp[1] = fval2;
}

inline void showTraceAmplitude(){
	std::cout<<"min = "<<gTraceAmp[0]<<" max = "<<gTraceAmp[1]<<std::endl;
}

inline void exportTraceAmplitude(){
	optPar<<"Trace.graph.Y.min:   "<<gTraceAmp[0]<<std::endl;
	optPar<<"Trace.graph.Y.max:   "<<gTraceAmp[1]<<std::endl;
}

inline void setCFDwindow(double fval1, double fval2){
	gCFDwin[0] = fval1;
	gCFDwin[1] = fval2;
}

inline void showCFDwindow(){
	std::cout<<"CFDwin0 = "<<gCFDwin[0]<<" CFDwin1 = "<<gCFDwin[1]<<std::endl;
}

inline void exportCFDwindow(){
	optPar<<"CFD.window.0:   "<<gCFDwin[0]<<std::endl;
	optPar<<"CFD.window.1:   "<<gCFDwin[1]<<std::endl;
}

inline void setCFDreference(double fval){
	gCFDref = fval;
}

inline void showCFDreference(){
	std::cout<<"CFDref = "<<gCFDref<<std::endl;
}

inline void exportCFDreference(){
	optPar<<"CFD.reference:   "<<gCFDref<<std::endl;
}

inline void setSampleRate(double fval){
	gSampleRate = fval;
}

inline void showSampleRate(){
	std::cout<<"Sample rate = "<<gSampleRate<<std::endl;
}

inline void exportSampleRate(){
	optPar<<"Sample.rate[MHz]:   "<<gSampleRate<<std::endl;
}

inline void setTrapezMovingWindowLength(double fval){
	gtrapezMovingWindowLength = fval;
}

inline void showTrapezMovingWindowLength(){
	std::cout<<"Moving window length = "<<gtrapezMovingWindowLength<<std::endl;
}

inline void exportTrapezMovingWindowLength(){
	optPar<<"Trapez.Moving.Window.Length[ns]:   "<<gtrapezMovingWindowLength<<std::endl;
}

inline void setRisingTime(double fval){
	gRisingTime = fval;
}

inline void showRisingTime(){
	std::cout<<"Rising Time = "<<gRisingTime<<std::endl;
}

inline void exportRisingTime(){
	optPar<<"Rising.time[ns]:   "<<gRisingTime<<std::endl;
}

inline void setDeacytime(double fval){
	gDecaytime = fval;
}

inline void showDecaytime(){
	std::cout<<"Decay Time = "<<gDecaytime<<std::endl;
}

inline void exportDecaytime(){
	optPar<<"Decay.time[ns]:   "<<gDecaytime<<std::endl;
}

inline void setFitParLimit(double fval){
	gParLimit = fval;
}

inline void showFitParLimit(){
	std::cout<<"fit pars limit = "<<gParLimit<<std::endl;
}

inline void exportFitParLimit(){
	optPar<<"Fit.Pars.Limit:   "<<gParLimit<<std::endl;
}

inline void setNbins(int fval){
	gNbins = fval;
}

inline void showNbins(){
	std::cout<<"Nbins = "<<gNbins<<std::endl;
}

inline void exportNbins(){
	optPar<<"Histogram.bins:   "<<gNbins<<std::endl;
}

inline void setCFDpars(double fFra, double fDel){
	gFra = fFra;
	gDel = fDel;
}

inline void showCFDpars(){
	std::cout<<"Fraction = "<<gFra<<" delay = "<<gDel<<std::endl;
}

inline void exportCFDpars(){
	optPar<<"CFD.pars.fraction:   "<<gFra<<std::endl;
	optPar<<"CFD.pars.delay:   "<<gDel<<std::endl;
}

inline void setVerbose(int fval){
	gVerbose = fval;
}

inline void showVerbose(){
	std::cout<<"verbose = "<<gVerbose<<std::endl;
}

inline void exportVerbose(){
	optPar<<"Verbos:   "<<gVerbose<<std::endl;
}

inline void setMeans(double fm1){
	gmean[0] = fm1;
}

inline void setMeans(double fm1, double fm2){
	gmean[0] = fm1;
	gmean[1] = fm2;
}

inline void setMeans(double fm1, double fm2, double fm3){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
	gmean[5] = fm6;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
	gmean[5] = fm6;
	gmean[6] = fm7;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7, double fm8){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
	gmean[5] = fm6;
	gmean[6] = fm7;
	gmean[7] = fm8;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7, double fm8, double fm9){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
	gmean[5] = fm6;
	gmean[6] = fm7;
	gmean[7] = fm8;
	gmean[8] = fm9;
}

inline void setMeans(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7, double fm8, double fm9, double fm10){
	gmean[0] = fm1;
	gmean[1] = fm2;
	gmean[2] = fm3;
	gmean[3] = fm4;
	gmean[4] = fm5;
	gmean[5] = fm6;
	gmean[6] = fm7;
	gmean[7] = fm8;
	gmean[8] = fm9;
	gmean[9] = fm10;
}

inline void showMeans(){
	for(int ii=0;ii<MAX_SIZE/2;ii++)std::cout<<Form("mean%d = ",ii)<<gmean[ii]<<"; ";
	std::cout<<std::endl;
	for(int ii=MAX_SIZE/2;ii<MAX_SIZE;ii++)std::cout<<Form("mean%d = ",ii)<<gmean[ii]<<"; ";
	std::cout<<std::endl;
}

inline void exportMeans(){
	for(int ii=0;ii<MAX_SIZE;ii++)optPar<<Form("Mean.value.%d:   ",ii)<<gmean[ii]<<std::endl;
}

inline void setEnergyRef(double fm1){
	gEref[0] = fm1;
}

inline void setEnergyRef(double fm1, double fm2){
	gEref[0] = fm1;
	gEref[1] = fm2;
}

inline void setEnergyRef(double fm1, double fm2, double fm3){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
}

inline void setEnergyRef(double fm1, double fm2, double fm3, double fm4){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
	gEref[3] = fm4;
}

inline void setEnergyRef(double fm1, double fm2, double fm3, double fm4, double fm5){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
	gEref[3] = fm4;
	gEref[4] = fm5;
}

inline void setEnergyRef(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
	gEref[3] = fm4;
	gEref[4] = fm5;
	gEref[5] = fm6;
}

inline void setEnergyRef(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
	gEref[3] = fm4;
	gEref[4] = fm5;
	gEref[5] = fm6;
	gEref[6] = fm7;
}

inline void setEnergyRef(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7, double fm8){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
	gEref[3] = fm4;
	gEref[4] = fm5;
	gEref[5] = fm6;
	gEref[6] = fm7;
	gEref[7] = fm8;
}

inline void setEnergyRef(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7, double fm8, double fm9){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
	gEref[3] = fm4;
	gEref[4] = fm5;
	gEref[5] = fm6;
	gEref[6] = fm7;
	gEref[7] = fm8;
	gEref[8] = fm9;
}

inline void setEnergyRef(double fm1, double fm2, double fm3, double fm4, double fm5, double fm6, double fm7, double fm8, double fm9, double fm10){
	gEref[0] = fm1;
	gEref[1] = fm2;
	gEref[2] = fm3;
	gEref[3] = fm4;
	gEref[4] = fm5;
	gEref[5] = fm6;
	gEref[6] = fm7;
	gEref[7] = fm8;
	gEref[8] = fm9;
	gEref[9] = fm10;
}

inline void exportEnergyRef(){
	for(int ii=0;ii<MAX_SIZE;ii++)optPar<<Form("Energy.ref.%d:   ",ii)<<gEref[ii]<<std::endl;
}

inline void exportEnergyResolution(){
	for(int ii=0;ii<MAX_SIZE;ii++)optPar<<Form("dE/E.value.%d:   ",ii)<<gEres[ii]<<std::endl;
}

inline void setSigmas(double fs1){
	gsigma[0] = fs1;
}

inline void setSigmas(double fs1, double fs2){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
}

inline void setSigmas(double fs1, double fs2, double fs3){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
	gsigma[5] = fs6;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
	gsigma[5] = fs6;
	gsigma[6] = fs7;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7, double fs8){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
	gsigma[5] = fs6;
	gsigma[6] = fs7;
	gsigma[7] = fs8;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7, double fs8, double fs9){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
	gsigma[5] = fs6;
	gsigma[6] = fs7;
	gsigma[7] = fs8;
	gsigma[8] = fs9;
}

inline void setSigmas(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7, double fs8, double fs9, double fs10){
	gsigma[0] = fs1;
	gsigma[1] = fs2;
	gsigma[2] = fs3;
	gsigma[3] = fs4;
	gsigma[4] = fs5;
	gsigma[5] = fs6;
	gsigma[6] = fs7;
	gsigma[7] = fs8;
	gsigma[8] = fs9;
	gsigma[9] = fs10;
}

inline void showSigmas(){
	for(int ii=0;ii<MAX_SIZE/2;ii++)std::cout<<Form("sigma%d = ",ii)<<gsigma[ii];
	std::cout<<std::endl;
	for(int ii=MAX_SIZE/2;ii<MAX_SIZE;ii++)std::cout<<Form("sigma%d = ",ii)<<gsigma[ii];
	std::cout<<std::endl;
}

inline void exportSigmas(){
	for(int ii=0;ii<MAX_SIZE;ii++)optPar<<Form("Sigma.value.%d:   ",ii)<<gsigma[ii]<<std::endl;
}

inline void setAmplitudes(double fs1){
	gamp[0] = fs1;
}

inline void setAmplitudes(double fs1, double fs2){
	gamp[0] = fs1;
	gamp[1] = fs2;
}

inline void setAmplitudes(double fs1, double fs2, double fs3){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
}

inline void setAmplitudes(double fs1, double fs2, double fs3, double fs4){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
	gamp[3] = fs4;
}

inline void setAmplitudes(double fs1, double fs2, double fs3, double fs4, double fs5){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
	gamp[3] = fs4;
	gamp[4] = fs5;
}

inline void setAmplitudes(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
	gamp[3] = fs4;
	gamp[4] = fs5;
	gamp[5] = fs6;
}

inline void setAmplitudes(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
	gamp[3] = fs4;
	gamp[4] = fs5;
	gamp[5] = fs6;
	gamp[6] = fs7;
}

inline void setAmplitudes(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7, double fs8){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
	gamp[3] = fs4;
	gamp[4] = fs5;
	gamp[5] = fs6;
	gamp[6] = fs7;
	gamp[7] = fs8;
}

inline void setAmplitudes(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7, double fs8, double fs9){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
	gamp[3] = fs4;
	gamp[4] = fs5;
	gamp[5] = fs6;
	gamp[6] = fs7;
	gamp[7] = fs8;
	gamp[8] = fs9;
}

inline void setAmplitudes(double fs1, double fs2, double fs3, double fs4, double fs5, double fs6, double fs7, double fs8, double fs9, double fs10){
	gamp[0] = fs1;
	gamp[1] = fs2;
	gamp[2] = fs3;
	gamp[3] = fs4;
	gamp[4] = fs5;
	gamp[5] = fs6;
	gamp[6] = fs7;
	gamp[7] = fs8;
	gamp[8] = fs9;
	gamp[9] = fs10;
}

inline void showAmplitude(){
	for(int ii=0;ii<MAX_SIZE/2;ii++)std::cout<<Form("amp%d = ",ii)<<gamp[ii];
	std::cout<<std::endl;
	for(int ii=MAX_SIZE/2;ii<MAX_SIZE;ii++)std::cout<<Form("amp%d = ",ii)<<gamp[ii];
	std::cout<<std::endl;
}

inline void exportAmplitude(){
	for(int ii=0;ii<MAX_SIZE;ii++)optPar<<Form("Amplitude.value.%d:   ",ii)<<gamp[ii]<<std::endl;
}

inline void setEnergyCalibrationPars(double fbias1, double fgrad1){
	gCalibPar_b[0] = fbias1;
	gCalibPar_s[0] = fgrad1;
}

inline void setEnergyCalibrationPars(double fbias1, double fgrad1, double fbias2, double fgrad2){
	gCalibPar_b[0] = fbias1;
	gCalibPar_s[0] = fgrad1;
	gCalibPar_b[1] = fbias2;
	gCalibPar_s[1] = fgrad2;
}

inline void setEnergyCalibrationPars(double fbias1, double fgrad1, double fbias2, double fgrad2, double fbias3, double fgrad3){
	gCalibPar_b[0] = fbias1;
	gCalibPar_s[0] = fgrad1;
	gCalibPar_b[1] = fbias2;
	gCalibPar_s[1] = fgrad2;
	gCalibPar_b[2] = fbias3;
	gCalibPar_s[2] = fgrad3;
}

inline void setEnergyCalibrationPars(double fbias1, double fgrad1, double fbias2, double fgrad2, double fbias3, double fgrad3, double fbias4, double fgrad4){
	gCalibPar_b[0] = fbias1;
	gCalibPar_s[0] = fgrad1;
	gCalibPar_b[1] = fbias2;
	gCalibPar_s[1] = fgrad2;
	gCalibPar_b[2] = fbias3;
	gCalibPar_s[2] = fgrad3;
	gCalibPar_b[3] = fbias4;
	gCalibPar_s[3] = fgrad4;
}

inline void setEnergyCalibrationPars(double fbias1, double fgrad1, double fbias2, double fgrad2, double fbias3, double fgrad3, double fbias4, double fgrad4, double fbias5, double fgrad5, double fbias6, double fgrad6, double fbias7, double fgrad7, double fbias8, double fgrad8 ){
	gCalibPar_b[0] = fbias1;
	gCalibPar_s[0] = fgrad1;
	gCalibPar_b[1] = fbias2;
	gCalibPar_s[1] = fgrad2;
	gCalibPar_b[2] = fbias3;
	gCalibPar_s[2] = fgrad3;
	gCalibPar_b[3] = fbias4;
	gCalibPar_s[3] = fgrad4;
	gCalibPar_b[4] = fbias5;
	gCalibPar_s[4] = fgrad5;
	gCalibPar_b[5] = fbias6;
	gCalibPar_s[5] = fgrad6;
	gCalibPar_b[6] = fbias7;
	gCalibPar_s[6] = fgrad7;
	gCalibPar_b[7] = fbias8;
	gCalibPar_s[7] = fgrad8;
}

inline void showEnergyCalibrationPars(){
	for(int ii=0; ii<MAX_SIZE/2;ii++)std::cout<<"bias = "<<gCalibPar_b[ii]<<"; ";
	std::cout<<std::endl;
	for(int ii=MAX_SIZE/2; ii<MAX_SIZE;ii++)std::cout<<"bias = "<<gCalibPar_b[ii]<<"; ";
	std::cout<<std::endl;
	for(int ii=0;ii<MAX_SIZE/2;ii++)std::cout<<"gradient = "<<gCalibPar_s[ii]<<"; ";
	std::cout<<std::endl;
	for(int ii=MAX_SIZE/2;ii<MAX_SIZE;ii++)std::cout<<"gradient = "<<gCalibPar_s[ii]<<"; ";
	std::cout<<std::endl;
}

inline void exportEnergyCalibrationPars(){
	for(int ii=0;ii<MAX_SIZE;ii++){
		optPar<<Form("Energy.calib.bias.%d:       ",ii)<<gCalibPar_b[ii]<<std::endl;
		optPar<<Form("Energy.calib.gradient.%d:   ",ii)<<gCalibPar_s[ii]<<std::endl;
	}
}

inline void applyCalibParsToAll(){
	for(int ii=0;ii<MAX_SIZE;ii++){
		gCalibPar_b[ii] = gCalibPar_b[gFebexCh];
		gCalibPar_s[ii] = gCalibPar_s[gFebexCh];
	}
	std::cout<<"Done!"<<std::endl;
}

inline void setOutputRootFile(const char* fstring){
	gOptRootFileName = fstring;
}

inline void showOutputRootFile(){
	std::cout<<"Output root file: "<<gOptRootFileName<<std::endl;
}

inline void exportOutputRootFile(){
	optPar<<"SaveAsRoot.filename:   "<<gOptRootFileName<<std::endl;
}

inline void enableBaseLineCorr(){
	gBaselineCorr = true;
}

inline void disableBaseLineCorr(){
	gBaselineCorr = false;
}

inline void exportBaselineCorr(){
	optPar<<"Enable.baseline.correction:   "<<gBaselineCorr<<std::endl;
}

inline void enableIntegral(){
	gIntegral = true;
	gMWD = false;
}

inline void disableIntegral(){
	gIntegral = false;
	gMWD = true;
}

inline void exportIntegralSetting(){
	optPar<<"Enable.energyCalculation.integral:   "<<gIntegral<<std::endl;
}

inline void enableAlgorithm(){
	gMWD = true;
	gIntegral = false;
}

inline void disableAlgorithm(){
	gMWD = false;
	gIntegral = true;
}

inline void exportAlgorithmSetting(){
	optPar<<"Enable.energyCalculation.algorithm:   "<<gIntegral<<std::endl;
}

inline void enableLoadTimestamp(){
	glTs = true;
}

inline void disableLoadTimestamp(){
	glTs = false;
}

inline void enableTimeCorr(){
	gTimeCorr = true;
}

inline void disableTimeCorr(){
	gTimeCorr = false;
}

inline void exportTimeCorr(){
	optPar<<"Enable.time.correction:   "<<gTimeCorr<<std::endl;
}

inline void enableSaveAsRoot(){
	fSaveAsRoot = true;
}

inline void disableSaveAsRoot(){
	fSaveAsRoot = false;
}

inline void enableSaveTraces(){
	fsaveTraces = true;
}

inline void disableSaveTraces(){
	fsaveTraces = false;
}

inline bool isOpen(const std::string& name){
	struct stat buffer;
	return (stat(name.c_str(),&buffer) == 0);
}

inline void exportParameters(){
	exportVerbose();
	exportScopeRun();
	exportScopeDataDir();
	exportTsFile();
	exportV1742Data();
	exportFebex4Data();
	exportIsUcesb();
	exportIsGo4();
	exportAnaMode();
	exportMultiModeCh();
	exportIntergralWindow();
	exportNbins();
	exportBaselineCorr();
	exportTimeCorr();
	exportReadAllEntry();
	exportTreeEntry();
	exportTraceAmplitude();
	exportFebexTraceLength();
	exportTrapezWindow();
	exportScopeDataLoading();
	exportTrapezAmpCalcWindow();
	exportTrapezMovingWindowLength();
	exportRisingTime();
	exportSampleRate();
	exportDecaytime();
	exportEnergySpecRange();
	exportIntegralSetting();
	exportAlgorithmSetting();
	exportTimeSpecRange();
	exportTimeHistoRange();
	exportFitParLimit();
	exportCFDreference();
	exportCFDwindow();
	exportCFDpars();
	exportEnergyCalibrationPars();
	exportMeans();
	exportSigmas();
	exportAmplitude();
	exportEnergyRef();
	exportEnergyResolution();
	exportSettingFile();
	exportOutputRootFile();
	exportTotalRunningTime();
	exportSpeed();
	std::cout<<"--->All the parameters are exported."<<std::endl;
	optPar<<"...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo..."<<std::endl;
}

//inline void createSettingFile_Febex(string fname = "settings_Febex_export.set"){
inline void createNewSettingFile_Febex(){
	//fname=gSettings_rename;
	ofstream settings_febex(gSettings_rename,ios::out);//overwrite
	settings_febex<<"#...oooOO0OOooo......oooOO0OOooo.......oooOO0OOooo......oooOO0OOooo..."<<std::endl;
	settings_febex<<"###                    This is a new setting file.                 ###"<<std::endl;
	TTimeStamp ts1;
	settings_febex<<"###  "<<ts1.AsString("l")<<"     ###"<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"#...oooOO0OOooo...           debug section          ...oooOO0OOooo...#"<<std::endl;
	settings_febex<<"#1: general debug purpose; 2: check empty trace event#"<<std::endl;
	settings_febex<<"Verbose:                      "<<gVerbose<<"  #verbos"<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"#...oooOO0OOooo...            path section          ...oooOO0OOooo...#"<<std::endl;
	settings_febex<<"Febex4.data.filename:        "<<gFebex4data<<std::endl;
	settings_febex<<"#new settings will be saved to this file."<<std::endl;
	settings_febex<<"New.setting.file.name:       "<<gSettings_rename<<std::endl;
	settings_febex<<"Enable.save.traces:          "<<fsaveTraces<<" #set as true if you want to export traces to new root file."<<std::endl;
	settings_febex<<"Enable.save.as.root:         "<<fSaveAsRoot<<" #set as true if you want to export results to a new root file"<<std::endl;
	settings_febex<<"#new root file name"<<std::endl;
	settings_febex<<"SaveAsRoot.filename:         "<<gOptRootFileName<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"#...oooOO0OOooo...          algorithm section        ...oooOO0OOooo...#"<<std::endl;
	settings_febex<<"Enable.read.all.entry:       "<<gTreeAllEntry<<" #if ture, read all entries."<<std::endl;
	settings_febex<<"TreeEntry.read.from.0:       "<<gTreeEntry[0]<<" #read event from"<<std::endl;
	settings_febex<<"TreeEntry.read.from.1:       "<<gTreeEntry[1]<<" #read event to"<<std::endl;
	settings_febex<<"Unpack.with.ucesb:           "<<gIsUcesbUnpack<<" #unpack data with ucesb: true; Otherwise, false."<<std::endl;
	settings_febex<<"Unpack.with.go4:             "<<gIsGo4Unpack<<" #unpack data with go4: true; Otherwise, false."<<std::endl;
	settings_febex<<"Subtract.noise.with.baseline:   "<<gSubNoise<<" #useless now."<<std::endl;
	settings_febex<<"Enable.single.channel.analysis.mode: "<<gSingleChAnaMode<<" #only analyze ONE channel"<<std::endl;
	settings_febex<<"Enable.multi.channel.analysis.mode: "<<gMultiChAnaMode<<" #analyze more than one channels"<<std::endl;
	for(int ii=0;ii<16;ii++)settings_febex<<Form("Multimode.read.from.ch.%d:    ",ii)<<gMultiCh[ii]<<" #0-15"<<std::endl;
	for(int ii=0;ii<16;ii++)settings_febex<<Form("Threshold.ch.%d:              ",ii)<<gThreshold[ii]<<std::endl;
	settings_febex<<"Febex4.read.from.ch:         "<<gFebexCh<<" #0-15;working with single mode."<<std::endl;
	settings_febex<<"Febex4.trace.length:         "<<gFebexTraceL<<" #trace length read from raw data"<<std::endl;
	settings_febex<<"Enable.energyCalculation.algorithm:   "<<gMWD<<" #Set 'true' if you want to use MWD method."<<std::endl;
	settings_febex<<"Trapez.Sample.window.0:      "<<gtrapezSampleWindow[0]<<" #it must be larger than (rising time/10);"<<std::endl;
	settings_febex<<"Trapez.Sample.window.1:      "<<gtrapezSampleWindow[1]<<" #[sample points]"<<std::endl;
	settings_febex<<"Trapez.Amp.Calc.window.0:    "<<gTZAmp[0]<<" #[ns]"<<std::endl;
	settings_febex<<"Trapez.Amp.Calc.window.1:    "<<gTZAmp[1]<<" #[ns]"<<std::endl;
	settings_febex<<"Sample.rate[MHz]:            "<<gSampleRate<<" #[MHz]"<<std::endl;
	settings_febex<<"Rising.time[ns]:             "<<gRisingTime<<" #[ns]"<<std::endl;
	settings_febex<<"Decaytime[ns]:               "<<gDecaytime<<" #[ns]"<<std::endl;
	settings_febex<<"Trapez.Moving.Window.Length[ns]:   "<<gtrapezMovingWindowLength<<" #[ns]"<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"Enable.baseline.correction:   "<<gBaselineCorr<<" #if true, baseline will be forced to move to 0."<<std::endl;
	settings_febex<<"Enable.time.correction:       "<<gTimeCorr<<" #useless"<<std::endl;
	settings_febex<<"CFD.reference:                "<<gCFDref<<" #used in time drift correction"<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"#...oooOO0OOooo...         display section          ...oooOO0OOooo...#"<<std::endl;
	settings_febex<<"Energy.histogram.range.0:     "<<ghMIN<<" #"<<std::endl;
	settings_febex<<"Energy.histogram.range.1:     "<<ghMAX<<" #"<<std::endl;
	settings_febex<<"Histogram.bins:               "<<gNbins<<" #"<<std::endl;
	settings_febex<<"Energy.spectrum.range.0:      "<<gESrange[0]<<" #"<<std::endl;
	settings_febex<<"Energy.spectrum.range.1:      "<<gESrange[1]<<" #"<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"Time.histogram.range.0:       "<<ghTMIN<<" #"<<std::endl;
	settings_febex<<"Time.histogram.range.1:       "<<ghTMAX<<" #"<<std::endl;
	settings_febex<<"Time.spectrum.range.0:        "<<gTSrange[0]<<" #"<<std::endl;
	settings_febex<<"Time.spectrum.range.1:        "<<gTSrange[1]<<" #"<<std::endl;
	settings_febex<<"CFD.window.0:                 "<<gCFDwin[0]<<" #find zero point inside this window"<<std::endl;
	settings_febex<<"CFD.window.1:                 "<<gCFDwin[1]<<" #"<<std::endl;
	settings_febex<<"CFD.pars.fraction:            "<<gFra<<" #"<<std::endl;
	settings_febex<<"CFD.pars.delay:               "<<gDel<<" #[sample points]"<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"Trace.graph.Y.min:            "<<gTraceAmp[0]<<" #"<<std::endl;
	settings_febex<<"Trace.graph.Y.max:            "<<gTraceAmp[1]<<" #"<<std::endl;
	settings_febex<<std::endl;
	settings_febex<<"#...oooOO0OOooo...     fit&&calibration section     ...oooOO0OOooo...#"<<std::endl;
	settings_febex<<"Fit.Pars.Limit:               "<<gParLimit<<" #used to restrict fitting parameters"<<std::endl;
	std::cout<<"--->New settings have been written to: "<<gSettings_rename<<std::endl;

}

inline void setEnt(){
	gROOT->SetStyle("Modern");
	//gStyle->SetHistFillColor(7);
	//gStyle->SetHistFillStyle(3002);
	//gStyle->SetHistLineColor(kBlue);
	//*** FAIR style ***//
	gStyle->SetHistFillColor(796);
	gStyle->SetHistFillStyle(1001);
	gStyle->SetHistLineColor(kBlack);
	gStyle->SetFuncColor(kRed);
	gStyle->SetFrameLineWidth(2);
	gStyle->SetCanvasColor(0);
	gStyle->SetTitleFillColor(0);
	gStyle->SetTitleStyle(0);
	gStyle->SetStatColor(0);
	gStyle->SetStatStyle(0);
	gStyle->SetStatX(0.9);  
	gStyle->SetStatY(0.9);  
	gStyle->SetPalette(1);
	//gStyle->SetOptLogz(1);
	//  gStyle->SetOptTitle(0);
	//gStyle->SetOptFit(1);
	gStyle->SetOptStat(1111111);
	//gStyle->SetOptStat(0);
	//gStyle->SetPadBorderMode(1);
	//  gStyle->SetOptDate(1);
	gStyle->SetLabelFont(132,"XYZ");
	gStyle->SetLabelSize(0.05,"X");
	gStyle->SetLabelSize(0.05,"Y");
	gStyle->SetLabelOffset(0.004);
	gStyle->SetTitleOffset(1.05);
	gStyle->SetTitleFont(132,"XYZ");
	gStyle->SetTitleSize(0.045,"X");
	gStyle->SetTitleSize(0.045,"Y");
	//gStyle->SetTitleFont(132,"");
	gStyle->SetTextFont(132);
	gStyle->SetStatFont(132);
}

inline Double_t XtoPad(Double_t x)
{
	Double_t xl,yl,xu,yu;
	gPad->GetPadPar(xl,yl,xu,yu);
	Double_t pw = xu-xl;
	Double_t lm = gPad->GetLeftMargin();
	Double_t rm = gPad->GetRightMargin();
	Double_t fw = pw-pw*lm-pw*rm;
	return (x*fw+pw*lm)/pw;
}

inline Double_t YtoPad(Double_t y)
{
	Double_t xl,yl,xu,yu;
	gPad->GetPadPar(xl,yl,xu,yu);
	Double_t ph = yu-yl;
	Double_t tm = gPad->GetTopMargin();
	Double_t bm = gPad->GetBottomMargin();
	Double_t fh = ph-ph*bm-ph*tm;
	return (y*fh+bm*ph)/ph;
}

//const
inline Double_t fitConst(Double_t *xx, Double_t *fpar){
	return fpar[0] + xx[0] * 0.0;
}
//gaus
inline Double_t fitGaus1(Double_t *xx, Double_t *fpar){
	return fpar[0]*TMath::Exp(-0.5*TMath::Power( (xx[0]-fpar[1])/fpar[2],1) );
}

//const + gaus; 4pars
inline Double_t gauspeaks1(Double_t *xx, Double_t *fpar){
	return fitConst(xx,fpar) + fitGaus1(xx,&fpar[1]);
}

#endif
