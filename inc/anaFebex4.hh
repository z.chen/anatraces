//
//last updated @ 15 Jan,2024
//Z.Chen
//
//Error6xx.
//

//********release notes****************//
//21 Feb, 2023
//add timer to getTracesFebex4();
//some updates on raw traces display
//10 Mar, 2023
//comment those parts related to checkLastPoint();
//export raw traces to root file;(previously, only export traces after filter)
//11 Mar
//add gFebexTraceL to restrict trace length
//add isFebex
//17 Nov, 2023
//add ucesb
//subtract baseline noise
//24 Nov
//optimize tree entry reading
//02 Dec
//optimize display of running speed
//05 Dec
//optimize speed calculation; add total running time
//24 Dec
//traces are in UInt_t type when using ucesb.
//25 Dec
//simplify getsingletrace and gettraces()
//26 Dec
//add offlineAnalysis()
//30 Dec
//optimize energy calib
//15 Jan, 2024
//support data reading from 16 feb channels
//correct saveasroot()
//19 Jan
//add unpacked TRIGGER to anaroot
//

#ifndef _ANAFEBEX4_HH_
#define _ANAFEBEX4_HH_

#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>

#include "viewtrace.hh"
#include "anaUcesb.hh"
#include "anaGo4.hh"

TTree* loadFromFebex4(){
	std::cout<<"--->Load data from "<<gFebex4data<<std::endl;
	TFile *iptfile = new TFile(gFebex4data);
	if(!iptfile->IsOpen()){
		std::cout<<"Error602!!! Can not open/find data file"<<gFebex4data<<std::endl;
		return 0;
	}
	TTree *iptTree = 0;
	if(gIsGo4Unpack)iptTree	= (TTree*)iptfile->Get("tree");
	if(gIsUcesbUnpack)iptTree	= (TTree*)iptfile->Get("h101");
	if(!iptTree){
		std::cout<<"Error603!!! Can not find tree"<<std::endl;
		return 0;
	}
	if(gIsGo4Unpack)InitGo4Tree(iptTree);
	if(gIsUcesbUnpack)InitUcesbTree(iptTree);
	return iptTree;
}


void testFebex4Tree(TTree* ftree){
	if (!ftree) {
		std::cout<<"Error604!!! No tree. PLease check input file"<<std::endl;
		return;
	}
	if(gIsGo4Unpack)ftree->Draw("feb4_trace_amp[0]:feb4_trace_pts[0]>>(300,0,3000,1000,0,10000)","","colz");
	if(gIsUcesbUnpack)ftree->Draw("lisa_data1traces1v:lisa_data1traces1I>>(300,0,3000,1000,0,10000)","","colz");
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...
// read data from tree and get ONE trace //
TGraph* getSingleTraceFebex4(TTree *ftree, Long64_t jentry, bool fdraw=false, UInt_t fchannel = 0, UInt_t *flength = 0, UInt_t *fevtno = 0, UInt_t *ftrig = 0){
	if(gVerbose > 0)std::cout<<"--->Start to read single trace from event "<<jentry<<std::endl;
	if (!ftree) {
		std::cout<<"Error605!!! No tree. PLease check input file"<<std::endl;
		return 0;
	}
	if(gSingleTraceGraph!=NULL){
		delete gSingleTraceGraph;
		gSingleTraceGraph = NULL;
	}
	isFebex = true;
	for(int ii=0;ii<16;ii++)gBadEntry[ii] = 0;

	vector<double>yy;//amplitude
	vector<double>xx;//sample
	yy.clear();
	xx.clear();
	if(gVerbose == 1)std::cout<<"xx,yy clear"<<std::endl;
	ftree->GetEntry(jentry);

	vector<UInt_t>len;//vector for trace length;directly read from unpacked data
	vector<UInt_t*>lisa_data_v;//vector for pointers
	vector<UInt_t*>lisa_data_I;
	len.clear();//length
	lisa_data_v.clear();
	lisa_data_I.clear();
	if(gIsUcesbUnpack){//now, it supports for 16 channels
		len.push_back(lisa_data1traces1);
		len.push_back(lisa_data1traces2);
		len.push_back(lisa_data1traces3);
		len.push_back(lisa_data1traces4);
		len.push_back(lisa_data1traces5);
		len.push_back(lisa_data1traces6);
		len.push_back(lisa_data1traces7);
		len.push_back(lisa_data1traces8);
		len.push_back(lisa_data1traces9);
		len.push_back(lisa_data1traces10);
		len.push_back(lisa_data1traces11);
		len.push_back(lisa_data1traces12);
		len.push_back(lisa_data1traces13);
		len.push_back(lisa_data1traces14);
		len.push_back(lisa_data1traces15);
		len.push_back(lisa_data1traces16);

		lisa_data_v.push_back(lisa_data1traces1v);
		lisa_data_v.push_back(lisa_data1traces2v);
		lisa_data_v.push_back(lisa_data1traces3v);
		lisa_data_v.push_back(lisa_data1traces4v);
		lisa_data_v.push_back(lisa_data1traces5v);
		lisa_data_v.push_back(lisa_data1traces6v);
		lisa_data_v.push_back(lisa_data1traces7v);
		lisa_data_v.push_back(lisa_data1traces8v);
		lisa_data_v.push_back(lisa_data1traces9v);
		lisa_data_v.push_back(lisa_data1traces10v);
		lisa_data_v.push_back(lisa_data1traces11v);
		lisa_data_v.push_back(lisa_data1traces12v);
		lisa_data_v.push_back(lisa_data1traces13v);
		lisa_data_v.push_back(lisa_data1traces14v);
		lisa_data_v.push_back(lisa_data1traces15v);
		lisa_data_v.push_back(lisa_data1traces16v);

		lisa_data_I.push_back(lisa_data1traces1I);
		lisa_data_I.push_back(lisa_data1traces2I);
		lisa_data_I.push_back(lisa_data1traces3I);
		lisa_data_I.push_back(lisa_data1traces4I);
		lisa_data_I.push_back(lisa_data1traces5I);
		lisa_data_I.push_back(lisa_data1traces6I);
		lisa_data_I.push_back(lisa_data1traces7I);
		lisa_data_I.push_back(lisa_data1traces8I);
		lisa_data_I.push_back(lisa_data1traces9I);
		lisa_data_I.push_back(lisa_data1traces10I);
		lisa_data_I.push_back(lisa_data1traces11I);
		lisa_data_I.push_back(lisa_data1traces12I);
		lisa_data_I.push_back(lisa_data1traces13I);
		lisa_data_I.push_back(lisa_data1traces14I);
		lisa_data_I.push_back(lisa_data1traces15I);
		lisa_data_I.push_back(lisa_data1traces16I);
	}
	//*** test ***//
	//UInt_t *pp_v1 = lisa_data_v[0];
	//for(int ii=0;ii<10;ii++)std::cout<<"test vector: "<<pp_v1[ii]<<std::endl;

	if(gVerbose > 0)std::cout<<"read from raw data: lisa_dataxxx[501] = "<<lisa_data1traces1v[501]<<std::endl;
	//cout<<"febex ch = "<<fchannel<<std::endl;
	int cnt = 0;
	if(fchannel>15){//ch0-7
		std::cout<<"Error607!!! Febex channel number is not valid. Now it only supports 16 channels. Please check your setting file."<<std::endl;
		return 0;
	}
	//*** get trigger mode ***//
	ftrig[0] = TRIGGER;
	//*** get event num ***//
	fevtno[0] = EVENTNO;
	//*** get trace length ***//
	flength[0] = len[fchannel];//length

	//*** get a trace ***//
	UInt_t *p_v = lisa_data_v[fchannel];//y axis
	UInt_t *p_I = lisa_data_I[fchannel];//x axis

	for(int i=0;i<gFebexTraceL;i++){
		// **** Go4 **** //
		if(gIsGo4Unpack)yy.push_back(feb4_trace_amp[fchannel][i]);
		if(gIsGo4Unpack)xx.push_back(i*10);
		// **** Ucesb **** //
		if(gIsUcesbUnpack){
			if( p_v[i] == 0 ){
				gBadEntry[fchannel] = true;
				if(gVerbose==2&&cnt<1)std::cout<<"empty trace in event "<<jentry<<std::endl;
				if(gVerbose==2&&cnt<1)std::cout<<"lisa_data1traces1v"<<std::endl;
				cnt++;
			}
			yy.push_back(p_v[i]);
			xx.push_back(p_I[i]*10);//samples to ns
			p_v[i] = 0;//in general, the raw baseline is at ~8k;
			p_I[i] = i+1;
		}
	}
	p_v=NULL;
	p_I=NULL;
	if(gVerbose > 0)std::cout<<"read from raw data: yy[501] = "<<yy[501]<<std::endl;
	if(gVerbose > 0)std::cout<<"read from raw data: xx[501] = "<<xx[501]<<std::endl;
	//TGraph *grtem = new TGraph(yy.size(),&xx[0],&yy[0]);
	gSingleTraceGraph = new TGraph(yy.size(),&xx[0],&yy[0]);
	if(fdraw)gSingleTraceGraph->Draw("AP*");
	gSingleTraceGraph->SetTitle(Form("Febex4_Ch%d: 10ns/Sample",0));
	gSingleTraceGraph->GetXaxis()->SetTitle("Samples");
	gSingleTraceGraph->GetYaxis()->SetTitle("Amplitude[arb.]");
	if(gVerbose == 1)std::cout<<"--->end of reading single trace from event "<<jentry<<std::endl;
	return gSingleTraceGraph;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...
TGraph* offlineAnalysis(TTree* ftree, UInt_t jentry, UInt_t fchannel, double *ftiming, double *rawE_tem ){

	if (!ftree) {
		std::cout<<"Error608!!! No tree. Please check input file"<<std::endl;
		return 0;
	}
	TGraph *fgrsin=0;
	UInt_t ftrigger[1]={0};//trigger mode from c4
	UInt_t fevtno[1]={0};//event no from c4
	UInt_t flength[1]={0};//trace length;

	//*** start to read data from raw root file ***//
	if(gTimeCorr){
		fgrsin	= timeDriftCorr( baselineCorr(getSingleTraceFebex4(ftree,jentry,false,fchannel,flength,fevtno,ftrigger),false),false );
	}else if(gBaselineCorr && !gTimeCorr){
		fgrsin = baselineCorr(getSingleTraceFebex4(ftree,jentry,false,fchannel,flength,fevtno,ftrigger),false);
	}
	else{
		fgrsin = getSingleTraceFebex4(ftree,jentry,false,fchannel,flength,fevtno,ftrigger);
	}
	if(fgrsin == 0)return 0;
	//*****get time info*****//
	if( !gBadEntry[fchannel] ){
		ftiming[0] = getTimingCFD(fgrsin);
		gCFDtime.push_back(ftiming[0]);
		//std::cout<<"test CFD time: "<<ftiming[0]<<std::endl;
	}
	//****************** determine energy**************//
	if(gIntegral){
		rawE_tem[0] = Integrate(fgrsin);
	}
	else if(gMWD){
		//if(checkLastPoint(MWD(fgrsin))){
		rawE_tem[0] = getEdg3(fgrsin);
		//gRawE[jentry] = getEdg2(fgrsin);
		//gRawE[jentry] = getEdg(fgrsin);
		if(rawE_tem[0]<0)rawE_tem[0]*=(-1);
		//}
	}else{
		std::cout<<"Warning607!!! Please select energy calculation method."<<std::endl;return 0;

	}
	if(gFebexCh>15){
		std::cout<<"Error610!!! gFebexCh can not > 15."<<std::endl;
		return 0;
	}
	double fEnergy = gCalibPar_b[fchannel] + rawE_tem[0] * gCalibPar_s[fchannel];//calibration

	//*** debug ***//
	//std::cout<<"raw energy = "<<rawE_tem[0]<<std::endl;
	//std::cout<<"cali energy = "<<fEnergy<<std::endl;
	Long64_t fts=-1;
	dataCluster dC_opt;
	if(fSaveAsRoot){
		dC_opt.trigger = ftrigger[0];
		dC_opt.evtno = fevtno[0];
		dC_opt.fch = fchannel;
		dC_opt.fentry = jentry;
		if(!gBadEntry[fchannel])dC_opt.fentry_empty = jentry;
		dC_opt.fts = fts;//useless now

		//if(fchannel > 3)
		//{
		//	dC_opt.fenergy_raw = gRandom->Gaus(rawE_tem[0]+rawE_tem[0]/50.,60);
		//}else
		//{
		//	//dC_opt.fenergy_raw = gRandom->Gaus(rawE_tem[0],1);
		//	dC_opt.fenergy_raw = rawE_tem[0];
		//}
		dC_opt.fenergy_raw = rawE_tem[0];
		if(rawE_tem[0]>OVERFLOW_thre)dC_opt.foverflow = 1;
		dC_opt.fenergy_cal = fEnergy;
		if(gCFDtime.size()>0)dC_opt.ftime_cfd = gCFDtime.back();
		dC_opt.flength = flength[0];//trace length read from unpacked data
		dC_opt.fgr = fgrsin;
	}
	if(fSaveAsRoot)saveAsRoot(dC_opt);
	//if(fSaveAsRoot && dC_opt.fenergy_raw > gThreshold[fchannel])saveAsRoot(dC_opt);
	return fgrsin;
}
//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...
void getTracesFebex4(TTree* ftree, UInt_t fchannel = 0){
	if (!ftree) {
		std::cout<<"Error606!!! No tree. Please check input file"<<std::endl;
		return;
	}
	//*** print out; begin ***//
	if(gSingleChAnaMode)std::cout<<"--->Single-analysis mode. Read from Febex channel(0-15): "<<fchannel<<std::endl;
	if(gMultiChAnaMode)std::cout<<"--->Multi-analysis mode, turn off GUI."<<std::endl; 
	if(fsaveTraces && fSaveAsRoot)std::cout<<"--->Export traces to a new root file. "<<std::endl;
	if(fSaveAsRoot)std::cout<<"--->Export results to a new root file: "<<gOptRootFileName<<std::endl;
	Long64_t fnentries = ftree->GetEntriesFast();
	std::cout<<"--->Total events: "<<std::endl;
	std::cout<<fnentries<<std::endl;
	if(gTreeAllEntry)std::cout<<"--->Read all entries: enable"<<std::endl;
	if(!gTreeAllEntry)std::cout<<"--->Read entries from:"<<gTreeEntry[0]<<" to "<<gTreeEntry[1]<<std::endl;
	//*** print out; end ***//

	if(fSaveAsRoot)initRoot();//init output root file

	//*** declear new local variables ***//
	TGraph *fsingleTrace=0;//corrected tracexs;
	double rawE_tem[1] ={-1};
	double ftiming[1]={-1};
	//UInt_t flen[1]={0};//trace length
	//*** for display ***//
	vector<TGraph*>graphs;//graphs temporary container; used for display
	graphs.clear();
	gCFDtime.clear();//CFD timing container;used for display
	gRawE.clear();//used for display: showenergyspec();
	gRawTraces.clear();//used for display: showrawtraces();

	TStopwatch timer;
	TStopwatch timer2;//start before the loop; to calculate total running time
	vector<float>v_averageSpeed;
	v_averageSpeed.clear();

	//*** main loop ***//
	//*** analyze data event by event ***//
	for(Long64_t jentry=gTreeEntry[0];jentry<fnentries;jentry++){
		//stopwatch
		if(jentry%100==0){
			timer.Reset();
			timer.Start();
		}

		rawE_tem[0] = -1;
		ftiming[0] = -1;
		if(fSaveAsRoot)clearRoot();//clear

		//*** offline analysis in an event-by-event base ***//
		if(gMultiChAnaMode)for(int cc=0;cc<16;cc++){
			if(!gMultiCh[cc])continue;//cc means fchannel; by default false
			fsingleTrace = offlineAnalysis(ftree,jentry,cc,ftiming,rawE_tem);
			if(gVerbose==1)std::cout<<"gMultiCh = "<<cc<<"; enable? "<<gMultiCh[cc]<<std::endl;
		}
		if(gSingleChAnaMode)fsingleTrace = offlineAnalysis(ftree,jentry,fchannel,ftiming,rawE_tem);

		//*** write data to a new root file ***//
		if(fSaveAsRoot)optTree->Fill();

		//*** save analyzed results into global variables ***//
		if(gSingleChAnaMode)gRawTraces.push_back(fsingleTrace);//used for display: showrawtraces();
		if(gSingleChAnaMode)gRawE[jentry] = rawE_tem[0];//used for display: showenergyspec();
														//****************** fill graph vector**************//
		if(gSingleChAnaMode){
			if(jentry%6000==0){
				graphs.clear();//buffer protection;
				std::cout<<"--->Current limit: "<<6000<<". Graph is full. Empty graph."<<std::endl;}
			if(gIntegral){
				graphs.push_back(fsingleTrace);
			}
			else if(gMWD){
				//if(checkLastPoint(MWD(fsingleTrace))){
				graphs.push_back(MWD(fsingleTrace));
				//}
			}else{
				std::cout<<"Warning608!!! Please select energy calculation method."<<std::endl;
				return;
			}
		}
		//*** calculate running time ***//
		double fspeed;
		double fduration;
		if(jentry%100==99){
			fduration = timer.RealTime();//
			fspeed = 100.0/fduration;// [evt/sec]
			v_averageSpeed.push_back(fspeed);
		}
		double ftimeToGo = 0;
		if(gTreeAllEntry){
			if(jentry%100==99)ftimeToGo = (fnentries-jentry)/fspeed;
		}else{
			if(jentry%100==99)ftimeToGo = (gTreeEntry[1]-jentry)/fspeed;
		}

		if(jentry%100==99)std::cout<<"*Current speed: "<<fspeed<<"[evt/s]; Duration for 100 evts: "<<fduration<<", Num:"<<jentry<<"/"<<fnentries<<"  "<<ftimeToGo<<"s to go.";
		fflush(stdout);
		printf("\r");
		if(jentry>=gTreeEntry[1] && !gTreeAllEntry)break;
	}
	//*** end of loop ***//

	//*** display *** //
	//float faverageSpeed = std::reduce(std::execution::par, v_averageSpeed.begin(),v_averageSpeed.end())/v_averageSpeed.size();//c++17
	gAverageSpeed = std::accumulate(v_averageSpeed.begin(),v_averageSpeed.end(),0.0)/v_averageSpeed.size();
	gTotalRunningTime = timer2.RealTime();
	std::cout<<std::endl;
	std::cout<<"--->Total running time: "<<gTotalRunningTime<<" s."<<std::endl;
	std::cout<<"--->Average speed     : "<<gAverageSpeed<<" [evt/s]."<<std::endl;
	if( gBaselineCorr && !gTimeCorr )std::cout<<"--->Baseline correction: On."<<std::endl;
	if( gTimeCorr )std::cout<<"--->Time correction: On."<<std::endl;
	std::cout<<"N = "<<graphs.size()<<" trapez have been added to TGraphs."<<std::endl;
	std::cout<<"complete!"<<std::endl;
	if(gMultiChAnaMode)return;
	//*** plot the traces after MWD under single mode ***//
	if(graphs.size()>0){
		graphs.at(0)->Draw("APL");
		graphs.at(0)->SetTitle("Febex Traces: 10ns/Sample");
		graphs.at(0)->GetXaxis()->SetTitle("Time[ns]");
		graphs.at(0)->GetYaxis()->SetTitle("Amplitude[arb.]");
		graphs.at(0)->SetMaximum(gTraceAmp[1]);
		graphs.at(0)->SetMinimum(gTraceAmp[0]);
		for(int i=0;i<graphs.size();i++){
			graphs.at(i)->SetLineColor(1+i%40);
			graphs.at(i)->Draw("PL");
		}
	}
}

#endif
