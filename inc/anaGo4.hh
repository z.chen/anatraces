//
//last updated@17 Nov,2023
//Z.Chen
//
//Error6xx.
//

//********release notes****************//
//

#ifndef _ANAGO4_HH_
#define _ANAGO4_HH_

#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>

#include "viewtrace.hh"
#include "anaUcesb.hh"


// Declaration of leaf types
//Double_t        tracesX[10000];
//Double_t        tracesY[10000];
//Double_t        feb4_trace_raw[16][10000];
Double_t        feb4_trace_amp[16][10000]; //!unpacked by Go4

// List of branches
//TBranch        *b_tracesX;   //!
//TBranch        *b_tracesY;   //!
//TBranch        *b_feb4_trace_raw;   //!
TBranch        *b_feb4_trace_amp;   //!unpacked by Go4

void InitGo4Tree(TTree *ftree)
{
	// Set branch addresses and branch pointers
	if (!ftree) {
		std::cout<<"Error601!!! No tree. PLease check input file"<<std::endl;
		return;
	}

	//ftree->SetBranchAddress("tracesX", tracesX, &b_tracesX);
	//ftree->SetBranchAddress("tracesY", tracesY, &b_tracesY);
	//ftree->SetBranchAddress("feb4_trace_raw[16][10000]", feb4_trace_raw, &b_feb4_trace_raw);
	ftree->SetBranchAddress("feb4_trace_amp[16][10000]", feb4_trace_amp, &b_feb4_trace_amp);
}
#endif
