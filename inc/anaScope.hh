//
//last updated@ 02 Dec,2023
//Z.Chen
//
//Error3xx
//

//********release notes****************//
//29 Jan, 2023
//optimize loadFromTXT() for trapez;
//21 Feb
//some updates related to display
//10 Mar, 2023
//comment those parts related to checkLastPoint();
//export raw traces to root file;(previously, only export traces after filter)
//17 Apr, 2023
//optimize data loading
//02 Dec
//add T_cfd to tree
//30 Dec
//optimize energy calib
//
//
#ifndef _ANASCOPE_HH_
#define _ANASCOPE_HH_

#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>
#include <iostream>
#include <fstream>

#include "viewtrace.hh"

bool loadFromTXT(int run, vector<double>&time_ns, vector<double>&amplitude){
	ifstream fFile;
	TString fName = Form("%d.txt",run);
	fName = gTxtDir + fName;
	if(gVerbose == 1)std::cout<<"Load data from "<<fName<<std::endl;
	fFile.open(fName);
	if(!fFile.is_open()){
		std::cout<<"Error301! Can not open file "<<fName<<std::endl;
		return false;
	}
	time_ns.clear();
	amplitude.clear();
	int cnt = 0;
	//showScopeDataLoading();
	//showTrapezWindow();
	while(!fFile.eof()){
		double t, a;
		fFile >> t >> a;
		if(gVerbose == 1 && cnt<10)std::cout<<"t = "<<t<<" a = "<<a<<std::endl;
		if(gVerbose == 1 && cnt<1 )std::cout<<"read from pts = "<<gScopeDataLoading[0]<<" to = "<<gScopeDataLoading[1]<<std::endl;
		cnt++;
		fFile.ignore(100,'\n');
		//if(cnt>gScopeDataLoading[1])break;
		if(cnt<gScopeDataLoading[0] || cnt>gScopeDataLoading[1])continue;
		//if( cnt>gtrapezSampleWindow[1]+1000)break;
		time_ns.push_back(t*ns);
		amplitude.push_back(a*mV);
	}
	if(gVerbose == 1)std::cout<<"test: "<<time_ns.back()<<" "<<amplitude.back()<<std::endl;
	return true;
}

bool loadTimestamp(){
	ifstream fFile;
	TString fName = gTxtDir + gTsfilename;
	if(gVerbose == 1)std::cout<<"Load timestamp data from "<<fName<<std::endl;
	fFile.open(fName);
	if(!fFile.is_open()){
		std::cout<<"Error303! Can not open file "<<fName<<std::endl;
		return false;
	}
	gTs.clear();
	//gTs_S.clear();
	int ientry = 1;
	while(!fFile.eof()){
		Long64_t ts;
		fFile >> ts;
		fFile.ignore(100,'\n');
		gTs[ientry]=ts;
		//gTs_S[ientry]=ts%1000000000000;
		ientry++;
	}
	return true;
}

TGraph* getSingleTrace(vector<double>xx, vector<double>yy, int run, bool fplot= false){
	if(xx.size()<1){
		std::cout<<"Error302!! empty vector, please check your input data."<<std::endl;
		return 0;
	}
	TGraph* grtem = new TGraph(xx.size(),&xx[0],&yy[0]);
	if(fplot){
		grtem->Draw("AP*");
	}
	grtem->SetTitle(TString::Format("Traces:Run%d_800ps/Simple",run));
	//grtem->SetTitle(TString::Format("Traces:Run%d_80ps/Simple",run));
	grtem->GetYaxis()->SetTitle("Amplitude[mV]");
	grtem->GetXaxis()->SetTitle("Time[ns]");
	//grtem->SetMaximum(2000);
	//grtem->SetMinimum(-2000);
	grtem->SetMaximum(200);
	grtem->SetMinimum(-200);
	return grtem;
}

void ViewTraces(int RUN1 = -1, int RUN2 = 1290,bool fSHOW = true){

	vector<TGraph*> graphs;
	//*****load data*****//
	vector<double> xx;
	vector<double> yy;
	vector<Long64_t> fts;
	xx.clear();
	yy.clear();
	fts.clear();

	if(fSaveAsRoot)initRoot();
	double ftiming[1]={-1};
	std::cout<<"running...Please wait."<<std::endl;
	std::cout<<"========================="<<std::endl;
	graphs.clear();
	gRawE.clear();
	gRawTraces.clear();
	//gTs.clear();
	//gTs_S.clear();//ts short
	gCFDtime.clear();
	if(RUN1 != -1){
		gRun[0] = RUN1;
		gRun[1] = RUN2;
	}
	//std::cout<<"test : run1 "<<gRun[0]<<std::endl;
	//std::cout<<"test : run2 "<<gRun[1]<<std::endl;
	//std::cout<<"test: vector size = "<<graphs.size()<<"  "<<gRawE.size()<<std::endl;
	TStopwatch timer;
	for(int run = gRun[0];run < gRun[1];run++){
		timer.Start();
		//*****read data from txt file*****//
		if(!loadFromTXT(run,xx,yy))continue;
		if(glTs)loadTimestamp();
		//*****save a single trace to the graph*****//
		TGraph *fgrsin;
		if(gBaselineCorr && !gTimeCorr){
			fgrsin = baselineCorr( getSingleTrace(xx,yy,run,false),false );
		}else if(gTimeCorr){
			fgrsin = timeDriftCorr( baselineCorr( getSingleTrace(xx,yy,run,false), false),false);
		}
		else{
			fgrsin = getSingleTrace(xx,yy,run,false);
		}
		if(!fgrsin)continue;
		//*****get time info*****//
		ftiming[0] = getTimingCFD(fgrsin);
		gCFDtime.push_back(ftiming[0]);
		//***** save raw traces into map *****//
		gRawTraces.push_back(fgrsin);
		//****************** determine energy**************//
		if(gIntegral){
			graphs.push_back(fgrsin);gRawE[run]=Integrate(graphs.back());//map<int,double>;calculate energy
		}else if(gMWD){
			//if(checkLastPoint(MWD(fgrsin))){
				graphs.push_back(MWD(fgrsin));
				gRawE[run] = getEdg3(graphs.back());
				//gRawE[run] = getEdg2(graphs.back());
				//gRawE[run] = getEdg(graphs.back());
				if(gRawE[run]<0)gRawE[run]*=(-1);
			//}
		}else{
			std::cout<<"Warning301!!! Please select energy calculation method."<<std::endl;return;
		}
		//std::cout<<"rawE = "<<gRawE[run]<<std::endl;
	if(gFebexCh>15){
		std::cout<<"Error304!!! gFebexCh can not > 15."<<std::endl;
		return;
	}
		double fEnergy = gCalibPar_b[gFebexCh] + gRawE[run] * gCalibPar_s[gFebexCh];//calibration
		//if(fSaveAsRoot)saveAsRoot(optTree,run,gTs[run],gRawE[run],fEnergy,graphs.back());
		if(fSaveAsRoot)saveAsRoot(optTree,run,gTs[run],gRawE[run],fEnergy,gCFDtime[run],gRawTraces.back());
		timer.Stop();
		double fspeed = timer.RealTime();
		double ftimeToGo = (gRun[1]-run)*fspeed;
		//std::cout<<"Speed: "<<fspeed<<" Time to go..."<<ftimeToGo<<std::endl;
		if(run%10==0)std::cout<<"Num:"<<run<<"/"<<gRun[1]<<"  "<<ftimeToGo<<"s to go.";
		fflush(stdout);
		printf("\r");
	}
	if( gBaselineCorr && !gTimeCorr )std::cout<<"--->Baseline correction: On."<<std::endl;
	if( gTimeCorr )std::cout<<"--->Time correction: On."<<std::endl;
	std::cout<<"========================="<<std::endl;
	std::cout<<gRawE.size()<<" traces loaded."<<std::endl;
	std::cout<<"complete!"<<std::endl;
	if(fSaveAsRoot)closeRoot();
	//if(fSaveAsRoot)closeRoot(optTree,optfile);
	if(!fSHOW)return;

	if(gBaselineCorr)std::cout<<"--->With baseline correction."<<std::endl;
	if(graphs.size()>0){
		graphs.at(0)->Draw("APL");
		graphs.at(0)->SetTitle(TString::Format("Traces_%dns/Simple",(int)(1000/gSampleRate) ) );
		graphs.at(0)->GetXaxis()->SetTitle("Time[ns]");
		graphs.at(0)->GetYaxis()->SetTitle("Amplitude[mV]");
		//graphs.at(0)->SetTitle("Traces_80ps/Simple");
		graphs.at(0)->SetMaximum(gTraceAmp[1]);
		graphs.at(0)->SetMinimum(gTraceAmp[0]);
		for(int i=0;i<graphs.size();i++){
			graphs.at(i)->SetLineColor(1+i%40);
			graphs.at(i)->Draw("PL");
		}
	}
}

#endif
