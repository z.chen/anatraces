//
//last updated@15 Jan,2024
//Z.Chen
//
//Error7xx.
//

//********release notes****************//
//17 Nov, 2023
//Anytime if the branch structure in the tree was changed, please update the related variables in the following lines:
//In anaFebex4.hh
//line 60,83,87
//15 Jan
//update on the init of branch
//17 Jan
//add EVENTNO
//

#ifndef _ANAUCESB_HH_
#define _ANAUCESB_HH_

#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>

// Declaration of leaf types
UInt_t          TRIGGER;
UInt_t          EVENTNO;
UInt_t          lisa_ts_subsystem_id;
UInt_t          lisa_ts_t1;
UInt_t          lisa_ts_t2;
UInt_t          lisa_ts_t3;
UInt_t          lisa_ts_t4;
UInt_t          lisa_data1event_trigger_time_hi;
UInt_t          lisa_data1event_trigger_time_lo;
UInt_t          lisa_data1hit_pattern;
UInt_t          lisa_data1num_channels_fired;
UInt_t          lisa_data1channel_id;
UInt_t          lisa_data1channel_idI[4];   //[lisa_data1channel_id]
UInt_t          lisa_data1channel_idv[4];   //[lisa_data1channel_id]
UInt_t          lisa_data1channel_trigger_time_hi;
UInt_t          lisa_data1channel_trigger_time_hiI[4];   //[lisa_data1channel_trigger_time_hi]
UInt_t          lisa_data1channel_trigger_time_hiv[4];   //[lisa_data1channel_trigger_time_hi]
UInt_t          lisa_data1channel_trigger_time_lo;
UInt_t          lisa_data1channel_trigger_time_loI[4];   //[lisa_data1channel_trigger_time_lo]
UInt_t          lisa_data1channel_trigger_time_lov[4];   //[lisa_data1channel_trigger_time_lo]
UInt_t          lisa_data1pileup1;
UInt_t          lisa_data1pileup2;
UInt_t          lisa_data1pileup3;
UInt_t          lisa_data1pileup4;
UInt_t          lisa_data1pileup5;
UInt_t          lisa_data1pileup6;
UInt_t          lisa_data1pileup7;
UInt_t          lisa_data1pileup8;
UInt_t          lisa_data1pileup9;
UInt_t          lisa_data1pileup10;
UInt_t          lisa_data1pileup11;
UInt_t          lisa_data1pileup12;
UInt_t          lisa_data1pileup13;
UInt_t          lisa_data1pileup14;
UInt_t          lisa_data1pileup15;
UInt_t          lisa_data1pileup16;
UInt_t          lisa_data1overflow1;
UInt_t          lisa_data1overflow2;
UInt_t          lisa_data1overflow3;
UInt_t          lisa_data1overflow4;
UInt_t          lisa_data1overflow5;
UInt_t          lisa_data1overflow6;
UInt_t          lisa_data1overflow7;
UInt_t          lisa_data1overflow8;
UInt_t          lisa_data1overflow9;
UInt_t          lisa_data1overflow10;
UInt_t          lisa_data1overflow11;
UInt_t          lisa_data1overflow12;
UInt_t          lisa_data1overflow13;
UInt_t          lisa_data1overflow14;
UInt_t          lisa_data1overflow15;
UInt_t          lisa_data1overflow16;
UInt_t          lisa_data1channel_cfd;
UInt_t          lisa_data1channel_cfdI[4];   //[lisa_data1channel_cfd]
UInt_t          lisa_data1channel_cfdv[4];   //[lisa_data1channel_cfd]
UInt_t          lisa_data1channel_energy;
UInt_t          lisa_data1channel_energyI[4];   //[lisa_data1channel_energy]
UInt_t          lisa_data1channel_energyv[4];   //[lisa_data1channel_energy]
												//*** traces ***//												
UInt_t          lisa_data1traces1;
UInt_t          lisa_data1traces1I[8000];   //[lisa_data1traces1]
UInt_t          lisa_data1traces1v[8000];   //[lisa_data1traces1]
UInt_t          lisa_data1traces2;
UInt_t          lisa_data1traces2I[8000];   //[lisa_data1traces2]
UInt_t          lisa_data1traces2v[8000];   //[lisa_data1traces2]
UInt_t          lisa_data1traces3;
UInt_t          lisa_data1traces3I[8000];   //[lisa_data1traces3]
UInt_t          lisa_data1traces3v[8000];   //[lisa_data1traces3]
UInt_t          lisa_data1traces4;
UInt_t          lisa_data1traces4I[8000];   //[lisa_data1traces4]
UInt_t          lisa_data1traces4v[8000];   //[lisa_data1traces4]
UInt_t          lisa_data1traces5;
UInt_t          lisa_data1traces5I[8000];   //[lisa_data1traces5]
UInt_t          lisa_data1traces5v[8000];   //[lisa_data1traces5]
UInt_t          lisa_data1traces6;
UInt_t          lisa_data1traces6I[8000];   //[lisa_data1traces6]
UInt_t          lisa_data1traces6v[8000];   //[lisa_data1traces6]
UInt_t          lisa_data1traces7;
UInt_t          lisa_data1traces7I[8000];   //[lisa_data1traces7]
UInt_t          lisa_data1traces7v[8000];   //[lisa_data1traces7]
UInt_t          lisa_data1traces8;
UInt_t          lisa_data1traces8I[8000];   //[lisa_data1traces8]
UInt_t          lisa_data1traces8v[8000];   //[lisa_data1traces8]
UInt_t          lisa_data1traces9;
UInt_t          lisa_data1traces9I[8000];   //[lisa_data1traces9]
UInt_t          lisa_data1traces9v[8000];   //[lisa_data1traces9]
UInt_t          lisa_data1traces10;
UInt_t          lisa_data1traces10I[8000];   //[lisa_data1traces10]
UInt_t          lisa_data1traces10v[8000];   //[lisa_data1traces10]
UInt_t          lisa_data1traces11;
UInt_t          lisa_data1traces11I[8000];   //[lisa_data1traces11]
UInt_t          lisa_data1traces11v[8000];   //[lisa_data1traces11]
UInt_t          lisa_data1traces12;
UInt_t          lisa_data1traces12I[8000];   //[lisa_data1traces12]
UInt_t          lisa_data1traces12v[8000];   //[lisa_data1traces12]
UInt_t          lisa_data1traces13;
UInt_t          lisa_data1traces13I[8000];   //[lisa_data1traces13]
UInt_t          lisa_data1traces13v[8000];   //[lisa_data1traces13]
UInt_t          lisa_data1traces14;
UInt_t          lisa_data1traces14I[8000];   //[lisa_data1traces14]
UInt_t          lisa_data1traces14v[8000];   //[lisa_data1traces14]
UInt_t          lisa_data1traces15;
UInt_t          lisa_data1traces15I[8000];   //[lisa_data1traces15]
UInt_t          lisa_data1traces15v[8000];   //[lisa_data1traces15]
UInt_t          lisa_data1traces16;
UInt_t          lisa_data1traces16I[8000];   //[lisa_data1traces16]
UInt_t          lisa_data1traces16v[8000];   //[lisa_data1traces16]

// List of branches
TBranch        *b_TRIGGER;   //!
TBranch        *b_EVENTNO;   //!
TBranch        *b_lisa_ts_subsystem_id;   //!
TBranch        *b_lisa_ts_t1;   //!
TBranch        *b_lisa_ts_t2;   //!
TBranch        *b_lisa_ts_t3;   //!
TBranch        *b_lisa_ts_t4;   //!
TBranch        *b_lisa_data1event_trigger_time_hi;   //!
TBranch        *b_lisa_data1event_trigger_time_lo;   //!
TBranch        *b_lisa_data1hit_pattern;   //!
TBranch        *b_lisa_data1num_channels_fired;   //!
TBranch        *b_lisa_data1channel_id;   //!
TBranch        *b_lisa_data1channel_idI;   //!
TBranch        *b_lisa_data1channel_idv;   //!
TBranch        *b_lisa_data1channel_trigger_time_hi;   //!
TBranch        *b_lisa_data1channel_trigger_time_hiI;   //!
TBranch        *b_lisa_data1channel_trigger_time_hiv;   //!
TBranch        *b_lisa_data1channel_trigger_time_lo;   //!
TBranch        *b_lisa_data1channel_trigger_time_loI;   //!
TBranch        *b_lisa_data1channel_trigger_time_lov;   //!
TBranch        *b_lisa_data1pileup1;   //!
TBranch        *b_lisa_data1pileup2;   //!
TBranch        *b_lisa_data1pileup3;   //!
TBranch        *b_lisa_data1pileup4;   //!
TBranch        *b_lisa_data1pileup5;   //!
TBranch        *b_lisa_data1pileup6;   //!
TBranch        *b_lisa_data1pileup7;   //!
TBranch        *b_lisa_data1pileup8;   //!
TBranch        *b_lisa_data1pileup9;   //!
TBranch        *b_lisa_data1pileup10;   //!
TBranch        *b_lisa_data1pileup11;   //!
TBranch        *b_lisa_data1pileup12;   //!
TBranch        *b_lisa_data1pileup13;   //!
TBranch        *b_lisa_data1pileup14;   //!
TBranch        *b_lisa_data1pileup15;   //!
TBranch        *b_lisa_data1pileup16;   //!
TBranch        *b_lisa_data1overflow1;   //!
TBranch        *b_lisa_data1overflow2;   //!
TBranch        *b_lisa_data1overflow3;   //!
TBranch        *b_lisa_data1overflow4;   //!
TBranch        *b_lisa_data1overflow5;   //!
TBranch        *b_lisa_data1overflow6;   //!
TBranch        *b_lisa_data1overflow7;   //!
TBranch        *b_lisa_data1overflow8;   //!
TBranch        *b_lisa_data1overflow9;   //!
TBranch        *b_lisa_data1overflow10;   //!
TBranch        *b_lisa_data1overflow11;   //!
TBranch        *b_lisa_data1overflow12;   //!
TBranch        *b_lisa_data1overflow13;   //!
TBranch        *b_lisa_data1overflow14;   //!
TBranch        *b_lisa_data1overflow15;   //!
TBranch        *b_lisa_data1overflow16;   //!
TBranch        *b_lisa_data1channel_cfd;   //!
TBranch        *b_lisa_data1channel_cfdI;   //!
TBranch        *b_lisa_data1channel_cfdv;   //!
TBranch        *b_lisa_data1channel_energy;   //!
TBranch        *b_lisa_data1channel_energyI;   //!
TBranch        *b_lisa_data1channel_energyv;   //!

TBranch        *b_lisa_data1traces1;   //!
TBranch        *b_lisa_data1traces1I;   //!
TBranch        *b_lisa_data1traces1v;   //!
TBranch        *b_lisa_data1traces2;   //!
TBranch        *b_lisa_data1traces2I;   //!
TBranch        *b_lisa_data1traces2v;   //!
TBranch        *b_lisa_data1traces3;   //!
TBranch        *b_lisa_data1traces3I;   //!
TBranch        *b_lisa_data1traces3v;   //!
TBranch        *b_lisa_data1traces4;   //!
TBranch        *b_lisa_data1traces4I;   //!
TBranch        *b_lisa_data1traces4v;   //!
TBranch        *b_lisa_data1traces5;   //!
TBranch        *b_lisa_data1traces5I;   //!
TBranch        *b_lisa_data1traces5v;   //!
TBranch        *b_lisa_data1traces6;   //!
TBranch        *b_lisa_data1traces6I;   //!
TBranch        *b_lisa_data1traces6v;   //!
TBranch        *b_lisa_data1traces7;   //!
TBranch        *b_lisa_data1traces7I;   //!
TBranch        *b_lisa_data1traces7v;   //!
TBranch        *b_lisa_data1traces8;   //!
TBranch        *b_lisa_data1traces8I;   //!
TBranch        *b_lisa_data1traces8v;   //!
TBranch        *b_lisa_data1traces9;   //!
TBranch        *b_lisa_data1traces9I;   //!
TBranch        *b_lisa_data1traces9v;   //!
TBranch        *b_lisa_data1traces10;   //!
TBranch        *b_lisa_data1traces10I;   //!
TBranch        *b_lisa_data1traces10v;   //!
TBranch        *b_lisa_data1traces11;   //!
TBranch        *b_lisa_data1traces11I;   //!
TBranch        *b_lisa_data1traces11v;   //!
TBranch        *b_lisa_data1traces12;   //!
TBranch        *b_lisa_data1traces12I;   //!
TBranch        *b_lisa_data1traces12v;   //!
TBranch        *b_lisa_data1traces13;   //!
TBranch        *b_lisa_data1traces13I;   //!
TBranch        *b_lisa_data1traces13v;   //!
TBranch        *b_lisa_data1traces14;   //!
TBranch        *b_lisa_data1traces14I;   //!
TBranch        *b_lisa_data1traces14v;   //!
TBranch        *b_lisa_data1traces15;   //!
TBranch        *b_lisa_data1traces15I;   //!
TBranch        *b_lisa_data1traces15v;   //!
TBranch        *b_lisa_data1traces16;   //!
TBranch        *b_lisa_data1traces16I;   //!
TBranch        *b_lisa_data1traces16v;   //!

void InitUcesbTree(TTree *ftree)
{
	// Set branch addresses and branch pointers
	if (!ftree) {
		std::cout<<"Error701!!! No tree. PLease check input file"<<std::endl;
		return;
	}
	ftree->SetBranchAddress("TRIGGER", &TRIGGER, &b_TRIGGER);
	ftree->SetBranchAddress("EVENTNO", &EVENTNO, &b_EVENTNO);
	ftree->SetBranchAddress("lisa_ts_subsystem_id", &lisa_ts_subsystem_id, &b_lisa_ts_subsystem_id);
	ftree->SetBranchAddress("lisa_ts_t1", &lisa_ts_t1, &b_lisa_ts_t1);
	ftree->SetBranchAddress("lisa_ts_t2", &lisa_ts_t2, &b_lisa_ts_t2);
	ftree->SetBranchAddress("lisa_ts_t3", &lisa_ts_t3, &b_lisa_ts_t3);
	ftree->SetBranchAddress("lisa_ts_t4", &lisa_ts_t4, &b_lisa_ts_t4);
	ftree->SetBranchAddress("lisa_data1event_trigger_time_hi", &lisa_data1event_trigger_time_hi, &b_lisa_data1event_trigger_time_hi);
	ftree->SetBranchAddress("lisa_data1event_trigger_time_lo", &lisa_data1event_trigger_time_lo, &b_lisa_data1event_trigger_time_lo);
	ftree->SetBranchAddress("lisa_data1hit_pattern", &lisa_data1hit_pattern, &b_lisa_data1hit_pattern);
	ftree->SetBranchAddress("lisa_data1num_channels_fired", &lisa_data1num_channels_fired, &b_lisa_data1num_channels_fired);
	ftree->SetBranchAddress("lisa_data1channel_id", &lisa_data1channel_id, &b_lisa_data1channel_id);
	ftree->SetBranchAddress("lisa_data1channel_idI", lisa_data1channel_idI, &b_lisa_data1channel_idI);
	ftree->SetBranchAddress("lisa_data1channel_idv", lisa_data1channel_idv, &b_lisa_data1channel_idv);
	ftree->SetBranchAddress("lisa_data1channel_trigger_time_hi", &lisa_data1channel_trigger_time_hi, &b_lisa_data1channel_trigger_time_hi);
	ftree->SetBranchAddress("lisa_data1channel_trigger_time_hiI", lisa_data1channel_trigger_time_hiI, &b_lisa_data1channel_trigger_time_hiI);
	ftree->SetBranchAddress("lisa_data1channel_trigger_time_hiv", lisa_data1channel_trigger_time_hiv, &b_lisa_data1channel_trigger_time_hiv);
	ftree->SetBranchAddress("lisa_data1channel_trigger_time_lo", &lisa_data1channel_trigger_time_lo, &b_lisa_data1channel_trigger_time_lo);
	ftree->SetBranchAddress("lisa_data1channel_trigger_time_loI", lisa_data1channel_trigger_time_loI, &b_lisa_data1channel_trigger_time_loI);
	ftree->SetBranchAddress("lisa_data1channel_trigger_time_lov", lisa_data1channel_trigger_time_lov, &b_lisa_data1channel_trigger_time_lov);
	ftree->SetBranchAddress("lisa_data1pileup1", &lisa_data1pileup1, &b_lisa_data1pileup1);
	ftree->SetBranchAddress("lisa_data1pileup2", &lisa_data1pileup2, &b_lisa_data1pileup2);
	ftree->SetBranchAddress("lisa_data1pileup3", &lisa_data1pileup3, &b_lisa_data1pileup3);
	ftree->SetBranchAddress("lisa_data1pileup4", &lisa_data1pileup4, &b_lisa_data1pileup4);
	ftree->SetBranchAddress("lisa_data1pileup5", &lisa_data1pileup5, &b_lisa_data1pileup5);
	ftree->SetBranchAddress("lisa_data1pileup6", &lisa_data1pileup6, &b_lisa_data1pileup6);
	ftree->SetBranchAddress("lisa_data1pileup7", &lisa_data1pileup7, &b_lisa_data1pileup7);
	ftree->SetBranchAddress("lisa_data1pileup8", &lisa_data1pileup8, &b_lisa_data1pileup8);
	ftree->SetBranchAddress("lisa_data1pileup9", &lisa_data1pileup9, &b_lisa_data1pileup9);
	ftree->SetBranchAddress("lisa_data1pileup10", &lisa_data1pileup10, &b_lisa_data1pileup10);
	ftree->SetBranchAddress("lisa_data1pileup11", &lisa_data1pileup11, &b_lisa_data1pileup11);
	ftree->SetBranchAddress("lisa_data1pileup12", &lisa_data1pileup12, &b_lisa_data1pileup12);
	ftree->SetBranchAddress("lisa_data1pileup13", &lisa_data1pileup13, &b_lisa_data1pileup13);
	ftree->SetBranchAddress("lisa_data1pileup14", &lisa_data1pileup14, &b_lisa_data1pileup14);
	ftree->SetBranchAddress("lisa_data1pileup15", &lisa_data1pileup15, &b_lisa_data1pileup15);
	ftree->SetBranchAddress("lisa_data1pileup16", &lisa_data1pileup16, &b_lisa_data1pileup16);
	ftree->SetBranchAddress("lisa_data1overflow1", &lisa_data1overflow1, &b_lisa_data1overflow1);
	ftree->SetBranchAddress("lisa_data1overflow2", &lisa_data1overflow2, &b_lisa_data1overflow2);
	ftree->SetBranchAddress("lisa_data1overflow3", &lisa_data1overflow3, &b_lisa_data1overflow3);
	ftree->SetBranchAddress("lisa_data1overflow4", &lisa_data1overflow4, &b_lisa_data1overflow4);
	ftree->SetBranchAddress("lisa_data1overflow5", &lisa_data1overflow5, &b_lisa_data1overflow5);
	ftree->SetBranchAddress("lisa_data1overflow6", &lisa_data1overflow6, &b_lisa_data1overflow6);
	ftree->SetBranchAddress("lisa_data1overflow7", &lisa_data1overflow7, &b_lisa_data1overflow7);
	ftree->SetBranchAddress("lisa_data1overflow8", &lisa_data1overflow8, &b_lisa_data1overflow8);
	ftree->SetBranchAddress("lisa_data1overflow9", &lisa_data1overflow9, &b_lisa_data1overflow9);
	ftree->SetBranchAddress("lisa_data1overflow10", &lisa_data1overflow10, &b_lisa_data1overflow10);
	ftree->SetBranchAddress("lisa_data1overflow11", &lisa_data1overflow11, &b_lisa_data1overflow11);
	ftree->SetBranchAddress("lisa_data1overflow12", &lisa_data1overflow12, &b_lisa_data1overflow12);
	ftree->SetBranchAddress("lisa_data1overflow13", &lisa_data1overflow13, &b_lisa_data1overflow13);
	ftree->SetBranchAddress("lisa_data1overflow14", &lisa_data1overflow14, &b_lisa_data1overflow14);
	ftree->SetBranchAddress("lisa_data1overflow15", &lisa_data1overflow15, &b_lisa_data1overflow15);
	ftree->SetBranchAddress("lisa_data1overflow16", &lisa_data1overflow16, &b_lisa_data1overflow16);
	ftree->SetBranchAddress("lisa_data1channel_cfd", &lisa_data1channel_cfd, &b_lisa_data1channel_cfd);
	ftree->SetBranchAddress("lisa_data1channel_cfdI", lisa_data1channel_cfdI, &b_lisa_data1channel_cfdI);
	ftree->SetBranchAddress("lisa_data1channel_cfdv", lisa_data1channel_cfdv, &b_lisa_data1channel_cfdv);
	ftree->SetBranchAddress("lisa_data1channel_energy", &lisa_data1channel_energy, &b_lisa_data1channel_energy);
	ftree->SetBranchAddress("lisa_data1channel_energyI", lisa_data1channel_energyI, &b_lisa_data1channel_energyI);
	ftree->SetBranchAddress("lisa_data1channel_energyv", lisa_data1channel_energyv, &b_lisa_data1channel_energyv);

	ftree->SetBranchAddress("lisa_data1traces1", &lisa_data1traces1, &b_lisa_data1traces1);
	ftree->SetBranchAddress("lisa_data1traces1I", lisa_data1traces1I, &b_lisa_data1traces1I);
	ftree->SetBranchAddress("lisa_data1traces1v", lisa_data1traces1v, &b_lisa_data1traces1v);
	ftree->SetBranchAddress("lisa_data1traces2", &lisa_data1traces2, &b_lisa_data1traces2);
	ftree->SetBranchAddress("lisa_data1traces2I", lisa_data1traces2I, &b_lisa_data1traces2I);
	ftree->SetBranchAddress("lisa_data1traces2v", lisa_data1traces2v, &b_lisa_data1traces2v);
	ftree->SetBranchAddress("lisa_data1traces3", &lisa_data1traces3, &b_lisa_data1traces3);
	ftree->SetBranchAddress("lisa_data1traces3I", lisa_data1traces3I, &b_lisa_data1traces3I);
	ftree->SetBranchAddress("lisa_data1traces3v", lisa_data1traces3v, &b_lisa_data1traces3v);
	ftree->SetBranchAddress("lisa_data1traces4", &lisa_data1traces4, &b_lisa_data1traces4);
	ftree->SetBranchAddress("lisa_data1traces4I", lisa_data1traces4I, &b_lisa_data1traces4I);
	ftree->SetBranchAddress("lisa_data1traces4v", lisa_data1traces4v, &b_lisa_data1traces4v);
	ftree->SetBranchAddress("lisa_data1traces5", &lisa_data1traces5, &b_lisa_data1traces5);
	ftree->SetBranchAddress("lisa_data1traces5I", lisa_data1traces5I, &b_lisa_data1traces5I);
	ftree->SetBranchAddress("lisa_data1traces5v", lisa_data1traces5v, &b_lisa_data1traces5v);
	ftree->SetBranchAddress("lisa_data1traces6", &lisa_data1traces6, &b_lisa_data1traces6);
	ftree->SetBranchAddress("lisa_data1traces6I", lisa_data1traces6I, &b_lisa_data1traces6I);
	ftree->SetBranchAddress("lisa_data1traces6v", lisa_data1traces6v, &b_lisa_data1traces6v);
	ftree->SetBranchAddress("lisa_data1traces7", &lisa_data1traces7, &b_lisa_data1traces7);
	ftree->SetBranchAddress("lisa_data1traces7I", lisa_data1traces7I, &b_lisa_data1traces7I);
	ftree->SetBranchAddress("lisa_data1traces7v", lisa_data1traces7v, &b_lisa_data1traces7v);
	ftree->SetBranchAddress("lisa_data1traces8", &lisa_data1traces8, &b_lisa_data1traces8);
	ftree->SetBranchAddress("lisa_data1traces8I", lisa_data1traces8I, &b_lisa_data1traces8I);
	ftree->SetBranchAddress("lisa_data1traces8v", lisa_data1traces8v, &b_lisa_data1traces8v);
	ftree->SetBranchAddress("lisa_data1traces9", &lisa_data1traces9, &b_lisa_data1traces9);
	ftree->SetBranchAddress("lisa_data1traces9I", lisa_data1traces9I, &b_lisa_data1traces9I);
	ftree->SetBranchAddress("lisa_data1traces9v", lisa_data1traces9v, &b_lisa_data1traces9v);
	ftree->SetBranchAddress("lisa_data1traces10", &lisa_data1traces10, &b_lisa_data1traces10);
	ftree->SetBranchAddress("lisa_data1traces10I", lisa_data1traces10I, &b_lisa_data1traces10I);
	ftree->SetBranchAddress("lisa_data1traces10v", lisa_data1traces10v, &b_lisa_data1traces10v);
	ftree->SetBranchAddress("lisa_data1traces11", &lisa_data1traces11, &b_lisa_data1traces11);
	ftree->SetBranchAddress("lisa_data1traces11I", lisa_data1traces11I, &b_lisa_data1traces11I);
	ftree->SetBranchAddress("lisa_data1traces11v", lisa_data1traces11v, &b_lisa_data1traces11v);
	ftree->SetBranchAddress("lisa_data1traces12", &lisa_data1traces12, &b_lisa_data1traces12);
	ftree->SetBranchAddress("lisa_data1traces12I", lisa_data1traces12I, &b_lisa_data1traces12I);
	ftree->SetBranchAddress("lisa_data1traces12v", lisa_data1traces12v, &b_lisa_data1traces12v);
	ftree->SetBranchAddress("lisa_data1traces13", &lisa_data1traces13, &b_lisa_data1traces13);
	ftree->SetBranchAddress("lisa_data1traces13I", lisa_data1traces13I, &b_lisa_data1traces13I);
	ftree->SetBranchAddress("lisa_data1traces13v", lisa_data1traces13v, &b_lisa_data1traces13v);
	ftree->SetBranchAddress("lisa_data1traces14", &lisa_data1traces14, &b_lisa_data1traces14);
	ftree->SetBranchAddress("lisa_data1traces14I", lisa_data1traces14I, &b_lisa_data1traces14I);
	ftree->SetBranchAddress("lisa_data1traces14v", lisa_data1traces14v, &b_lisa_data1traces14v);
	ftree->SetBranchAddress("lisa_data1traces15", &lisa_data1traces15, &b_lisa_data1traces15);
	ftree->SetBranchAddress("lisa_data1traces15I", lisa_data1traces15I, &b_lisa_data1traces15I);
	ftree->SetBranchAddress("lisa_data1traces15v", lisa_data1traces15v, &b_lisa_data1traces15v);
	ftree->SetBranchAddress("lisa_data1traces16", &lisa_data1traces16, &b_lisa_data1traces16);
	ftree->SetBranchAddress("lisa_data1traces16I", lisa_data1traces16I, &b_lisa_data1traces16I);
	ftree->SetBranchAddress("lisa_data1traces16v", lisa_data1traces16v, &b_lisa_data1traces16v);

	ftree->SetBranchStatus("*",0);  // disable all branches
	ftree->SetBranchStatus("TRIGGER",1);  // activate branchname
	ftree->SetBranchStatus("EVENTNO",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces1",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces1I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces1v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces2",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces2I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces2v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces3",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces3I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces3v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces4",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces4I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces4v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces5",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces5I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces5v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces6",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces6I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces6v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces7",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces7I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces7v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces8",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces8I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces8v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces9",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces9I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces9v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces10",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces10I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces10v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces11",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces11I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces11v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces12",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces12I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces12v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces13",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces13I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces13v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces14",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces14I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces14v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces15",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces15I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces15v",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces16",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces16I",1);  // activate branchname
	ftree->SetBranchStatus("lisa_data1traces16v",1);  // activate branchname

}
#endif
