//
//last updated@ 26 Jan,2024
//Z.Chen
//Email: z.chen@gsi.de
//
//Error4xx
//*********************************************************************************//
//*********************************release notes***********************************//
//15Jan,2023
//add "welcome()" and "anaHelp()"
//16Jan,2023
//add uncertainty calculation for resolution; only consider uncertainty from fit
//22Jan,2023
//some corrections are added to uncertainty calculation
//some updates on energy calibration
//29Jan,2023
//some updates related to trapez
//10 Feb,2023
//some updates related to febex4
//21 Feb
//some updates related to display
//06 Mar, 2023
//some updates on energy calibration; now the max of peaks for calib is 6
//10 Mar, 2023
//comment those parts related to checkLastPoint();
//11 Mar
//add gFebexTraceL to restrict trace length
//23 Mar
//some updates on baselineCorr()
//25 Mar
//Some updates on fit display
//simplify getEnergyResolution();
//17 Apr, 2023
//optimize data loading
//12 Nov, 2023
//some updates on baselineCorr(); add 200 fake points(y==0) at the beginning of the baseline for MWD analysis.
//some updates on MWD(): change moving window starting point to fit the newly added 200 fake points
//17 Nov, 2023
//add ucesb
//subtract baseline noise
//22 Nov,
//fix typo on Scope.data.runnumber
//26 Nov
//update energy calibration
//02 Dec
//add T_cfd
//06 Dec
//add random number to T_cfd
//21 Dec
//update energy calibration
//24 Dec
//fix bugs in baselineCOrr()
//30 optimize energy calib; update anaHelp()
//26 Jan
//fix energy calib bug
//
//**********************************************************************************//

#ifndef _VIEWTRACE_HH_
#define _VIEWTRACE_HH_

#include "Env.hh"
#include "TEnv.h"

inline void welcome(){

	std::cout<<"*************************************************************"<<std::endl;
	std::cout<<"**                 Welcome to anaTraces.                  **"<<std::endl;
	std::cout<<"** This toolkit was developed and tested with ROOT6.24.   **"<<std::endl;
	std::cout<<"** A brief introduction of how to perform simple          **"<<std::endl;
	std::cout<<"** analysis with this toolkit can be called by            **"<<std::endl;
	std::cout<<"** entering 'anaHelp()' in your ROOT interface.           **"<<std::endl;
	std::cout<<"** e.g. root [1] anaHelp()                                **"<<std::endl;
	std::cout<<"** To generate a hotstart, please run                     **"<<std::endl;
	std::cout<<"** root [1] createHotstart()                              **"<<std::endl;
	std::cout<<"************************************************************"<<std::endl;
}

inline void anaHelp(){

	std::cout<<"******************************************************************"<<std::endl;
	std::cout<<"**               Welcome to anaHelp()_v2.0.                     **"<<std::endl;
	std::cout<<"** 1. Prepare your setting file and load it.                    **"<<std::endl;
	std::cout<<"** [] setSettingFile(\"SETTING.SET\")                            **"<<std::endl;
	std::cout<<"** 1.1 if it's your first time to load this setting file, you   **"<<std::endl;
	std::cout<<"** can create a hotstart with                                   **"<<std::endl;
	std::cout<<"** [] createHotstart()                                          **"<<std::endl;
	std::cout<<"** Next time you can executive this code and load setting file  **"<<std::endl;
	std::cout<<"** using                                                        **"<<std::endl;
	std::cout<<"** $ sh hotStart.sh                                             **"<<std::endl;
	std::cout<<"** 2. Three most frequently used commands/functions:            **"<<std::endl;
	std::cout<<"** [] anaFebex4_checkSingleTrace(ientry)                        **"<<std::endl;
	std::cout<<"** Only display one waveform with given ientry.                 **"<<std::endl;
	std::cout<<"** [] checkMWDFebex(ientry)                                     **"<<std::endl;
	std::cout<<"** display one raw waveform and a new one calculated using MWD. **"<<std::endl;
	std::cout<<"** [] anaFebex4_checkTraces()                                   **"<<std::endl;
	std::cout<<"** analyze N traces(can be set in your setting file) and display**"<<std::endl;
	std::cout<<"** raw waveforms, time spectrum and energy spectrum.            **"<<std::endl;
	std::cout<<"** 3. get energy resolutions and do calibration                 **"<<std::endl;
	std::cout<<"** 3.1 the resolution will be deduced by fitting the peaks with **"<<std::endl;
	std::cout<<"** gaus functions. You have to set the means, sigmas and        **"<<std::endl;
	std::cout<<"** amplitudes for the candidate peaks in advance by using       **"<<std::endl;
	std::cout<<"** [] setMeans(mean1,mean2,..) //up to 10                       **"<<std::endl;
	std::cout<<"** [] setSigmas(sigma1,sigma2,..)                               **"<<std::endl;
	std::cout<<"** [] setAmplitudes(amp1,amp2,..)                               **"<<std::endl;
	std::cout<<"** 3.2 To get energy resolution with data from one Febex ch     **"<<std::endl;
	std::cout<<"** [] getEnergyResolution(num_of_peaks) //up to 10              **"<<std::endl;
	std::cout<<"** 3.3 To do energy calibration for this febex channel, you have**"<<std::endl;
	std::cout<<"** to set the reference energy in advance using                 **"<<std::endl;
	std::cout<<"** [] setEnergyRef(energy1,energy2,...)                         **"<<std::endl;
	std::cout<<"** Then do                                                      **"<<std::endl;
	std::cout<<"** [] energyCalib(num_of_peaks,true/false)                      **"<<std::endl;
	std::cout<<"** If the second argument is set as true, you'll save the new   **"<<std::endl;
	std::cout<<"** calibration parameters. Otherwise, not.                      **"<<std::endl;
	std::cout<<"** 3.3 get energy resolution for other febex channels           **"<<std::endl;
	std::cout<<"** [] setFebexChannel(channel)                                  **"<<std::endl;
	std::cout<<"** [] anaFebex4_checkTraces()                                   **"<<std::endl;
	std::cout<<"** 3.4 You can also copy the current calibration parameters     **"<<std::endl;
	std::cout<<"** to all other febex channels using                            **"<<std::endl;
	std::cout<<"** [] applyCalibParsToAll()                                     **"<<std::endl;
	std::cout<<"** 3.5 To adjust x range of energy spectrum, you can use        **"<<std::endl;
	std::cout<<"** [] setEnergySpecRange(val_min,val_max)                       **"<<std::endl;
	std::cout<<"** [] showEnergySpectrum()                                      **"<<std::endl;
	std::cout<<"** 4 To save all the parameters you're using in your analysis   **"<<std::endl;
	std::cout<<"** [] exportParameters()                                        **"<<std::endl;
}

inline bool loadSettings(){
	if(!isOpen(gSettings)){
		std::cout<<"Error401!!! Can not find/open setting file "<<gSettings<<std::endl;
		return false;
	}
	if(gcnt_loadSetting>0)return true;
	std::cout<<"--->Load "<<gSettings<<std::endl;
	TEnv env(gSettings);
	gV1742data = env.GetValue("V1742.data.filename","../V1742/output_0018.root");
	gFebex4data = env.GetValue("Febex4.data.filename","./LISA_root_tree.root");
	gTxtDir = env.GetValue("Scope.data.directory","../fD_fS_3.5V/");
	gTsfilename = env.GetValue("Scope.timestamp.filename","timestamp.log");
	gSettings_rename = env.GetValue("New.setting.file.name","settings_Febex_export.set");
	gIsGo4Unpack = env.GetValue("Unpack.with.go4",false);
	gIsUcesbUnpack = env.GetValue("Unpack.with.ucesb",false);
	gSubNoise = env.GetValue("Subtract.noise.with.baseline",false);
	gTreeAllEntry = env.GetValue("Enable.read.all.entry",false);
	gSingleChAnaMode = env.GetValue("Enable.single.channel.analysis.mode",true);
	gMultiChAnaMode = env.GetValue("Enable.multi.channel.analysis.mode",false);
	gRun[0] = env.GetValue("Scope.data.runnumber.0",0);
	gRun[1] = env.GetValue("Scope.data.runnumber.1",2000);
	gTreeEntry[0] = env.GetValue("TreeEntry.read.from.0",0);
	gTreeEntry[1] = env.GetValue("TreeEntry.read.from.1",2000);
	gVerbose = env.GetValue("Verbose",0);
	gSampleRate = env.GetValue("Sample.rate[MHz]",62.5);
	gFebexCh = env.GetValue("Febex4.read.from.ch",0);
	for(int i=0;i<16;i++)gMultiCh[i] = env.GetValue(TString::Format("Multimode.read.from.ch.%d",i),0);//bool
	for(int i=0;i<16;i++)gThreshold[i] = env.GetValue(TString::Format("Threshold.ch.%d",i),-1);
	gFebexTraceL = env.GetValue("Febex4.trace.length",2000);
	window[0] = env.GetValue("Integral.window.0",-0.2);
	window[1] = env.GetValue("Integral.window.1",2.);
	gScopeDataLoading[0] = env.GetValue("Scope.Data.From.0",10000);
	gScopeDataLoading[1] = env.GetValue("Scope.Data.From.1",20000);
	gtrapezSampleWindow[0] = env.GetValue("Trapez.Sample.window.0",700);
	gtrapezSampleWindow[1] = env.GetValue("Trapez.Sample.window.1",1500);
	gTZAmp[0] = env.GetValue("Trapez.Amp.Calc.window.0",-200);
	gTZAmp[1] = env.GetValue("Trapez.Amp.Calc.window.1",200);
	gtrapezMovingWindowLength = env.GetValue("Trapez.Moving.Window.Length[ns]",3200);
	gRisingTime = env.GetValue("Rising.time[ns]",100);
	gDecaytime = env.GetValue("Decaytime[ns]",48000);
	gNbins = env.GetValue("Histogram.bins",4000);
	gParLimit = env.GetValue("Fit.Pars.Limit",12.0);
	gIntegral = env.GetValue("Enable.energyCalculation.integral",false);
	gMWD = env.GetValue("Enable.energyCalculation.algorithm",false);
	ghMIN = env.GetValue("Energy.histogram.range.0",0);
	ghMAX = env.GetValue("Energy.histogram.range.1",10000);
	ghTMIN = env.GetValue("Time.histogram.range.0",-2000);
	ghTMAX = env.GetValue("Time.histogram.range.1",2000);
	gESrange[0] = env.GetValue("Energy.spectrum.range.0",0);
	gESrange[1] = env.GetValue("Energy.spectrum.range.1",20000);
	gTSrange[0] = env.GetValue("Time.spectrum.range.0",-200.);
	gTSrange[1] = env.GetValue("Time.spectrum.range.1",200.);
	gTraceAmp[0] = env.GetValue("Trace.graph.Y.min",-2000);
	gTraceAmp[1] = env.GetValue("Trace.graph.Y.max",2000);
	gBaselineCorr = env.GetValue("Enable.baseline.correction",false);
	gTimeCorr = env.GetValue("Enable.time.correction",false);
	gCFDref = env.GetValue("CFD.reference",0.0);
	gCFDwin[0] = env.GetValue("CFD.window.0",140.);
	gCFDwin[1] = env.GetValue("CFD.window.1",160.);
	gFra = env.GetValue("CFD.pars.fraction",0.9);
	gDel = env.GetValue("CFD.pars.delay",1.2);
	for(int ii=0;ii<MAX_SIZE;ii++)gCalibPar_b[ii] = env.GetValue(Form("Energy.calib.bias.%d",ii),0.0);
	for(int ii=0;ii<MAX_SIZE;ii++)gCalibPar_s[ii] = env.GetValue(Form("Energy.calib.gradient.%d",ii),1.0);
	for(int ii=0;ii<MAX_SIZE;ii++)gmean[ii] = env.GetValue(Form("Mean.value.%d",ii),-1);
	for(int ii=0;ii<MAX_SIZE;ii++)gsigma[ii] = env.GetValue(Form("Sigma.value.%d",ii),-1);
	for(int ii=0;ii<MAX_SIZE;ii++)gamp[ii] = env.GetValue(Form("Amplitude.value.%d",ii),-1);
	for(int ii=0;ii<MAX_SIZE;ii++)gEref[ii] = env.GetValue(Form("Energy.reference.%d",ii),-1);
	for(int ii=0;ii<MAX_SIZE;ii++)gEres[ii] = env.GetValue(Form("Energy.resolution.%d",ii),-1);
	gOptRootFileName = env.GetValue("SaveAsRoot.filename","sCVD.root");
	fsaveTraces = env.GetValue("Enable.save.traces",false);
	fSaveAsRoot = env.GetValue("Enable.save.as.root",false);

	std::cout<<"--->settings loaded!"<<std::endl;
	std::cout<<"--->Start a new recoding..."<<std::endl;
	TTimeStamp ts;
	optPar<<"-----------Start a new recording-----------"<<std::endl;
	optPar<<ts.AsString("l")<<std::endl;
	optPar<<std::endl;
	optPar<<std::endl;
	gcnt_loadSetting++;
	return true;
}

//Error calculation
double calculateResErr(double fres, double fmean, double fmeanErr, double fsigma, double fsigmaErr){

	return fres*sqrt( pow((fmeanErr/fmean),2) + pow((fsigmaErr/fsigma),2) );
}

//NIMA 58(2):253(1968)
//delay ~ 1/4 * total trace width
//fraction ~ 0.9
TGraph* CFD(TGraph *fgr,bool fdraw=false, double *ftiming=0){
	if(gVerbose == 1)std::cout<<"--->Time discrimination with CFD."<<std::endl;
	int length = 0;
	if(fgr == 0 || fgr->GetN() < 1){
		std::cout<<"Error402. Empty graph."<<std::endl;
		return 0;
	}
	if(gCFDGraph!=NULL){
		delete gCFDGraph;
		gCFDGraph=NULL;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;
	vector<double>yyf;//with fraction
	vector<double>xxd;//with delay
	vector<double>yyCFD;//after CFD
	xx.clear();
	yy.clear();
	yyf.clear();
	xxd.clear();
	yyCFD.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(-y);
		yyf.push_back(y*gFra);
		xxd.push_back(x+gDel);
	}
	TGraph *ftem1 = new TGraph(xx.size(),&xx[0],&yyf[0]);
	//TGraph *fMWD = new TGraph(MWD.size(),&MWDx[0],&MWD[0]);
	for(int i=0;i<length;i++){
		double ytem=0;
		ytem = ftem1->Eval(xxd[i]);
		yyCFD.push_back(ytem+yy[i]);
	}
	gCFDGraph = new TGraph(xxd.size(),&xxd[0],&yyCFD[0]);
	//TGraph *ftem2 = new TGraph(xxd.size(),&xxd[0],&yyCFD[0]);
	gCFDGraph->SetTitle("Constant-fraction discrimination(CFD)");
	gCFDGraph->GetXaxis()->SetTitle("Time[ns]");
	if(fdraw)gCFDGraph->Draw("AP*");
	//calculate zero
	double ymax_y,ymax_x,ymin_y,ymin_x;
	ymax_y = ymin_y = 0;
	ymax_x = ymin_x = -9999;
	double fx1,fx2;
	fx1 = fx2 = -9999;
	for(int i=0;i<length;i++){
		if(xxd[i]>gCFDwin[0] && xxd[i]<gCFDwin[1]){
			if(yyCFD[i]>ymax_y){
				ymax_y = yyCFD[i];
				ymax_x = xxd[i];
			}
			if(yyCFD[i]<ymin_y){
				ymin_y = yyCFD[i];
				ymin_x = xxd[i];
			}
		}
	}
	fx1 = ymax_x;
	fx2 = ymin_x;
	//swap
	if(fx2<fx1){
		double tem = fx1;
		fx1 = fx2;
		fx2 = tem;
	}
	vector<double>xxz;//to calculate zero
	vector<double>yyz;//to calculate zero
	xxz.clear();
	yyz.clear();
	for(int i=0;i<length;i++){
		if(xxd[i]>fx1 && xxd[i]<fx2){
			xxz.push_back(yyCFD[i]);
			yyz.push_back(xxd[i]);//int time related to the sample rate;
								  //yyz.push_back(xxd[i]+frd->Uniform(-1,1));//float time which is nice for display;
		}
	}
	TGraph *ftem3 = new TGraph(xxz.size(),&xxz[0],&yyz[0]);
	ftiming[0] = ftem3->Eval(0,0)+gRandom->Uniform(-5,5);//use spline interpolation to get the "0" point;float time
														 //gCFDtime.push_back(ftem3->Eval(0,0)+gRandom->Uniform(-5,5));//use spline interpolation to get the "0" point;float time
	if(gVerbose == 1)std::cout<<"--->CFD result: zero point @ "<<ftiming[0]<<" ns."<<std::endl;
	//*** clear ***//
	delete ftem1;
	delete ftem3;
	ftem1=NULL;
	ftem3=NULL;
	return gCFDGraph;
}

double getTimingCFD(TGraph *fgr){
	double fTiming[1] = {-1};
	CFD(fgr,false,fTiming);
	return fTiming[0];
}

TGraph* baselineCorr(TGraph *fgr, bool plot=false){

	int length=0;
	if(fgr==0 || fgr->GetN()<1 ){
		std::cout<<"Error403. Empty graph."<<std::endl;
		return 0;
	}
	if(gBaselineCorrGraph!=NULL){
		delete gBaselineCorrGraph;
		gBaselineCorrGraph=NULL;
	}
	length = fgr->GetN();
	double *fxx = fgr->GetX();
	double *fyy = fgr->GetY();
	//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
	// calculate baseline shift //
	double fshift = 0;
	//only use 10 points
	for(int i=20;i<30;i++){
		fshift += fyy[i];
	}
	fshift /= 10.0;
	for(int i = 0;i<length;i++){
		fyy[i] -= fshift;
	}	
	gBaselineCorrGraph = new TGraph(length,fxx,fyy);
	//TGraph *fbaseGr2 = new TGraph(length,fxx,fyy);
	gBaselineCorrGraph->SetTitle("Trace: With baseline correction");
	gBaselineCorrGraph->GetXaxis()->SetTitle("Time[ns]");
	gBaselineCorrGraph->GetYaxis()->SetTitle("Amplitude[mV]");
	if(gVerbose == 1)std::cout<<"*With Baseline correction."<<std::endl;
	if(plot)gBaselineCorrGraph->Draw("AP*");
	return gBaselineCorrGraph;
}

//update, 12 Nov, 2023
//const int fcnt=200;//add 200 fake points
//double fxx2[length+fcnt];
//double fyy2[length+fcnt];

//double fshift = 0;
//first 100 points
//updates:only use 10 points
//for(int i=20;i<30;i++){
//	fshift+=fyy[i];
//}
//for(int i = 0;i<length;i++){
//	fyy[i] -= fshift/10.0;
//	fyy2[i] = fyy[i];
//	fxx2[i] = fxx[i];
//}
//update, 12 Nov, 2023
/*
   for(int ii=0;ii<fcnt;ii++){
   fxx2[ii+length] = (ii-fcnt+1)*10;
   fyy2[ii+length] = fyy[ii%30];//fake points are taken from the original first 30 points
   }
   */
//length+=fcnt;


TGraph* timeDriftCorr(TGraph* fgr,bool fdraw=false){
	if(gVerbose == 1)std::cout<<"--->start time drift corr"<<std::endl;
	int length=0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error404. Empty graph."<<std::endl;
		return 0;
	}
	if(gTimeDriftCorrGraph!=NULL){
		delete gTimeDriftCorrGraph;
		gTimeDriftCorrGraph=NULL;
	}
	length = fgr->GetN();
	CFD(fgr,false);
	//std::cout<<"CFD time test: "<<gCFDtime.back()<<std::endl;
	float fdelt = gCFDtime.back()-gCFDref;
	gCFDtime.pop_back();
	//std::cout<<"CFD time test2: "<<gCFDtime.back()<<std::endl;
	vector<double>xx;
	vector<double>yy;
	xx.clear();
	yy.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x-fdelt);
		yy.push_back(y);
	}

	//TGraph *ftem1 = new TGraph(xx.size(),&xx[0],&yy[0]);
	TGraph *gTimeDriftCorrGraph = new TGraph(xx.size(),&xx[0],&yy[0]);
	if(fdraw)gTimeDriftCorrGraph->Draw("AP*");
	gTimeDriftCorrGraph->GetXaxis()->SetTitle("Time[ns]");
	gTimeDriftCorrGraph->GetYaxis()->SetTitle("Amplitude[mV]");
	return gTimeDriftCorrGraph;
}

void showRawTraces(){
	if(gRawTraces.size()<1){
		std::cout<<"Error415! No raw trace data, please check loaded traces."<<std::endl;
		return;
	}
	std::cout<<gRawTraces.size()<<" raw traces loaded."<<std::endl;
	int cnt=0;
	if(gRawTraces.size()>0){
		gRawTraces.at(0)->Draw("APL");
		gRawTraces.at(0)->SetTitle(TString::Format("Raw Traces_%dns/Simple",(int)(1000/gSampleRate) ) );
		gRawTraces.at(0)->GetXaxis()->SetTitle("Time[ns]");
		gRawTraces.at(0)->GetYaxis()->SetTitle("Amplitude[arb.]");
		gRawTraces.at(0)->SetMaximum(gTraceAmp[1]);
		gRawTraces.at(0)->SetMinimum(gTraceAmp[0]);
		for(int i=0;i<gRawTraces.size();i++){
			gRawTraces.at(i)->SetLineColor(1+i%40);
			gRawTraces.at(i)->Draw("PL");
			if(i>=6000)break;
			cnt++;
		}
		std::cout<<cnt<<" raw traces are added to TGraph."<<std::endl;
	}

	std::cout<<"***********************"<<std::endl;
}

void showEnergySpectrum(){
	if(gRawE.size()<1){
		std::cout<<"Error405! No energy data, please check loaded traces."<<std::endl;
		return;
	}
	//std::cout<<gRawE.size()<<" traces loaded."<<std::endl;
	//TCanvas *EScan = new TCanvas();
	TH1F* fhES = new TH1F("Energy spectrum","Energy spectra",gNbins,ghMIN,ghMAX);
	if(gFebexCh>15){
		std::cout<<"Error419!!! gFebexCh can not > 15."<<std::endl;
		return;
	}
	for(map<int,double>::iterator iter = gRawE.begin();iter!=gRawE.end();++iter)fhES->Fill( gCalibPar_b[gFebexCh] + iter->second * gCalibPar_s[gFebexCh]);
	fhES->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);
	if(gCalibPar_s[gFebexCh]==1){
		fhES->GetXaxis()->SetTitle("Without calibration[arb.]");
	}
	else{
		fhES->GetXaxis()->SetTitle("After calibration[MeV]");
	}
	fhES->SetTitle("Energy Spectrum");
	fhES->Draw();
	// ***** FAIR style ************//
	//setFAIRstyle(fhES);
	//fhES->SetFillColor(796);
	//fhES->SetFillStyle(1001);
	//fhES->SetLineColor(kBlack);
	//fhES->SetLineWidth(1);
	std::cout<<"***********************"<<std::endl;
	ghist = (TH1F*)fhES->Clone();
	std::cout<<"*Adjust display range on energy spectrum: 'setEnergySpecRange(range1,range2)'"<<std::endl;
	std::cout<<"*Update: 'showEnergySpectrum()'"<<std::endl;
	std::cout<<"***********************"<<std::endl;
}

void showTimeSpectrum(){
	if(gCFDtime.size()<1){
		std::cout<<"Error406! No time data, please check time calculation."<<std::endl;
		return;
	}
	std::cout<<"Time info of "<<gCFDtime.size()<<" traces has been calculated."<<std::endl;
	TH1F* fhTS = new TH1F("Time spectrum","Time spectum",gNbins,ghTMIN,ghTMAX);
	for(int i=0;i<gCFDtime.size();i++)fhTS->Fill(gCFDtime[i]);
	fhTS->GetXaxis()->SetRangeUser(gTSrange[0],gTSrange[1]);
	fhTS->GetXaxis()->SetTitle("Time[ns]");
	fhTS->SetTitle("Time Spectrum");
	fhTS->Draw();
}

double Integrate(TGraph* fgr){

	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error407. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double sum =0;
	double bkg=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(x>window[0] && x<window[1])sum+=y;
		//if(gBaselineCorr && x>bwindow[0] && x<bwindow[1])bkg+=y;
	}
	/*
	   if(gBaselineCorr){
	   double ratio = 1.0*(window[1]-window[0])/(bwindow[1]-bwindow[0]);
	   std::cout<<"ratio = "<<ratio<<"; bkg = "<<bkg<<std::endl;
	   sum = sum - bkg*ratio;
	   }
	   */
	if(sum<0)sum = -sum;
	//std::cout<<"Energy = "<<sum<<std::endl;
	return sum;
}
//find edge(max/min) within the given window
double getEdg(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error408. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double xtem,ytem;
	xtem=-9999;
	ytem=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(gTZAmp[0] > gTZAmp[1]){
			std::cout<<"Error409. Please check trapez amp calc window."<<std::endl;
			return 0;
		}
		if(x>gTZAmp[0] && x<gTZAmp[1] && ytem>y){
			xtem = x;
			ytem = y;
			//std::cout<<"here!"<<std::endl;
		}
	}
	//std::cout<<"min = "<<ytem<<" @ x = "<<xtem<<std::endl;
	return ytem;
}

//find edge within the given window by gradient
//center of the rise edge must be around 0!!!
double getEdg2(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error408. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double fx[3]={-9999,-9999,-9999};
	double fy[3]={-9999,-9999,-9999};
	double fgrad[2] = {-9999,-9999};
	double dgrad = -9999;
	double xtem,ytem;
	xtem=-9999;
	ytem=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(gTZAmp[0] > gTZAmp[1]){
			std::cout<<"Error420. Please check trapez amp calc window."<<std::endl;
			return 0;
		}
		if(x>gTZAmp[0] && x<gTZAmp[1]){
			fx[0]=fx[1];
			fx[1]=fx[2];
			fx[2]=x;
			fy[0]=fy[1];
			fy[1]=fy[2];
			fy[2]=y;
			if(fx[0]!=-9999 && fx[1]!=-9999 && fx[2]!=-9999){
				fgrad[0] = (fy[1]-fy[0])/(fx[1]-fx[0]);
				fgrad[1] = (fy[2]-fy[1])/(fx[2]-fx[1]);
				dgrad = fgrad[1] - fgrad[0];
				//std::cout<<"gradient0= "<<fgrad[0]<<" @ x = "<<fx[0]<<std::endl;
				//std::cout<<"gradient1= "<<fgrad[1]<<" @ x = "<<fx[1]<<std::endl;
				//std::cout<<"delta = "<<dgrad<<std::endl;
				if(fx[1]>0 && abs(fgrad[1])<0.1 && dgrad>0.1)
				{
					xtem = fx[1];
					ytem = fy[1];
				}
			}
		}
	}
	//std::cout<<"min = "<<ytem<<" @ x = "<<xtem<<std::endl;
	return ytem;
}

//find amplitude within the given window by average
double getEdg3(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error408. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	double ytem;
	ytem=0;
	int cnt=0;
	double fsum=0;
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		if(gTZAmp[0] > gTZAmp[1]){
			std::cout<<"Error421. Please check trapez amp calc window."<<std::endl;
			return 0;
		}
		if(x>gTZAmp[0] && x<gTZAmp[1]){//ns
									   //std::cout<<"*** x and y"<<x<<" "<<y<<std::endl;
			fsum+=y;
			cnt++;
		}
	}
	if(gVerbose == 1)std::cout<<"min = "<<fsum/cnt<<std::endl;
	return ytem=fsum/cnt;
}

bool checkLastPoint(TGraph *fgr){
	int length = fgr->GetN();
	double *fxx = fgr->GetX();
	double *fyy = fgr->GetY();
	//std::cout<<"last point = "<<fyy[length-1]<<" "<<fxx[length-1]<<std::endl;
	bool fcheck = false;
	if(fyy[length-1]<100) fcheck = true;
	return fcheck;
}
/*
   void energyCalib(int nCal = 3, bool fSAVE = false){

   showEnergySpectrum();
   ghist->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);
   float xx[6];
   float yy[6];
   if(nCal==3){
   yy[0]= 5156;
   yy[1]= 5486;
   yy[2]= 5806;
   }
   else if(nCal == 6){
   yy[0] = 5106;
   yy[1] = 5156;
   yy[2] = 5443;
   yy[3] = 5486;
   yy[4] = 5763;
   yy[5] = 5806;
   }
   else{
   std::cout<<"Error416!!! nCal = illegal value. Only 3-peaks OR 6-peaks calibration is allowed now."<<std::endl;
   return;
   }
//float fpeak[6] = {5106,5156,5443,5486,5763,5806};
//int flimit = 12;//old version;now is global gParLimit
double temm[6];
double tems[6];

TCanvas *cCalib = new TCanvas("Energy calibration","Energy calibration",10,40,900,900);
cCalib->Divide(2,2);
cCalib->cd(1);
ghist->Draw();
TH1F *hcalib = (TH1F*)ghist->Clone();
cCalib->cd(2);
hcalib->Draw();
hcalib->SetTitle("Fit");
//gaus fit
TF1 *fsum;
vector<char*> fitFuncs;
fitFuncs.clear();
fitFuncs.push_back((char*)"pol0+gaus(1)+gaus(4)+gaus(7)");
fitFuncs.push_back((char*)"pol0+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)");
fsum = new TF1("fsum",fitFuncs[nCal/3-1],1,20000);
for(int i=0;i<nCal;i++){
fsum->SetParameter(3*i+2,gmean[i]);
fsum->SetParLimits(3*i+2,gmean[i]-gParLimit,gmean[i]+gParLimit);
fsum->SetParameter(3*i+3,gsigma[i]);
fsum->SetParLimits(3*i+3,gsigma[i]-gParLimit,gsigma[i]+gParLimit);
temm[i] = gmean[i];
tems[i] = gsigma[i];
}
hcalib->Fit("fsum","R");

//print out mean and sigma
double fitres[20];
fsum->GetParameters(fitres);
const Double_t *fitresErr = fsum->GetParErrors();//par errors
												 //std::cout<<"test par1 err = "<<fitresErr[1]<<std::endl;
												 Double_t meanErr[20],sigErr[20];
												 for(int i=0;i<nCal;i++){
												 gamp[i] = fitres[3*i+1];
												 gmean[i] = fitres[3*i+2];
												 gsigma[i] = fitres[3*i+3];
												 meanErr[i] = fitresErr[3*i+2];
												 sigErr[i] = fitresErr[3*i+3];
//std::cout<<"test "<<meanErr[i]<<std::endl;
gEres[i] = gsigma[i]*2.355/gmean[i]*100;//%
xx[i]=gmean[i];
}
std::cout<<"Initialized/Expected mean and sigma:"<<std::endl;
if(nCal==3)for(int i=0;i<3;i++)std::cout<<"peak"<<i<<" = "<<temm[i]<<"; with sigma = "<<tems[i]<<std::endl;
if(nCal==6)for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<temm[i]<<"; with sigma = "<<tems[i]<<std::endl;
std::cout<<"*Limit for mean and sigma fit now is "<<gParLimit<<"."<<std::endl;
std::cout<<"*To adjust this limit, please run 'setFitParLimit(NEW_VALUE)'and 'energyCalib()',respectively.."<<std::endl;
std::cout<<"Fit results(from left to right;only errors of fit pars were considered):"<<std::endl;
if(nCal==3)for(int i=0;i<3;i++)std::cout<<"peak"<<i<<" = "<<gmean[i]<<"; with sigma = "<<gsigma[i]<<" ;deltE/E = ("<<gEres[i]<<" +/- "<<calculateResErr(gEres[i],gmean[i],meanErr[i],gsigma[i],sigErr[i])<<")%"<<std::endl;
if(nCal==3)for(int i=0;i<3;i++){
	if(abs(temm[i] - gmean[i])>10)std::cout<<"Warning!!! Please update initial mean for peak"<<i<<" with fit result above and try it again."<<std::endl;
	if(abs(tems[i] - gsigma[i])>10)std::cout<<"Warning!!! Please update initial sigma for peak"<<i<<" with fit result above and try it again."<<std::endl;
}
if(nCal==6)for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<gmean[i]<<"; with sigma = "<<gsigma[i]<<" ;deltE/E = ("<<gEres[i]<<" +/- "<<calculateResErr(gEres[i],gmean[i],meanErr[i],gsigma[i],sigErr[i])<<")%"<<std::endl;
if(nCal==6)for(int i=0;i<6;i++){
	if(abs(temm[i] - gmean[i])>10)std::cout<<"Warning!!! Please update initial mean for peak"<<i<<" with fit result above and try it again."<<std::endl;
	if(abs(tems[i] - gsigma[i])>10)std::cout<<"Warning!!! Please update initial sigma for peak"<<i<<" with fit result above and try it again."<<std::endl;
}
//calibration parameters
cCalib->cd(3);
TGraph *fCalib;
if(nCal==3)fCalib = new TGraph(3,xx,yy);
if(nCal==6)fCalib = new TGraph(6,xx,yy);
fCalib->SetTitle("Calibration");
fCalib->GetYaxis()->SetTitle("Energy[keV]");
fCalib->GetXaxis()->SetTitle("Integral");
fCalib->SetMarkerSize(2);
fCalib->Draw("AP*");
TFitResultPtr fpar = fCalib->Fit("pol1","S");
if(fSAVE){
	gCalibPar[0] = fpar->Parameter(0);
	gCalibPar[1] = fpar->Parameter(1);
	std::cout<<"energy calibration parameters updated."<<std::endl;
}
std::cout<<"Fit result:"<<std::endl;
std::cout<<"par0 = "<<gCalibPar[0]<<std::endl;
std::cout<<"par1 = "<<gCalibPar[1]<<std::endl;
std::cout<<"Calibration complete!"<<std::endl;
}
*/
void energyCalib(const int npoints=6, bool fSAVE = false){
	if(gEref[0] < 0){
		std::cout<<"Error418!!! Please set energy reference before the calibration."<<std::endl;
		std::cout<<"setMeans(E1,E2,..)"<<std::endl;
		std::cout<<"setEnergyRef(E1,E2,..)"<<std::endl;
		return;
	}
	vector<float>xx; //Eraw; [MeV]
	vector<float>yy; //Ecal; [MeV]
	xx.clear();
	yy.clear();
	for(int ii=0;ii<npoints;ii++){
		xx.push_back(gmean[ii]);
		yy.push_back(gEref[ii]);
	}
	TGraph *fCalib;
	//fCalib = new TGraph(npoints,xx,yy);
	fCalib = new TGraph(npoints,&xx[0],&yy[0]);
	fCalib->SetTitle("Calibration");
	fCalib->GetYaxis()->SetTitle("Energy[MeV]");
	fCalib->GetXaxis()->SetTitle("Eraw[arb.]");
	fCalib->SetMarkerSize(2);
	fCalib->Draw("AP*");
	TFitResultPtr fpar = fCalib->Fit("pol1","S");
	if(gFebexCh>15){
		std::cout<<"Error419!!! gFebexCh can not > 15."<<std::endl;
		return;
	}
	if(fSAVE){
		gCalibPar_b[gFebexCh] = fpar->Parameter(0);
		gCalibPar_s[gFebexCh] = fpar->Parameter(1);
		std::cout<<"--->Energy calibration parameters updated."<<std::endl;
	}
	std::cout<<"--->Fitting result of channel :"<<gFebexCh<<std::endl;
	if(gCalibPar_b[gFebexCh] !=0)std::cout<<"par0 = "<<gCalibPar_b[gFebexCh]<<std::endl;
	if(gCalibPar_b[gFebexCh] !=0)std::cout<<"par1 = "<<gCalibPar_s[gFebexCh]<<std::endl;
	std::cout<<"--->Calibration complete!"<<std::endl;
}

void getEnergyResolution(int fnpeaks = 3){
	std::cout<<"--->You will fit "<<fnpeaks<<" peaks from here."<<std::endl;
	//gaus fit
	TF1 *fgaus;
	vector<char*> fitFuncs;
	fitFuncs.clear();
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)+gaus(10)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)");
	fitFuncs.push_back((char*)"pol0(0)+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)");
	//std::cout<<"N of funs = "<<fitFuncs.size()<<std::endl;
	if(fnpeaks <= fitFuncs.size()){
		fgaus = new TF1("fgaus",fitFuncs[fnpeaks-1],0,4000);
		for(int i=0;i<fnpeaks;i++){
			fgaus->SetParameter(1+3*i,gamp[i]);
			fgaus->SetParameter(2+3*i,gmean[i]);
			fgaus->SetParameter(3+3*i,gsigma[i]);
			fgaus->SetParLimits(1+3*i,gamp[i]-5,gamp[i]+5);
			fgaus->SetParLimits(2+3*i,gmean[i]-gParLimit,gmean[i]+gParLimit);
			fgaus->SetParLimits(3+3*i,gsigma[i]-gParLimit/10.,gsigma[i]+gParLimit/10.);
		}
	}
	else{
		std::cout<<"Error414!!! Nonvalid peaks number. Now the function only supports up to "<<fitFuncs.size()<<" peaks."<<std::endl;
		return;
	}
	TCanvas *c_res = new TCanvas("get energy resolution","get energy resolution",10,10,800,800);
	showEnergySpectrum();
	ghist->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);
	ghist->GetYaxis()->SetRangeUser(0,*std::max_element(std::begin(gamp),std::end(gamp))*2.5);
	ghist->Draw();
	ghist->Fit("fgaus","R");

	double fitres[30];
	fgaus->GetParameters(fitres);
	const Double_t *fitresErr = fgaus->GetParErrors();//par errors
	Double_t meanErr[30],sigErr[30];
	//individual plots
	vector <TF1*> fdrawGaus;
	fdrawGaus.clear();
	for(int i=0;i<fnpeaks;i++){
		gamp[i] = fitres[1+i*3];
		gmean[i] = fitres[2+i*3];
		gsigma[i] = fitres[3+i*3];
		meanErr[i] = fitresErr[2+i*3];
		sigErr[i] = fitresErr[3+i*3];
		gEres[i] = gsigma[i]*2.355/gmean[i]*100;//%
		fdrawGaus.push_back(new TF1(TString::Format("fgaus%d",i),"gaus",0,2000));
		for(int ii=0;ii<3;ii++)fdrawGaus.back()->SetParameter(ii,fitres[1+i*3+ii]);
		fdrawGaus.back()->Draw("same");
		fdrawGaus.back()->SetLineColor(1);
	}
	//*** legend ***//
	TText text1;
	text1.SetTextAlign(31);
	text1.SetTextFont(43);
	text1.SetTextSize(20);
	text1.DrawTextNDC(XtoPad(0.4),YtoPad(0.9),"Energy resolution:");

	std::cout<<"--->Limit for pars fitting now is "<<gParLimit<<" for Mean and "<<gParLimit/10.<<" for Sigma."<<std::endl;
	std::cout<<"--->To adjust this limit, please run 'setFitParLimit(NEW_VALUE)'and 'energyCalib()',respectively.."<<std::endl;
	std::cout<<"--->Fit results(only errors of fit pars were considered):"<<std::endl;
	std::cout<<"================================="<<std::endl;
	std::cout<<"--->Energy resolution results with data from febex channel:"<<gFebexCh<<std::endl;
	TString E_res;
	for(int ii=0;ii<fnpeaks;ii++){
		E_res = Form("dE/E = (%.2f +/- %.2f)%%.",gEres[ii],calculateResErr(gEres[ii],gmean[ii],meanErr[ii],gsigma[ii],sigErr[ii]));
		std::cout<<"peak"<<ii<<" = "<<gmean[ii]<<"; with sigma = "<<gsigma[ii]<<" ;"<<E_res<<std::endl;
		//std::cout<<"peak"<<ii<<" = "<<gmean[ii]<<"; with sigma = "<<gsigma[ii]<<" ;deltE/E = ("<<gEres[ii]<<" +/- "<<calculateResErr(gEres[ii],gmean[ii],meanErr[ii],gsigma[ii],sigErr[ii])<<")%"<<std::endl;
		text1.DrawTextNDC(XtoPad(0.45),YtoPad(0.85-0.05*ii),E_res);
	}
}

void fineEnergyCalib(){

	showEnergySpectrum();
	float fpeak[6] = {5106,5156,5443,5486,5763,5806};
	//int gParLimit = 12;
	double tema[6];//amp
	double temm[6];//mean
	double tems[6];//sigma
	if(gcnt_FEC==0)for(int i =0;i<3;i++)gsigma[i]/=2.;
	gcnt_FEC++;

	TCanvas *cCalib2 = new TCanvas("Fine Energy calibration with 6 peaks","Fine Energy calibration",10,80,900,900);
	cCalib2->Divide(2,2);
	cCalib2->cd(1);
	ghist->Draw();
	TH1F *hcalib = (TH1F*)ghist->Clone();
	cCalib2->cd(2);
	hcalib->Draw();
	hcalib->GetXaxis()->SetRangeUser(gESrange[0],gESrange[1]);
	//hcalib->GetXaxis()->SetRangeUser(4000,7000);
	hcalib->SetTitle("6 peaks Fit");
	//gaus fit

	TF1 *fsum2 = new TF1("fsum2","pol0+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)+gaus(16)",4000,7000);
	fsum2->SetParameter(0,0);
	//source 1
	fsum2->SetParameter(1,gamp[0]/2.);
	fsum2->SetParameter(2,fpeak[0]);
	fsum2->SetParameter(3,gsigma[0]);
	fsum2->SetParameter(5,fpeak[1]);
	fsum2->SetParameter(6,gsigma[0]);
	//source 2
	fsum2->SetParameter(7,gamp[1]/2.);
	fsum2->SetParameter(8,fpeak[2]);
	fsum2->SetParameter(9,gsigma[1]);
	fsum2->SetParameter(11,fpeak[3]);
	fsum2->SetParameter(12,gsigma[1]);
	//source 3
	fsum2->SetParameter(13,gamp[2]/2.);
	fsum2->SetParameter(14,fpeak[4]);
	fsum2->SetParameter(15,gsigma[2]);
	fsum2->SetParameter(17,fpeak[5]);
	fsum2->SetParameter(18,gsigma[2]);

	fsum2->SetParLimits(1,gamp[0]/6.,0.5*gamp[0]);
	fsum2->SetParLimits(2,fpeak[0]-gParLimit,fpeak[0]+gParLimit);
	fsum2->SetParLimits(3,gsigma[0]-gParLimit/10.,gsigma[0]+gParLimit/10.);
	fsum2->SetParLimits(5,fpeak[1]-gParLimit,fpeak[1]+gParLimit);
	fsum2->SetParLimits(6,gsigma[0]-gParLimit/10.,gsigma[0]+gParLimit/10.);

	fsum2->SetParLimits(7,gamp[1]/6.,0.5*gamp[1]);
	fsum2->SetParLimits(8,fpeak[2]-gParLimit,fpeak[2]+gParLimit);
	fsum2->SetParLimits(9,gsigma[1]-gParLimit/10.,gsigma[1]+gParLimit/10.);
	fsum2->SetParLimits(11,fpeak[3]-gParLimit,fpeak[3]+gParLimit);
	fsum2->SetParLimits(12,gsigma[1]-gParLimit/10.,gsigma[1]+gParLimit/10.);

	fsum2->SetParLimits(13,gamp[2]/6.,0.5*gamp[2]);
	fsum2->SetParLimits(14,fpeak[4]-gParLimit,fpeak[4]+gParLimit);
	fsum2->SetParLimits(15,gsigma[2]-gParLimit/10.,gsigma[2]+gParLimit/10.);
	fsum2->SetParLimits(17,fpeak[5]-gParLimit,fpeak[5]+gParLimit);
	fsum2->SetParLimits(18,gsigma[2]-gParLimit/10.,gsigma[2]+gParLimit/10.);

	fsum2->SetLineColor(kBlack);
	hcalib->GetXaxis()->SetTitle("After calibration[keV]");
	//for(int i=0;i<6;i++)temm[i] = fpeak[i];

	hcalib->Fit("fsum2","R");

	//print out mean and sigma
	double fitres[20];
	fsum2->GetParameters(fitres);
	for(int i=0;i<6;i++)tema[i] = fitres[1+i*3];
	for(int i=0;i<6;i++)temm[i] = fitres[2+i*3];
	for(int i=0;i<6;i++)tems[i] = fitres[3+i*3];
	std::cout<<"*Limit for pars fitting now is "<<gParLimit<<" for Mean and "<<gParLimit/10.<<" for Sigma."<<std::endl;
	std::cout<<"Initialized/Expected mean:"<<std::endl;
	for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<fpeak[i]<<std::endl;
	std::cout<<"Fit results(from left to right):"<<std::endl;
	for(int i=0;i<6;i++)std::cout<<"peak"<<i<<" = "<<temm[i]<<std::endl;
	for(int i=0;i<6;i++)std::cout<<"sigma"<<i<<" = "<<tems[i]<<std::endl;
	std::cout<<"Calibration complete!"<<std::endl;

	//draw 6 peaks
	vector<TF1*> drPeaks;
	drPeaks.clear();

	for(int i=0;i<6;i++){
		drPeaks.push_back( new TF1(Form("peak%d",i),"gaus",4000,7000) );
		drPeaks.at(i)->SetTitle(Form("peaks%d",i) );
		drPeaks.at(i)->SetParameter(0,tema[i]);
		drPeaks.at(i)->SetParameter(1,temm[i]);
		drPeaks.at(i)->SetParameter(2,tems[i]);
		drPeaks.at(i)->SetLineColor(i+1);
		drPeaks.at(i)->SetLineStyle(3);
		drPeaks.at(i)->SetLineWidth(5);
		drPeaks.at(i)->Draw("same");
	}
	//drPeaks.at(0)->Draw("same");

	//linear fit with 6 points
	//to be continued
	//calibration parameters
	/*
	   cCalib2->cd(3);
	   TGraph *gCalib = new TGraph(6,xx,yy);
	   gCalib->SetTitle("Calibration");
	   gCalib->GetYaxis()->SetTitle("Energy[keV]");
	   gCalib->GetXaxis()->SetTitle("Integral");
	   gCalib->SetMarkerSize(2);
	   gCalib->Draw("AP*");
	   TFitResultPtr fpar = gCalib->Fit("pol1","S");
	   if(fSAVE){
	   gCalibPar[0] = fpar->Parameter(0);
	   gCalibPar[1] = fpar->Parameter(1);
	   std::cout<<"energy calibration parameters updated."<<std::endl;
	   }
	   std::cout<<"Fit result:"<<std::endl;
	   std::cout<<"par0 = "<<gCalibPar[0]<<std::endl;
	   std::cout<<"par1 = "<<gCalibPar[1]<<std::endl;
	   std::cout<<"Calibration complete!"<<std::endl;
	   */
}
void initRoot(){
	optfile = new TFile(gOptRootFileName,"RECREATE");
	optTree = new TTree("tree","tree");

	//optTree->Branch("runnum",&runnum,"runnum/I");
	optTree->Branch("TRIGGER_c4",&TRIGGER_c4,"TRIGGER_c4/I");
	optTree->Branch("EVENTNO_c4",&EVENTNO_c4,"EVENTNO_c4/I");
	optTree->Branch("hit",&hit,"hit/i");
	optTree->Branch("febCh",febCh,"febCh[hit]/I");
	optTree->Branch("entry",&entry,"entry/I");
	optTree->Branch("entry_empty",entry_empty,"entry_empty[hit]/I");
	optTree->Branch("ts",runts,"runts[hit]/L");
	optTree->Branch("tsS",runts_S,"runts_S[hit]/L");
	optTree->Branch("Eraw",Eraw,"Eraw[hit]/D");
	optTree->Branch("Ecal",Ecal,"Ecal[hit]/D");
	optTree->Branch("T_cfd",T_cfd,"T_cfd[hit]/D");
	optTree->Branch("if_overflow",if_overflow,"if_overflow[hit]/I");
	optTree->Branch("traceLength",traceLength,"traceLength[hit]/i");
	optTree->Branch("tracesX",tracesX,"tracesX[hit][3000]/D");
	optTree->Branch("tracesY",tracesY,"tracesY[hit][3000]/D");
	std::cout<<"--->init output root file."<<std::endl;
}

void clearRoot(){
	TRIGGER_c4 = 0; //UInt_t
	EVENTNO_c4 = 0; //UInt_t
	hit = 0;
	entry = -1;
	for(int ii=0;ii<16;ii++){
		febCh[ii] = -1;
		entry_empty[ii] = -1;
		runts[ii] = -1;
		runts_S[ii] = -1;
		Eraw[ii] = -1;
		Ecal[ii] = -1;
		T_cfd[ii] = -9999;
		if_overflow[ii] = 0; //0, no; 1, yes
		traceLength[ii] = 0;//UInt_t
		for(int jj=0;jj<TRACE_LENGTH;jj++){
			tracesX[ii][jj] = -1;
			tracesY[ii][jj] = -1;
		}
	}
}

//void root_Clear(&Long64_t fts, &double fenergyr, &double fenergyc, &double fTime_cfd){
//	fts = -1;
//	fenergyr = -1;
//	fenergyc = -1;
//	fTime_cfd = -9999;
//}

//*** export root file ***//
//*** old version ***//
void saveAsRoot(TTree *fopttree, int frun, Long64_t fts, double fenergyr, double fenergyc, double fTime_cfd, TGraph *fgr){

	clearRoot();

	//EVENTNO_c4 = EVENTNO;
	entry = frun;
	//if(gBadEntry[gFebexCh])entry_empty[gFebexCh] = entry;
	runts[0] = fts;
	runts_S[0] = fts%1000000000;
	Eraw[0] = fenergyr;
	Ecal[0] = fenergyc;
	T_cfd[0] = fTime_cfd;
	if(gVerbose == 1)std::cout<<"test: "<<Eraw[0]<<" "<<Ecal[0]<<std::endl;
	if(fsaveTraces){
		int length = fgr->GetN();
		for(int i=0;i<length;i++){
			double x,y;
			fgr->GetPoint(i,x,y);
			if(gVerbose == 2)std::cout<<"in saveasroot: x = "<<x<<" y = "<<y<<std::endl;
			tracesX[0][i] = x;
			tracesY[0][i] = y;
		}
	}
	hit++;
	fopttree->Fill();
}

//*** export root file ***//
void saveAsRoot(struct dataCluster fopt){

	TRIGGER_c4 = fopt.trigger;
	EVENTNO_c4 = fopt.evtno;

	if(fopt.flength==0)return;
	if(fopt.fenergy_raw < gThreshold[fopt.fch])return;
	febCh[hit] = fopt.fch;//0-15
	entry = fopt.fentry;
	entry_empty[hit] = fopt.fentry_empty;
	runts[hit] = fopt.fts;
	runts_S[hit] = fopt.fts%1000000000;
	Eraw[hit] = fopt.fenergy_raw;
	Ecal[hit] = fopt.fenergy_cal;
	T_cfd[hit] = fopt.ftime_cfd;
	if_overflow[hit] = fopt.foverflow;
	int length = fopt.fgr->GetN();
	traceLength[hit] = length;//real trace length of the exported trace
	//traceLength[hit] = fopt.flength;//read from unpacked data
	if(gVerbose == 1)std::cout<<"test in saveasroot: "<<Eraw[hit]<<" "<<Ecal[hit]<<std::endl;
	if(fsaveTraces){
		for(int i=0;i<length;i++){
			double x,y;
			fopt.fgr->GetPoint(i,x,y);
			if(gVerbose == 2)std::cout<<"in saveasroot: x = "<<x<<" y = "<<y<<std::endl;
			tracesX[hit][i] = x;
			tracesY[hit][i] = y;
		}
	}
	hit++;
}

/*
   double getZERO(){

   }
   */
//https://doi.org/10.1016/j.apradiso.2020.109171
//B.J.Kim
TGraph* MWD(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error409. Empty graph."<<std::endl;
		return 0;
	}
	if(gMWDGraph!=NULL){
		delete gMWDGraph;
		gMWDGraph=NULL;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;

	vector<double>MWD;
	vector<double>MWDx;
	int k0,MM,LL;
	double sum0=0;
	double tau = gDecaytime*gSampleRate/MHZtoNS;//3000 sample points
	double DM=0;
	double mwd=0;
	//********************//
	//---            -----//
	//  a-          -d    //
	//    -        -      //
	//     -      -       //
	//     b------c       //
	//********************//
	//
	MM=(int)gtrapezMovingWindowLength*gSampleRate/MHZtoNS;//length from b to d; [sample point]
	LL=(int)gRisingTime*gSampleRate/MHZtoNS;//length from a to b; [sample point]
											//flat on top/bottom: MM-LL; [sample point]
	k0=gtrapezSampleWindow[0];//[sample point]
	xx.clear();
	yy.clear();
	//test
	//std::cout<<"kk = "<<k0<<" MM = "<<MM<<" LL = "<<LL<<std::endl;

	MWD.clear();
	MWDx.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(y);
	}
	//std::cout<<"start from "<<Sx[0]<<" ns"<<std::endl;
	int fend = (int)gtrapezSampleWindow[1];
	if(isFebex && fend>=gFebexTraceL){
		std::cout<<"Error417!!!"<<std::endl;
		std::cout<<"Current trace length = "<<gFebexTraceL<<std::endl;
		std::cout<<"Trapez window should be < trace length. Please reduce your trapez sample window length."<<std::endl;
		return 0;
	}
	for(int kk = k0;kk < fend;kk=kk+1){
		DM=0;
		for(int j=kk-LL;j<=kk-1;j=j+1){
			if(j<1){
				std::cout<<"Error410!!! Please decrease your rising time OR shift the start point of sample window to the right."<<std::endl;
				return 0;
			}
			sum0=0;
			for(int i=kk-MM;i<=kk-1;i++){
				if(i<-200+1){
					std::cout<<"*kk-MM = "<<i<<std::endl;
					std::cout<<"Error411!!! Please decrease your Trapezoidal moving window length OR shift the start point of sample window to the right."<<std::endl;
					return 0;
				}
				sum0 += yy[i];
			}
			DM += (yy[kk] - yy[kk-MM] + 1.0*sum0/tau);
		}
		mwd = DM/LL;

		MWD.push_back(mwd);
		MWDx.push_back(xx[kk]);
	}

	//TGraph *fMWD = new TGraph(MWD.size(),&MWDx[0],&MWD[0]);
	gMWDGraph = new TGraph(MWD.size(),&MWDx[0],&MWD[0]);
	gMWDGraph->SetTitle("Trapezoidal Filter with Moving Window Deconvolution");
	return gMWDGraph;
}

TGraph* MWD2(TGraph *fgr){
	int length = 0 ;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error410. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;
	vector<double>SS;
	vector<double>Sx;
	int kk,LL,GG,k0;
	//30 and 2, read energy from max: better but not good enough.
	//3 and 30, read energy from max in (1.5,3): only single peak.
	LL=16;
	GG=5;
	xx.clear();
	yy.clear();
	SS.clear();
	Sx.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(y);
	}
	//TFA-MWD
	SS.push_back(0);
	Sx.push_back(xx[494]);
	//std::cout<<"start from "<<Sx[0]<<" ns"<<std::endl;
	//for(int i=1;i<50;i++){
	for(int i=1;i<(2*LL+GG);i++){
		kk=i+494;
		SS.push_back( SS[i-1] + yy[kk] - yy[kk-LL] + yy[kk-2*LL-GG] - yy[kk-LL-GG] );
		Sx.push_back(xx[kk]);
	}
	TGraph *fMWD = new TGraph(SS.size(),&Sx[0],&SS[0]);
	fMWD->SetTitle("Trapezoidal Filter with Moving Window Deconvolution");
	return fMWD;
}

TGraph* CRRC(TGraph *fgr){
	int length = 0;
	if(fgr == 0 || fgr->GetN()<1 ){
		std::cout<<"Error412. Empty graph."<<std::endl;
		return 0;
	}
	length = fgr->GetN();
	vector<double>xx;
	vector<double>yy;//raw data
	vector<double>SS;
	vector<double>Sx;
	float b1,b2,a0,a1,a2;
	int kk=0;
	b1=0.1;
	b2=0.1;
	a0=0.3;
	a1=0.3;
	a2=0.3;
	const int FSP = 400;//first sample point
	xx.clear();
	yy.clear();
	SS.clear();
	Sx.clear();
	for(int i=0;i<length;i++){
		double x,y;
		fgr->GetPoint(i,x,y);
		xx.push_back(x);
		yy.push_back(y);
	}
	//
	SS.push_back(0);
	Sx.push_back(xx[FSP]);
	SS.push_back(0);
	Sx.push_back(xx[FSP+1]);
	//std::cout<<"start from "<<Sx[0]<<" ns"<<std::endl;
	for(int i=2;i<300;i++){
		kk=i+FSP;
		SS.push_back( b1*SS[i-1] + b2*SS[i-2] + a0*yy[kk] + a1*yy[kk-1] + a2*yy[kk-2] );
		Sx.push_back(xx[kk]);
	}
	TGraph *fCRRC = new TGraph(SS.size(),&Sx[0],&SS[0]);
	fCRRC->SetTitle("CR-RC filter");
	return fCRRC;
}

TGraph* graphAdd(TGraph *fgr1, TGraph *fgr2, double fpar1, double fpar2){

	int length1 = 0;
	int length2 = 0;
	if(fgr1 == 0 || fgr1->GetN()<1 || fgr2 == 0 || fgr2->GetN()<1 ){
		std::cout<<"Error412. Empty graph."<<std::endl;
		return 0;
	}
	length1 = fgr1->GetN();
	length2 = fgr2->GetN();
	if(length1 != length2){
		std::cout<<"Error413. Different graph format."<<std::endl;
		return 0;
	}
	vector<double>xx;
	vector<double>yy;
	xx.clear();
	yy.clear();
	for(int i=0;i<length1;i++){
		double x1,y1;
		double x2,y2;
		fgr1->GetPoint(i,x1,y1);
		fgr2->GetPoint(i,x2,y2);
		xx.push_back(x1);
		yy.push_back(y1*fpar1+y2*fpar2);
	}
	TGraph *fadd = new TGraph(xx.size(),&xx[0],&yy[0]);
	fadd->SetTitle("Add");
	return fadd;
}
/*
   TGraph* graphDrawSame(TGraph *fgr1, TGraph *fgr2){

   }
   */

inline void closeRoot(){
	//inline void closeRoot(TTree *fopttree, TFile *foptfile){
	optTree->Write("",TObject::kOverwrite);//save only the new version of the tree
	optfile->Close();
	std::cout<<"******************************************"<<std::endl;
	std::cout<<"Data has been saved to "<<gOptRootFileName<<"."<<std::endl;
	//std::cout<<"*"<<tracesX.size()<<" Samples have been kept in the data."<<std::endl;
	if(gFebexCh>15){
		std::cout<<"Error419!!! gFebexCh can not > 15."<<std::endl;
		return;
	}
	if(gMultiChAnaMode)
		std::cout<<"Energy calibration parameters used in this analysis:"<<std::endl;
	for(int cc=0;cc<16;cc++){
		if(!gMultiCh[cc])continue;//cc = fchannel; by default false
		std::cout<<Form("Ch%d",cc)<<": "<<gCalibPar_b[cc]<<"  "<<gCalibPar_s[cc]<<std::endl;
	}
	if(gSingleChAnaMode)
	{
		std::cout<<"Energy calibration parameters used in this analysis:"<<std::endl;
		std::cout<<Form("Ch%d",gFebexCh)<<": "<<gCalibPar_b[gFebexCh]<<"  "<<gCalibPar_s[gFebexCh]<<std::endl;
	}
	std::cout<<"******************************************"<<std::endl;
}

#endif
