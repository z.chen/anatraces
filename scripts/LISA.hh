#ifndef __LISA_HH__
#define __LISA_HH__
#include <TBox.h>

struct LISA_FRAME {

	int ID;
	vector<float>fx1;
	vector<float>fy1;
	vector<float>fx2;
	vector<float>fy2;
	vector<TBox*> fbox;

	LISA_FRAME():ID(0) {}
	void clear() {
		ID = 0;
		fx1.clear();
		fy1.clear();
		fx2.clear();
		fy2.clear();
		fbox.clear();
	}
};

#endif
