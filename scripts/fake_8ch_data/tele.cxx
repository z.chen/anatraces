#ifndef tele_cxx
#define tele_cxx
#include "tele.hh"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void tele::Loop(TTree *opttree)
{
	//   In a ROOT session, you can do:
	//      root> .L tele.C
	//      root> tele t
	//      root> t.GetEntry(12); // Fill t data members with entry number 12
	//      root> t.Show();       // Show values of entry 12
	//      root> t.Show(16);     // Read and show values of entry 16
	//      root> t.Loop();       // Loop on all entries
	//

	//     This is the loop skeleton where:
	//    jentry is the global entry number in the chain
	//    ientry is the entry number in the current Tree
	//  Note that the argument to GetEntry must be:
	//    jentry for TChain::GetEntry
	//    ientry for TTree::GetEntry and TBranch::GetEntry
	//
	//       To read only selected branches, Insert statements like:
	// METHOD1:
	//    fChain->SetBranchStatus("*",0);  // disable all branches
	//    fChain->SetBranchStatus("branchname",1);  // activate branchname
	// METHOD2: replace line
	//    fChain->GetEntry(jentry);       //read all branches
	//by  b_branchname->GetEntry(ientry); //read only this branch

	if (fChain == 0) return;

	Long64_t nentries = fChain->GetEntriesFast();
	std::cout<<"--->total entries = "<<nentries<<std::endl;

	fChain->SetBranchStatus("*",0);  // disable all branches
	fChain->SetBranchStatus("lisa_data1traces1",1);  // activate branchname
	fChain->SetBranchStatus("lisa_data1traces1I",1);  // activate branchname
	fChain->SetBranchStatus("lisa_data1traces1v",1);  // activate branchname

	//for (Long64_t jentry=0; jentry<10;jentry++)
	for (Long64_t jentry=0; jentry<nentries;jentry++) 
	{
		Long64_t ientry = LoadTree(jentry);
		if (ientry < 0){
			std::cout<<"centry < 0."<<std::endl;
			break;
		}
		fChain->GetEntry(jentry);

		clearOpt();

		double frandom = gRandom->Uniform(0,4);
		//std::cout<<"random = "<<frandom<<std::endl;
		bool fch0 = frandom > 0 && frandom < 1;
		bool fch1 = frandom > 1 && frandom < 2;
		bool fch2 = frandom > 2 && frandom < 3;
		bool fch3 = frandom > 3 && frandom < 4;
		//std::cout<<"selection:fch0 = "<<fch0<<std::endl;
		//std::cout<<"selection:fch1 = "<<fch1<<std::endl;
		//std::cout<<"selection:fch2 = "<<fch2<<std::endl;
		//std::cout<<"selection:fch3 = "<<fch3<<std::endl;

		if(fch0){
			fake_lisa_data1traces1 = lisa_data1traces1;
			fake_lisa_data1traces5 = lisa_data1traces1;
			for(int jj=0;jj<lisa_data1traces1;jj++){
				fake_lisa_data1traces1I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces1v[jj] = lisa_data1traces1v[jj];
				fake_lisa_data1traces5I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces5v[jj] = lisa_data1traces1v[jj];
			}
		}
		if(fch1){
			fake_lisa_data1traces2 = lisa_data1traces1;
			fake_lisa_data1traces6 = lisa_data1traces1;
			for(int jj=0;jj<lisa_data1traces1;jj++){
				fake_lisa_data1traces2I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces2v[jj] = lisa_data1traces1v[jj];
				fake_lisa_data1traces6I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces6v[jj] = lisa_data1traces1v[jj];
			}
		}
		if(fch2){
			fake_lisa_data1traces3 = lisa_data1traces1;
			fake_lisa_data1traces7 = lisa_data1traces1;
			for(int jj=0;jj<lisa_data1traces1;jj++){
				fake_lisa_data1traces3I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces3v[jj] = lisa_data1traces1v[jj];
				fake_lisa_data1traces7I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces7v[jj] = lisa_data1traces1v[jj];
			}
		}
		if(fch3){
			fake_lisa_data1traces4 = lisa_data1traces1;
			fake_lisa_data1traces8 = lisa_data1traces1;
			for(int jj=0;jj<lisa_data1traces1;jj++){
				fake_lisa_data1traces4I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces4v[jj] = lisa_data1traces1v[jj];
				fake_lisa_data1traces8I[jj] = lisa_data1traces1I[jj];
				fake_lisa_data1traces8v[jj] = lisa_data1traces1v[jj];
			}
		}

		//continue;
		/*
		   fake_lisa_data1traces1 = lisa_data1traces1;
		   fake_lisa_data1traces2 = lisa_data1traces1;
		   fake_lisa_data1traces3 = lisa_data1traces1;
		   fake_lisa_data1traces4 = lisa_data1traces1;
		   fake_lisa_data1traces5 = lisa_data1traces1;
		   fake_lisa_data1traces6 = lisa_data1traces1;
		   fake_lisa_data1traces7 = lisa_data1traces1;
		   fake_lisa_data1traces8 = lisa_data1traces1;

		   for(int jj=0;jj<lisa_data1traces1;jj++){
		   fake_lisa_data1traces1I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces1v[jj] = lisa_data1traces1v[jj];
		   fake_lisa_data1traces2I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces2v[jj] = lisa_data1traces1v[jj];
		   fake_lisa_data1traces3I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces3v[jj] = lisa_data1traces1v[jj];
		   fake_lisa_data1traces4I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces4v[jj] = lisa_data1traces1v[jj];
		   fake_lisa_data1traces5I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces5v[jj] = lisa_data1traces1v[jj];
		   fake_lisa_data1traces6I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces6v[jj] = lisa_data1traces1v[jj];
		   fake_lisa_data1traces7I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces7v[jj] = lisa_data1traces1v[jj];
		   fake_lisa_data1traces8I[jj] = lisa_data1traces1I[jj];
		   fake_lisa_data1traces8v[jj] = lisa_data1traces1v[jj];
		   }
		   */
		//std::cout<<"test7"<<std::endl;
		opttree->Fill();
		std::cout<<"running..."<<jentry<<"/"<<nentries;
		fflush(stdout);
		printf("\r");
	}
	std::cout<<"--->end of loop."<<std::endl;
}


void tele::Sort(){

	//*** set opt root file ***//
	TFile *fopt = new TFile("fake8ch_run0089_new.root","RECREATE");
	//std::cout<<"test1"<<std::endl;
	TTree *optTree = new TTree("h101","fake_8ch_tree");
	setOptBranch(optTree);
	//std::cout<<"test2"<<std::endl;

	Loop(optTree);
	//std::cout<<"test3"<<std::endl;

	optTree->Write("",TObject::kOverwrite);//save only the new version of the tree
	fopt->Close();
	std::cout<<"--->new file is closed."<<std::endl;
}

tele::tele(TTree *tree) : fChain(0) 
{
	// if parameter tree is not specified (or zero), connect the file
	// used to generate this class and read the Tree.
	if (tree == 0) {
		TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("tokyo_11dec_0089.root");
		if (!f || !f->IsOpen()) {
			f = new TFile("tokyo_11dec_0089.root");
		}
		f->GetObject("h101",tree);

	}
	Init(tree);
}

tele::~tele()
{
	if (!fChain) return;
	delete fChain->GetCurrentFile();
}

Int_t tele::GetEntry(Long64_t entry)
{
	// Read contents of entry.
	if (!fChain) return 0;
	return fChain->GetEntry(entry);
}
Long64_t tele::LoadTree(Long64_t entry)
{
	// Set the environment to read one entry
	if (!fChain) return -5;
	Long64_t centry = fChain->LoadTree(entry);
	if (centry < 0) return centry;
	if (fChain->GetTreeNumber() != fCurrent) {
		fCurrent = fChain->GetTreeNumber();
		Notify();
	}
	return centry;
}

void tele::Init(TTree *tree)
{
	// The Init() function is called when the selector needs to initialize
	// a new tree or chain. Typically here the branch addresses and branch
	// pointers of the tree will be set.
	// It is normally not necessary to make changes to the generated
	// code, but the routine can be extended by the user if needed.
	// Init() will be called many times when running on PROOF
	// (once per file to be processed).

	// Set branch addresses and branch pointers
	if (!tree) return;
	fChain = tree;
	fCurrent = -1;
	fChain->SetMakeClass(1);

	fChain->SetBranchAddress("TRIGGER", &TRIGGER, &b_TRIGGER);
	fChain->SetBranchAddress("EVENTNO", &EVENTNO, &b_EVENTNO);
	fChain->SetBranchAddress("lisa_ts_subsystem_id", &lisa_ts_subsystem_id, &b_lisa_ts_subsystem_id);
	fChain->SetBranchAddress("lisa_ts_t1", &lisa_ts_t1, &b_lisa_ts_t1);
	fChain->SetBranchAddress("lisa_ts_t2", &lisa_ts_t2, &b_lisa_ts_t2);
	fChain->SetBranchAddress("lisa_ts_t3", &lisa_ts_t3, &b_lisa_ts_t3);
	fChain->SetBranchAddress("lisa_ts_t4", &lisa_ts_t4, &b_lisa_ts_t4);
	fChain->SetBranchAddress("lisa_data1event_trigger_time_hi", &lisa_data1event_trigger_time_hi, &b_lisa_data1event_trigger_time_hi);
	fChain->SetBranchAddress("lisa_data1event_trigger_time_lo", &lisa_data1event_trigger_time_lo, &b_lisa_data1event_trigger_time_lo);
	fChain->SetBranchAddress("lisa_data1hit_pattern", &lisa_data1hit_pattern, &b_lisa_data1hit_pattern);
	fChain->SetBranchAddress("lisa_data1num_channels_fired", &lisa_data1num_channels_fired, &b_lisa_data1num_channels_fired);
	fChain->SetBranchAddress("lisa_data1channel_id", &lisa_data1channel_id, &b_lisa_data1channel_id);
	fChain->SetBranchAddress("lisa_data1channel_idI", lisa_data1channel_idI, &b_lisa_data1channel_idI);
	fChain->SetBranchAddress("lisa_data1channel_idv", lisa_data1channel_idv, &b_lisa_data1channel_idv);
	fChain->SetBranchAddress("lisa_data1channel_trigger_time_hi", &lisa_data1channel_trigger_time_hi, &b_lisa_data1channel_trigger_time_hi);
	fChain->SetBranchAddress("lisa_data1channel_trigger_time_hiI", lisa_data1channel_trigger_time_hiI, &b_lisa_data1channel_trigger_time_hiI);
	fChain->SetBranchAddress("lisa_data1channel_trigger_time_hiv", lisa_data1channel_trigger_time_hiv, &b_lisa_data1channel_trigger_time_hiv);
	fChain->SetBranchAddress("lisa_data1channel_trigger_time_lo", &lisa_data1channel_trigger_time_lo, &b_lisa_data1channel_trigger_time_lo);
	fChain->SetBranchAddress("lisa_data1channel_trigger_time_loI", lisa_data1channel_trigger_time_loI, &b_lisa_data1channel_trigger_time_loI);
	fChain->SetBranchAddress("lisa_data1channel_trigger_time_lov", lisa_data1channel_trigger_time_lov, &b_lisa_data1channel_trigger_time_lov);
	fChain->SetBranchAddress("lisa_data1pileup1", &lisa_data1pileup1, &b_lisa_data1pileup1);
	fChain->SetBranchAddress("lisa_data1pileup2", &lisa_data1pileup2, &b_lisa_data1pileup2);
	fChain->SetBranchAddress("lisa_data1pileup3", &lisa_data1pileup3, &b_lisa_data1pileup3);
	fChain->SetBranchAddress("lisa_data1pileup4", &lisa_data1pileup4, &b_lisa_data1pileup4);
	fChain->SetBranchAddress("lisa_data1pileup5", &lisa_data1pileup5, &b_lisa_data1pileup5);
	fChain->SetBranchAddress("lisa_data1pileup6", &lisa_data1pileup6, &b_lisa_data1pileup6);
	fChain->SetBranchAddress("lisa_data1pileup7", &lisa_data1pileup7, &b_lisa_data1pileup7);
	fChain->SetBranchAddress("lisa_data1pileup8", &lisa_data1pileup8, &b_lisa_data1pileup8);
	fChain->SetBranchAddress("lisa_data1pileup9", &lisa_data1pileup9, &b_lisa_data1pileup9);
	fChain->SetBranchAddress("lisa_data1pileup10", &lisa_data1pileup10, &b_lisa_data1pileup10);
	fChain->SetBranchAddress("lisa_data1pileup11", &lisa_data1pileup11, &b_lisa_data1pileup11);
	fChain->SetBranchAddress("lisa_data1pileup12", &lisa_data1pileup12, &b_lisa_data1pileup12);
	fChain->SetBranchAddress("lisa_data1pileup13", &lisa_data1pileup13, &b_lisa_data1pileup13);
	fChain->SetBranchAddress("lisa_data1pileup14", &lisa_data1pileup14, &b_lisa_data1pileup14);
	fChain->SetBranchAddress("lisa_data1pileup15", &lisa_data1pileup15, &b_lisa_data1pileup15);
	fChain->SetBranchAddress("lisa_data1pileup16", &lisa_data1pileup16, &b_lisa_data1pileup16);
	fChain->SetBranchAddress("lisa_data1overflow1", &lisa_data1overflow1, &b_lisa_data1overflow1);
	fChain->SetBranchAddress("lisa_data1overflow2", &lisa_data1overflow2, &b_lisa_data1overflow2);
	fChain->SetBranchAddress("lisa_data1overflow3", &lisa_data1overflow3, &b_lisa_data1overflow3);
	fChain->SetBranchAddress("lisa_data1overflow4", &lisa_data1overflow4, &b_lisa_data1overflow4);
	fChain->SetBranchAddress("lisa_data1overflow5", &lisa_data1overflow5, &b_lisa_data1overflow5);
	fChain->SetBranchAddress("lisa_data1overflow6", &lisa_data1overflow6, &b_lisa_data1overflow6);
	fChain->SetBranchAddress("lisa_data1overflow7", &lisa_data1overflow7, &b_lisa_data1overflow7);
	fChain->SetBranchAddress("lisa_data1overflow8", &lisa_data1overflow8, &b_lisa_data1overflow8);
	fChain->SetBranchAddress("lisa_data1overflow9", &lisa_data1overflow9, &b_lisa_data1overflow9);
	fChain->SetBranchAddress("lisa_data1overflow10", &lisa_data1overflow10, &b_lisa_data1overflow10);
	fChain->SetBranchAddress("lisa_data1overflow11", &lisa_data1overflow11, &b_lisa_data1overflow11);
	fChain->SetBranchAddress("lisa_data1overflow12", &lisa_data1overflow12, &b_lisa_data1overflow12);
	fChain->SetBranchAddress("lisa_data1overflow13", &lisa_data1overflow13, &b_lisa_data1overflow13);
	fChain->SetBranchAddress("lisa_data1overflow14", &lisa_data1overflow14, &b_lisa_data1overflow14);
	fChain->SetBranchAddress("lisa_data1overflow15", &lisa_data1overflow15, &b_lisa_data1overflow15);
	fChain->SetBranchAddress("lisa_data1overflow16", &lisa_data1overflow16, &b_lisa_data1overflow16);
	fChain->SetBranchAddress("lisa_data1channel_cfd", &lisa_data1channel_cfd, &b_lisa_data1channel_cfd);
	fChain->SetBranchAddress("lisa_data1channel_cfdI", lisa_data1channel_cfdI, &b_lisa_data1channel_cfdI);
	fChain->SetBranchAddress("lisa_data1channel_cfdv", lisa_data1channel_cfdv, &b_lisa_data1channel_cfdv);
	fChain->SetBranchAddress("lisa_data1channel_energy", &lisa_data1channel_energy, &b_lisa_data1channel_energy);
	fChain->SetBranchAddress("lisa_data1channel_energyI", lisa_data1channel_energyI, &b_lisa_data1channel_energyI);
	fChain->SetBranchAddress("lisa_data1channel_energyv", lisa_data1channel_energyv, &b_lisa_data1channel_energyv);
	fChain->SetBranchAddress("lisa_data1traces1", &lisa_data1traces1, &b_lisa_data1traces1);
	fChain->SetBranchAddress("lisa_data1traces1I", lisa_data1traces1I, &b_lisa_data1traces1I);
	fChain->SetBranchAddress("lisa_data1traces1v", lisa_data1traces1v, &b_lisa_data1traces1v);
	fChain->SetBranchAddress("lisa_data1traces2", &lisa_data1traces2, &b_lisa_data1traces2);
	fChain->SetBranchAddress("lisa_data1traces2I", &lisa_data1traces2I, &b_lisa_data1traces2I);
	fChain->SetBranchAddress("lisa_data1traces2v", &lisa_data1traces2v, &b_lisa_data1traces2v);
	fChain->SetBranchAddress("lisa_data1traces3", &lisa_data1traces3, &b_lisa_data1traces3);
	fChain->SetBranchAddress("lisa_data1traces3I", &lisa_data1traces3I, &b_lisa_data1traces3I);
	fChain->SetBranchAddress("lisa_data1traces3v", &lisa_data1traces3v, &b_lisa_data1traces3v);
	fChain->SetBranchAddress("lisa_data1traces4", &lisa_data1traces4, &b_lisa_data1traces4);
	fChain->SetBranchAddress("lisa_data1traces4I", &lisa_data1traces4I, &b_lisa_data1traces4I);
	fChain->SetBranchAddress("lisa_data1traces4v", &lisa_data1traces4v, &b_lisa_data1traces4v);
	fChain->SetBranchAddress("lisa_data1traces5", &lisa_data1traces5, &b_lisa_data1traces5);
	fChain->SetBranchAddress("lisa_data1traces5I", &lisa_data1traces5I, &b_lisa_data1traces5I);
	fChain->SetBranchAddress("lisa_data1traces5v", &lisa_data1traces5v, &b_lisa_data1traces5v);
	fChain->SetBranchAddress("lisa_data1traces6", &lisa_data1traces6, &b_lisa_data1traces6);
	fChain->SetBranchAddress("lisa_data1traces6I", &lisa_data1traces6I, &b_lisa_data1traces6I);
	fChain->SetBranchAddress("lisa_data1traces6v", &lisa_data1traces6v, &b_lisa_data1traces6v);
	fChain->SetBranchAddress("lisa_data1traces7", &lisa_data1traces7, &b_lisa_data1traces7);
	fChain->SetBranchAddress("lisa_data1traces7I", &lisa_data1traces7I, &b_lisa_data1traces7I);
	fChain->SetBranchAddress("lisa_data1traces7v", &lisa_data1traces7v, &b_lisa_data1traces7v);
	fChain->SetBranchAddress("lisa_data1traces8", &lisa_data1traces8, &b_lisa_data1traces8);
	fChain->SetBranchAddress("lisa_data1traces8I", &lisa_data1traces8I, &b_lisa_data1traces8I);
	fChain->SetBranchAddress("lisa_data1traces8v", &lisa_data1traces8v, &b_lisa_data1traces8v);
	fChain->SetBranchAddress("lisa_data1traces9", &lisa_data1traces9, &b_lisa_data1traces9);
	fChain->SetBranchAddress("lisa_data1traces9I", &lisa_data1traces9I, &b_lisa_data1traces9I);
	fChain->SetBranchAddress("lisa_data1traces9v", &lisa_data1traces9v, &b_lisa_data1traces9v);
	fChain->SetBranchAddress("lisa_data1traces10", &lisa_data1traces10, &b_lisa_data1traces10);
	fChain->SetBranchAddress("lisa_data1traces10I", &lisa_data1traces10I, &b_lisa_data1traces10I);
	fChain->SetBranchAddress("lisa_data1traces10v", &lisa_data1traces10v, &b_lisa_data1traces10v);
	fChain->SetBranchAddress("lisa_data1traces11", &lisa_data1traces11, &b_lisa_data1traces11);
	fChain->SetBranchAddress("lisa_data1traces11I", &lisa_data1traces11I, &b_lisa_data1traces11I);
	fChain->SetBranchAddress("lisa_data1traces11v", &lisa_data1traces11v, &b_lisa_data1traces11v);
	fChain->SetBranchAddress("lisa_data1traces12", &lisa_data1traces12, &b_lisa_data1traces12);
	fChain->SetBranchAddress("lisa_data1traces12I", &lisa_data1traces12I, &b_lisa_data1traces12I);
	fChain->SetBranchAddress("lisa_data1traces12v", &lisa_data1traces12v, &b_lisa_data1traces12v);
	fChain->SetBranchAddress("lisa_data1traces13", &lisa_data1traces13, &b_lisa_data1traces13);
	fChain->SetBranchAddress("lisa_data1traces13I", &lisa_data1traces13I, &b_lisa_data1traces13I);
	fChain->SetBranchAddress("lisa_data1traces13v", &lisa_data1traces13v, &b_lisa_data1traces13v);
	fChain->SetBranchAddress("lisa_data1traces14", &lisa_data1traces14, &b_lisa_data1traces14);
	fChain->SetBranchAddress("lisa_data1traces14I", &lisa_data1traces14I, &b_lisa_data1traces14I);
	fChain->SetBranchAddress("lisa_data1traces14v", &lisa_data1traces14v, &b_lisa_data1traces14v);
	fChain->SetBranchAddress("lisa_data1traces15", &lisa_data1traces15, &b_lisa_data1traces15);
	fChain->SetBranchAddress("lisa_data1traces15I", &lisa_data1traces15I, &b_lisa_data1traces15I);
	fChain->SetBranchAddress("lisa_data1traces15v", &lisa_data1traces15v, &b_lisa_data1traces15v);
	fChain->SetBranchAddress("lisa_data1traces16", &lisa_data1traces16, &b_lisa_data1traces16);
	fChain->SetBranchAddress("lisa_data1traces16I", &lisa_data1traces16I, &b_lisa_data1traces16I);
	fChain->SetBranchAddress("lisa_data1traces16v", &lisa_data1traces16v, &b_lisa_data1traces16v);
	Notify();
}

void tele::setOptBranch(TTree *tree)
{
	if (!tree) return;
	//tree->Branch("traceLength",       &fake_traceLength,      "fake_traceLength/i");//UInt_t->i
	//tree->Branch("lisa_data1traces",  fake_lisa_data1traces,  "fake_lisa_data1traces[16]/i");//UInt_t->i
	//tree->Branch("lisa_data1tracesI", fake_lisa_data1tracesI, "fake_lisa_data1tracesI[16][3000]/i");
	//tree->Branch("lisa_data1tracesv", fake_lisa_data1tracesv, "fake_lisa_data1tracesv[16][3000]/i");

	tree->Branch("lisa_data1traces1", &fake_lisa_data1traces1,  "fake_lisa_data1traces1/i");//UInt_t->i
	tree->Branch("lisa_data1traces1I", fake_lisa_data1traces1I, "fake_lisa_data1traces1I[fake_lisa_data1traces1]/i");
	tree->Branch("lisa_data1traces1v", fake_lisa_data1traces1v, "fake_lisa_data1traces1v[fake_lisa_data1traces1]/i");

	tree->Branch("lisa_data1traces2", &fake_lisa_data1traces2, "fake_lisa_data1traces2/i");//UInt_t->i
	tree->Branch("lisa_data1traces2I", fake_lisa_data1traces2I, "fake_lisa_data1traces2I[fake_lisa_data1traces2]/i");
	tree->Branch("lisa_data1traces2v", fake_lisa_data1traces2v, "fake_lisa_data1traces2v[fake_lisa_data1traces2]/i");

	tree->Branch("lisa_data1traces3", &fake_lisa_data1traces3, "fake_lisa_data1traces3/i");//UInt_t->i
	tree->Branch("lisa_data1traces3I", fake_lisa_data1traces3I, "fake_lisa_data1traces3I[fake_lisa_data1traces3]/i");
	tree->Branch("lisa_data1traces3v", fake_lisa_data1traces3v, "fake_lisa_data1traces3v[fake_lisa_data1traces3]/i");

	tree->Branch("lisa_data1traces4", &fake_lisa_data1traces4, "fake_lisa_data1traces4/i");//UInt_t->i
	tree->Branch("lisa_data1traces4I", fake_lisa_data1traces4I, "fake_lisa_data1traces4I[fake_lisa_data1traces4]/i");
	tree->Branch("lisa_data1traces4v", fake_lisa_data1traces4v, "fake_lisa_data1traces4v[fake_lisa_data1traces4]/i");

	tree->Branch("lisa_data1traces5", &fake_lisa_data1traces5, "fake_lisa_data1traces5/i");//UInt_t->i
	tree->Branch("lisa_data1traces5I", fake_lisa_data1traces5I, "fake_lisa_data1traces5I[fake_lisa_data1traces5]/i");
	tree->Branch("lisa_data1traces5v", fake_lisa_data1traces5v, "fake_lisa_data1traces5v[fake_lisa_data1traces5]/i");

	tree->Branch("lisa_data1traces6", &fake_lisa_data1traces6, "fake_lisa_data1traces6/i");//UInt_t->i
	tree->Branch("lisa_data1traces6I", fake_lisa_data1traces6I, "fake_lisa_data1traces6I[fake_lisa_data1traces6]/i");
	tree->Branch("lisa_data1traces6v", fake_lisa_data1traces6v, "fake_lisa_data1traces6v[fake_lisa_data1traces6]/i");

	tree->Branch("lisa_data1traces7", &fake_lisa_data1traces7, "fake_lisa_data1traces7/i");//UInt_t->i
	tree->Branch("lisa_data1traces7I", fake_lisa_data1traces7I, "fake_lisa_data1traces7I[fake_lisa_data1traces7]/i");
	tree->Branch("lisa_data1traces7v", fake_lisa_data1traces7v, "fake_lisa_data1traces7v[fake_lisa_data1traces7]/i");

	tree->Branch("lisa_data1traces8", &fake_lisa_data1traces8, "fake_lisa_data1traces8/i");//UInt_t->i
	tree->Branch("lisa_data1traces8I", fake_lisa_data1traces8I, "fake_lisa_data1traces8I[fake_lisa_data1traces8]/i");
	tree->Branch("lisa_data1traces8v", fake_lisa_data1traces8v, "fake_lisa_data1traces8v[fake_lisa_data1traces8]/i");

}

void tele::clearOpt(){
	//fake_traceLength = 0;
	//for(int ii=0;ii<16;ii++){
	//	fake_lisa_data1traces[ii] = 0; //UInt_t
	//	for(int jj=0; jj<TRACE_LENGTH;jj++){
	//		fake_lisa_data1tracesI[ii][jj] = 0;//UInt_t
	//		fake_lisa_data1tracesv[ii][jj] = 0;
	//	}
	//}

	fake_lisa_data1traces1 = 0; //UInt_t
	fake_lisa_data1traces2 = 0; //UInt_t
	fake_lisa_data1traces3 = 0; //UInt_t
	fake_lisa_data1traces4 = 0; //UInt_t
	fake_lisa_data1traces5 = 0; //UInt_t
	fake_lisa_data1traces6 = 0; //UInt_t
	fake_lisa_data1traces7 = 0; //UInt_t
	fake_lisa_data1traces8 = 0; //UInt_t
	for(int jj=0; jj<TRACE_LENGTH;jj++){
		fake_lisa_data1traces1I[jj] = 0;//UInt_t
		fake_lisa_data1traces1v[jj] = 0;
		fake_lisa_data1traces2I[jj] = 0;//UInt_t
		fake_lisa_data1traces2v[jj] = 0;
		fake_lisa_data1traces3I[jj] = 0;//UInt_t
		fake_lisa_data1traces3v[jj] = 0;
		fake_lisa_data1traces4I[jj] = 0;//UInt_t
		fake_lisa_data1traces4v[jj] = 0;
		fake_lisa_data1traces5I[jj] = 0;//UInt_t
		fake_lisa_data1traces5v[jj] = 0;
		fake_lisa_data1traces6I[jj] = 0;//UInt_t
		fake_lisa_data1traces6v[jj] = 0;
		fake_lisa_data1traces7I[jj] = 0;//UInt_t
		fake_lisa_data1traces7v[jj] = 0;
		fake_lisa_data1traces8I[jj] = 0;//UInt_t
		fake_lisa_data1traces8v[jj] = 0;
	}
}

Bool_t tele::Notify()
{
	// The Notify() function is called when a new file is opened. This
	// can be either for a new TTree in a TChain or when when a new TTree
	// is started when using PROOF. It is normally not necessary to make changes
	// to the generated code, but the routine can be extended by the
	// user if needed. The return value is currently not used.

	return kTRUE;
}

void tele::Show(Long64_t entry)
{
	// Print contents of entry.
	// If entry is not specified, print current entry
	if (!fChain) return;
	fChain->Show(entry);
}
Int_t tele::Cut(Long64_t entry)
{
	// This function may be called from Loop.
	// returns  1 if entry is accepted.
	// returns -1 otherwise.
	return 1;
}
#endif 
