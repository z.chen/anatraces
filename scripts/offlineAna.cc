//for HiMAC exp
//=======================================================================
//root [0] .L offlineAna.cc
//root [1] offlineAna tt("ch0-7_random_gaus1.root")
//root [2] tt.hitPattern()
//               OR
//root [1] offlineAna *tt = new offlineAna("ch0-7_random_gaus1.root")
//root [2] tt->hitPattern()
//=======================================================================
//change display mode: see offlineAna.hh

#include "offlineAna.hh"
#include "position.hh"
#include "LISA.hh"
#include "mycut.cxx"
#include "peaks.hh"
#include <TH2.h>

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::hitPattern()
{
	//*** data reading ***//
	Long64_t nentries = iptTree->GetEntriesFast();
	std::cout<<"===================================="<<std::endl;
	std::cout<<"--->Total event number: "<<nentries<<std::endl;

	if(USE_CENTER)std::cout<<"Center position is used."<<std::endl;
	if(USE_FAKE)std::cout<<"Center position + random values(fake results) is used."<<std::endl;
	if(SINGLE_MODE)std::cout<<"Running at single mode."<<std::endl;
	if(HOLDON_MODE)std::cout<<"Running at Hold-on mode."<<std::endl;

	//*** tracking ***//
	setEnt();
	loadPosition();
	setDisplay();

	//*** loop ***//
	for (Long64_t jentry=0; jentry<10000;jentry++)
		//for (Long64_t jentry=0; jentry<nentries;jentry++)
	{
		offlineLoop(hit,jentry,nentries);
	}
	std::cout<<"===================================="<<std::endl;
	std::cout<<"The end."<<std::endl;
	return;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::energyCalib()
{
	TH1F* fdh[24];//8*3
	double fmeans[24];
	int npeaks = 1;
	double *fpar = NULL;

	setEnt();

	TFile *ff1 = new TFile("himac_anaroot_run6_all_thre200.root");
	std::cout<<"***start to analyze run6..."<<std::endl;
	TTree *fftree = (TTree*)ff1->Get("tree");

	for(int ii=0;ii<4;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,10000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=12;ii<16;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,10000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=0;ii<4;ii++)fdh[ii] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run6_ch%d",ii));
	for(int ii=12;ii<16;ii++)fdh[ii-8] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run6_ch%d",ii));

	//*** display ***//
	TCanvas *mycan = new TCanvas("mycan","mycan",0,0,800,800);
	for(int ii=0;ii<8;ii++){
		fdh[ii]->GetXaxis()->SetRangeUser(3000,8000);
		mycan->Update();
		mycan->Modified();
		fpar = peaks(fdh[ii],npeaks);
		fmeans[ii] = fpar[3*(npeaks-1)+3];
		fdh[ii]->GetXaxis()->SetRangeUser(fmeans[ii]-500,fmeans[ii]+500);
		peaks(fdh[ii],npeaks);
		mycan->Print(Form("./figs/id_%02d.png",ii));
		//std::cout<<"--->amplitude: "<<fpar[3*(npeaks-1)+2]<<std::endl;
		std::cout<<"--->mean: "<<fmeans[ii]<<std::endl;
		//std::cout<<"--->sigma: "<<fpar[3*(npeaks-1)+4]<<std::endl;
	}
	std::cout<<"run6, done."<<std::endl;
	//return;

	TFile *ff2 = new TFile("himac_anaroot_run7_all_thre200.root");
	fftree = (TTree*)ff2->Get("tree");
	std::cout<<"***start to analyze run7..."<<std::endl;

	for(int ii=0;ii<4;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,10000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=12;ii<16;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,10000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=0;ii<4;ii++)fdh[ii+8] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run7_ch%d",ii));
	for(int ii=12;ii<16;ii++)fdh[ii] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run7_ch%d",ii));
	for(int ii=8;ii<16;ii++){
		fdh[ii]->GetXaxis()->SetRangeUser(3000,8000);
		mycan->Update();
		mycan->Modified();
		fpar = peaks(fdh[ii],npeaks);
		fmeans[ii] = fpar[3*(npeaks-1)+3];
		fdh[ii]->GetXaxis()->SetRangeUser(fmeans[ii]-500,fmeans[ii]+500);
		peaks(fdh[ii],npeaks);
		mycan->Print(Form("./figs/id_%02d.png",ii));
		//std::cout<<"--->amplitude: "<<fpar[3*(npeaks-1)+2]<<std::endl;
		//std::cout<<"--->mean: "<<fpar[3*(npeaks-1)+3]<<std::endl;
		//std::cout<<"--->sigma: "<<fpar[3*(npeaks-1)+4]<<std::endl;
	}


	TFile *ff3 = new TFile("himac_anaroot_run9_all_thre200.root");
	fftree = (TTree*)ff3->Get("tree");
	std::cout<<"***start to analyze run9..."<<std::endl;

	for(int ii=0;ii<4;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,10000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=12;ii<16;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,10000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=0;ii<4;ii++)fdh[ii+16] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run9_ch%d",ii));
	for(int ii=12;ii<16;ii++)fdh[ii+8] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run9_ch%d",ii));
	for(int ii=16;ii<24;ii++){
		fdh[ii]->GetXaxis()->SetRangeUser(3000,8000);
		mycan->Update();
		mycan->Modified();
		fpar = peaks(fdh[ii],npeaks);
		fmeans[ii] = fpar[3*(npeaks-1)+3];
		fdh[ii]->GetXaxis()->SetRangeUser(fmeans[ii]-500,fmeans[ii]+500);
		peaks(fdh[ii],npeaks);
		mycan->Print(Form("./figs/id_%02d.png",ii));
		//std::cout<<"--->amplitude: "<<fpar[3*(npeaks-1)+2]<<std::endl;
		//std::cout<<"--->mean: "<<fpar[3*(npeaks-1)+3]<<std::endl;
		//std::cout<<"--->sigma: "<<fpar[3*(npeaks-1)+4]<<std::endl;
	}

	for(int ii=0;ii<24;ii++)std::cout<<"--->collected means: "<<"id = "<<ii<<"  "<<fmeans[ii]<<std::endl;
	//mycan->Divide(2,2);
	//for(int ii=4;ii<8;ii++){
	//	mycan->cd(ii+1-4);
	//	fdh[ii]->Draw();
	//}
	
	//*** calib ***/
	double det0[3];//ref
	double det1[3];
	double det2[3];
	double det3[3];
	double det12[3];//ref
	double det12_2[2];//kick out one point
	double det13[3];
	double det13_2[2];
	double det14[3];
	double det15[3];
	double det15_2[2];
	double fitPars[2];
	//*** eris ***//
	for(int ii=0;ii<3;ii++)det0[ii] = fmeans[8*ii];
	for(int ii=0;ii<3;ii++)det1[ii] = fmeans[8*ii+1];
	for(int ii=0;ii<3;ii++)det2[ii] = fmeans[8*ii+2];
	for(int ii=0;ii<3;ii++)det3[ii] = fmeans[8*ii+3];
	TCanvas *fit_Can = new TCanvas("fit_Can","fit_Can",0,0,1000,800);
	fit_Can->Divide(3,2);
	fit_Can->cd(1);
	//***test***//
	for(int ii=0;ii<3;ii++)std::cout<<"---> det0: "<<det0[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det1: "<<det1[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det2: "<<det2[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det3: "<<det3[ii]<<std::endl;
	TGraph *fcalib1 = new TGraph(3,det1,det0);
	TGraph *fcalib2 = new TGraph(3,det2,det0);
	TGraph *fcalib3 = new TGraph(3,det3,det0);
	fcalib1->Draw("AP*");
	TF1 *calib_fit = new TF1("calib_fit","pol1",3000,8000);
	fcalib1->Fit("calib_fit","q");
	fcalib1->SetTitle("calib1");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(2);

	fcalib2->Draw("AP*");
	fcalib2->Fit("calib_fit","q");
	fcalib2->SetTitle("calib2");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(3);

	fcalib3->Draw("AP*");
	fcalib3->Fit("calib_fit","q");
	fcalib3->SetTitle("calib3");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	//*** sparrow ***//
	for(int ii=0;ii<3;ii++)det12[ii] = fmeans[8*(ii+1)-4];
	for(int ii=0;ii<3;ii++)det13[ii] = fmeans[8*(ii+1)-3];
	for(int ii=0;ii<3;ii++)det14[ii] = fmeans[8*(ii+1)-2];
	for(int ii=0;ii<3;ii++)det15[ii] = fmeans[8*(ii+1)-1];
	fit_Can->cd(4);
	//kick out middle point
	//det12_2[0]=det12[0];
	//det12_2[1]=det12[2];
	//det13_2[0]=det13[0];
	//det13_2[1]=det13[2];
	//det15_2[0]=det15[0];
	//det15_2[1]=det15[2];
	//***test***//
	for(int ii=0;ii<3;ii++)std::cout<<"---> det12: "<<det12[ii]<<std::endl;
	for(int ii=0;ii<2;ii++)std::cout<<"---> det13: "<<det13[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det14: "<<det14[ii]<<std::endl;
	for(int ii=0;ii<2;ii++)std::cout<<"---> det15: "<<det15[ii]<<std::endl;
	TGraph *fcalib4 = new TGraph(3,det13,det12);
	TGraph *fcalib5 = new TGraph(3,det14,det12);
	TGraph *fcalib6 = new TGraph(3,det15,det12);
	fcalib4->Draw("AP*");
	fcalib4->Fit("calib_fit","q");
	fcalib4->SetTitle("calib13");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(5);

	fcalib5->Draw("AP*");
	fcalib5->Fit("calib_fit","q");
	fcalib5->SetTitle("calib14");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(6);

	fcalib6->Draw("AP*");
	fcalib6->Fit("calib_fit","q");
	fcalib6->SetTitle("calib15");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->Print("./figs/linear_fit.png");
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::getPID(int fch1 = 0, int fch2 = 12)
{
	char fnamex[30] = "NULL";
	char fnamey[30] = "NULL";
	setEnt();
	pidcut();
	iptTree->SetBranchStatus("Eraw",1);  // activate branchname
	iptTree->SetBranchStatus("Ecal",1);  // activate branchname

	//*** data reading ***//
	Long64_t nentries = iptTree->GetEntriesFast();
	std::cout<<"===================================="<<std::endl;
	std::cout<<"--->Total event number: "<<nentries<<std::endl;
	TH2F *fPID = new TH2F("fPID","fPID",200,0,8100,200,0,8100);
	fPID->SetTitle("PID");
	fPID->SetName("PID");
	for(int ii=0;ii<nentries;ii++){
		iptTree->GetEntry(ii);
		if( hit != 2 )continue;
		double e1=0;
		double e2=0;
		for(int jhit = 0;jhit<hit;jhit++){
			//*** all to all ***//
			if( febCh[jhit] >=0 && febCh[jhit]<=3  )
			{
				e1 = Ecal[jhit];//Eris
				//e1 = Eraw[jhit];//Eris
				strcpy(fnamex,"Eris");
			}
			if( febCh[jhit] >=12 && febCh[jhit] <=15 )
			{
				e2 = Ecal[jhit];//Sparrow
				//e2 = Eraw[jhit];//Sparrow
				strcpy(fnamey,"Sparrow");
			}
			//*** single to single ***//
			//if( febCh[jhit] == fch1  )
			//{
			//	e1 = Ecal[jhit];//Eris
				//e1 = Eraw[jhit];//Eris
			//	getMapping(febCh[jhit],fnamex);
			//}
			//if( febCh[jhit] == fch2 )
			//{
			//	e2 = Ecal[jhit];//Sparrow
				//e2 = Eraw[jhit];//Sparrow
			//	getMapping(febCh[jhit],fnamey);
			//}
		}
		fPID->Fill(e1,e2);
		//if(cutg->IsInside(e1,e2))fPID->Fill(e1,e2);
	}
	fPID->GetXaxis()->SetTitle(fnamex);
	fPID->GetYaxis()->SetTitle(fnamey);
	fPID->GetYaxis()->SetTitleOffset(1.2);
	TCanvas *fcan = new TCanvas("PID","PID",0,0,900,700);
	//TCanvas *fcan = new TCanvas("PID","PID",0,0,1100,900);
	//fcan->Divide(1,2);
	//fcan->cd(1);
	gPad->SetLogz();
	fPID->Draw("colz");
	return;
	//*** 2 layer calibration ***//
	TH2D *calib = (TH2D*)fPID->Clone("calib");
	fcan->cd(2);
	calib->Draw("colz");
	TF1 *flin = new TF1("flin","pol1",1000,5000);
	flin->SetParameter(1,1);

	calib->Fit("flin","rob=0.5");
	flin->Draw("same");
	return;
	TH1F *py = (TH1F*)fPID->ProjectionY("py",1,1000);
	py->Draw();
	py->GetXaxis()->SetRangeUser(100,8000);
	py->GetXaxis()->SetTitleOffset(1.0);
	//TH1F *ftest=0;
	//TH1F *ftest = new TH1F("ftest","ftest",1000,0,1000);
	double famp[1]  = {2200};
	double fmean[1] = {5000};
	double fsig[1]  = {40};
	singleFit(py,famp,fmean,fsig);
	//std::cout<<"par0 = "<<famp[0]<<"; par1 = "<<fmean[0]<<"; par2 = "<<fsig[0]<<std::endl;
	std::cout<<"--->Fitting results:"<<std::endl;
	std::cout<<"mean = "<<fmean[0]<<"; sigma = "<<fsig[0]<<std::endl;
	std::cout<<"dE/E(FWHM) = "<<fsig[0]*2.355/fmean[0]*100<<"\%"<<std::endl;
}


//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
//theta_phi plot
void offlineAna::hitPattern_2()
{
	Long64_t nentries = iptTree->GetEntriesFast();
	std::cout<<"--->Total event number: "<<nentries<<std::endl;

	TH2F *h_thetaPhi = new TH2F("theta_phi","theta_phi",180,-180,180,180,0,180);//deg
	h_thetaPhi->GetXaxis()->SetTitle("Phi[deg]");
	h_thetaPhi->GetYaxis()->SetTitle("Theta[deg]");
	TCanvas *c_tracking = new TCanvas("tracking","tracking",10,10,800,400);
	//*** defination ***//
	//*** ---***--- *** //
	//*** |0|***|1| *** //
	//*** ---***--- *** //
	//*** |3|***|2| *** //
	//*** ---***--- *** //
	//**X--->********** //
	//******^********** //
	//**Y**/ \********* //
	//******|********** //
	//0,(-2.5,2.5);1,(2.5,2.5)//
	//3,(-2.5,-2.5);2,(2.5,-2.5) //
	//eris, z=-5; sparrow, z=+5 //mm
	//***************** //
	for (Long64_t jentry=0; jentry<1000;jentry++)
		//for (Long64_t jentry=0; jentry<nentries;jentry++)
	{
		iptTree->GetEntry(jentry);
		TVector3 v3;//
		vector<double>ftheta;
		vector<double>fphi;
		ftheta.clear();
		fphi.clear();
		//TVector3 v_sparrow;//z=+5mm
		//if( febCh[0] == 0 && hit == 2 ){
		for( int hh = 0; hh < hit; hh++ )
		{
			v3.SetXYZ(0,0,0);
			switch(febCh[hh]) {//compatible with ch0-7
				case 0:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(0.5,4.5),-5);
					break;
				case 1:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(0.5,4.5),-5);
					break;
				case 2:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(-4.5,-0.5),-5);
					break;
				case 3:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(-4.5,-0.5),-5);
					break;
				case 4:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(0.5,4.5),5);
					break;
				case 5:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(0.5,4.5),5);
					break;
				case 6:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(-4.5,-0.5),5);
					break;
				case 7:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(-4.5,-0.5),5);
					break;
				default:
					std::cout<<"--->Error501. Incorrect channel ID. It's only compatible with ch0-7."<<std::endl;
					return;
			}
			ftheta.push_back(v3.Theta()/TMath::Pi()*180.);
			fphi.push_back(v3.Phi()/TMath::Pi()*180.);
			h_thetaPhi->Fill(fphi.back(),ftheta.back());
		}
		//}
		if(ftheta.size()!=0 &&  ftheta.size() != hit)
		{
			std::cout<<"Error502. ftheta.size != hit"<<std::endl;
			std::cout<<"ftheta.size = "<<ftheta.size()<<std::endl;
			std::cout<<"hit = "<<hit<<std::endl;
			return;
		}
		if(jentry%100==0){
			h_thetaPhi->Draw("colz");
			gPad->Modified();
			gPad->Update();
		}

		std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
		//if(jentry%100==0)std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
		fflush(stdout);
		printf("\r");
	}//end loop
	/* 
	   c_hit_pattern->cd(1);
	   h_eris->Draw();
	   gPad->Modified();
	   gPad->Update();

	   c_hit_pattern->cd(2);
	   h_sparrow->Draw();
	   gPad->Modified();
	   gPad->Update();
	   */
	std::cout<<"===================================="<<std::endl;
	std::cout<<"The end."<<std::endl;
	return;
}
