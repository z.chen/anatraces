//
//Error4xxx
//

#ifndef offlineAna_hh
#define offlineAna_hh

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include "position.hh"
#include "LISA.hh"
//============================================================================================================================================//
// change display mode here //
const bool IF_DEBUG = 0;
const int SPEED = 10;  //the larger the speed value you set, the lower the refresh frequency you'll get. e.g. 100, refresh every 100 events.
const bool USE_CENTER = true;
const bool USE_FAKE = false;
const bool SINGLE_MODE = false;
const bool HOLDON_MODE = true; 
//============================================================================================================================================//

class offlineAna {
	public:
		TFile *fiptFile;
		TTree *iptTree;
		//*** root file structure ***//
		// Declaration of leaf types
		Int_t          TRIGGER_c4;
		Int_t          EVENTNO_c4;
		UInt_t          hit;
		Int_t           febCh[16];   //[hit]
		Int_t           entry;
		Int_t           entry_empty[16];   //[hit]
		Long64_t        ts[16];   //[hit]
		Long64_t        tsS[16];   //[hit]
		Double_t        Eraw[16];   //[hit]
		Double_t        Ecal[16];   //[hit]
		Double_t        T_cfd[16];   //[hit]
		Int_t           if_overflow[16];   //[hit]
		UInt_t          traceLength[16];   //[hit]
		Double_t        tracesX[16][3000];   //[hit]
		Double_t        tracesY[16][3000];   //[hit]

		// List of branches
		TBranch        *b_TRIGGER_c4;   //!
		TBranch        *b_EVEVTNO_c4;   //!
		TBranch        *b_hit;   //!
		TBranch        *b_febCh;   //!
		TBranch        *b_entry;   //!
		TBranch        *b_entry_empty;   //!
		TBranch        *b_ts;   //!
		TBranch        *b_tsS;   //!
		TBranch        *b_Eraw;   //!
		TBranch        *b_Ecal;   //!
		TBranch        *b_T_cfd;   //!
		TBranch        *b_if_overflow;   //!
		TBranch        *b_traceLength;   //!
		TBranch        *b_tracesX;   //!
		TBranch        *b_tracesY;   //!

		//*** display ***//
		TLine *p_eris1;
		TLine *p_eris2;
		TLine *p_sparrow1;
		TLine *p_sparrow2;
		TLine *pzline;

		TH2D *offline_hx;
		TH2D *offline_hy;
		TH2D *offline_hpz;

		TCanvas *offline_c1;
		TCanvas *offline_c2;
		TCanvas *c_hit_pattern;

		TH2F *h_eris;
		TH2F *h_sparrow;

		LISA_FRAME hit_eris;
		LISA_FRAME hit_sparrow;

		offlineAna(TString sIptFileName = "himac_anaroot_run6_all_thre200_energycali.root");
		//offlineAna(TString sIptFileName = "himac_anaroot_run6_all_thre200.root");
		virtual ~offlineAna();
		virtual void filltrack(double x1,double y1,double z1,double x2, double y2,double z2, double pz,TH2D *offline_hx,TH2D *offline_hy,TH2D *offline_hpz);
		virtual void InitRoot(TTree *iptTree);
		virtual void hitPattern();
		virtual void hitPattern_2();
		virtual void getPID(int fch1, int fch2);
		virtual void energyCalib();
		virtual void singleFit(TH1F *fhh, double *famp, double *fmean, double *fsigma);
		virtual void setEnt();
		virtual void loadPosition();
		virtual void setDisplay();
		virtual void convert(float fk1, float fb1, float fk2, float fb2);
		virtual void offlineLoop(UInt_t fhit,Long64_t jentry, Long64_t nentries);
		virtual void setFileName(TString sIptFileName = "ch0-7_random.root"){f_iptFile = sIptFileName;};
		virtual TString getFileName(){ return f_iptFile;};

	private:
		TString f_iptFile;

};

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
offlineAna::offlineAna(TString sIptFileName = "ch0-7_random.root")
{
	setFileName(sIptFileName);
	fiptFile = new TFile(getFileName(),"READ");
	//fiptFile = new TFile(sIptFileName,"READ");
	if( !fiptFile->IsOpen() ){
		std::cout<<"--->Error401.Can not open root file "<<sIptFileName<<std::endl;
		return;
	}
	fiptFile->GetObject("tree",iptTree);
	InitRoot(iptTree);
}
//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
offlineAna::~offlineAna()
{
	std::cout<<"--->The end."<<std::endl;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::loadPosition(){
	loadPositionInfo();
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::singleFit(TH1F *fhh, double *famp, double *fmean, double *fsigma){
	if(!fhh || fhh->GetEntries()==0 ){
		std::cout<<"Error402!!! Empty histo. Please check."<<std::endl;
		return;
	}
	TF1 *fgaus = new TF1("fgaus","pol0(0)+gaus(1)",0,10000);
	fgaus->SetParameter(1,famp[0]);
	fgaus->SetParameter(2,fmean[0]);
	fgaus->SetParameter(3,fsigma[0]);
	fgaus->SetParLimits(1,famp[0]-famp[0]/10.,famp[0]+famp[0]/10.);
	fgaus->SetParLimits(2,fmean[0]-100.,fmean[0]+100.);
	fgaus->SetParLimits(3,fsigma[0]-10.,fsigma[0]+10.);
	fhh->Fit("fgaus","R");
	double fpar[6];
	fgaus->GetParameters(fpar);
	famp[0]   = fpar[1];
	fmean[0]  = fpar[2];
	fsigma[0] = fpar[3];

}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::filltrack(double x1,double y1,double z1,double x2, double y2,double z2, double pz,TH2D *offline_hx,TH2D *offline_hy,TH2D *offline_hpz)
{
	if(SINGLE_MODE){
		offline_hx->Reset();
		offline_hy->Reset();
		offline_hpz->Reset();
	}
	double px,py;
	px=x1+(x2-x1)/(z2-z1)*(pz-z1);
	py=y1+(y2-y1)/(z2-z1)*(pz-z1);

	offline_hpz->Fill(px,py);

	int nbinxx=offline_hx->GetNbinsX();
	//std::cout<<"offline_hx_NBINS = "<<nbinxx<<std::endl;
	double zmin=offline_hx->GetXaxis()->GetXmin();
	double zmax=offline_hx->GetXaxis()->GetXmax();
	for(int ii=0;ii<nbinxx;ii++)
	{
		double nowz=zmin+(zmax-zmin)*ii/(nbinxx*1.0);
		double nowf=x1+(x2-x1)/(z2-z1)*(nowz-z1);
		//std::cout<<"nowz = "<<nowz<<std::endl;
		//std::cout<<"nowf = "<<nowf<<std::endl;
		offline_hx->Fill(nowz,nowf);
	}

	nbinxx=offline_hy->GetNbinsX();
	//std::cout<<"offline_hy_NBINS = "<<nbinxx<<std::endl;
	zmin=offline_hy->GetXaxis()->GetXmin();
	zmax=offline_hy->GetXaxis()->GetXmax();
	for(int ii=0;ii<nbinxx;ii++)
	{
		double nowz=zmin+(zmax-zmin)*ii/(nbinxx*1.0);
		double nowf=y1+(y2-y1)/(z2-z1)*(nowz-z1);
		offline_hy->Fill(nowz,nowf);
	}
	return;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::InitRoot(TTree *ftree){

	// Set branch addresses and branch pointers
	if (!ftree) {
		std::cout<<"--->Error403!!! Failed to init root file. Couln't find tree."<<std::endl;
		return;
	}
	ftree->SetBranchAddress("TRIGGER_c4", &TRIGGER_c4, &b_TRIGGER_c4);
	ftree->SetBranchAddress("EVENTNO_c4", &EVENTNO_c4, &b_EVEVTNO_c4);
	ftree->SetBranchAddress("hit", &hit, &b_hit);
	ftree->SetBranchAddress("febCh", febCh, &b_febCh);
	ftree->SetBranchAddress("entry", &entry, &b_entry);
	ftree->SetBranchAddress("entry_empty", entry_empty, &b_entry_empty);
	ftree->SetBranchAddress("ts", ts, &b_ts);
	ftree->SetBranchAddress("tsS", tsS, &b_tsS);
	ftree->SetBranchAddress("Eraw", Eraw, &b_Eraw);
	ftree->SetBranchAddress("Ecal", Ecal, &b_Ecal);
	ftree->SetBranchAddress("T_cfd", T_cfd, &b_T_cfd);
	ftree->SetBranchAddress("traceLength", traceLength, &b_traceLength);
	ftree->SetBranchAddress("tracesX", tracesX, &b_tracesX);
	ftree->SetBranchAddress("tracesY", tracesY, &b_tracesY);

	ftree->SetBranchStatus("*",0);  // disable all branches
	ftree->SetBranchStatus("hit",1);  // activate branchname
	ftree->SetBranchStatus("febCh",1);  // activate branchname
	std::cout<<"--->init complete."<<std::endl;
	std::cout<<"--->read data from "<<getFileName()<<std::endl;

}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
//define histograms, canvas, ...//
void offlineAna::setDisplay(){

	p_eris1=new TLine(positionZ_eris,coor_LISA_y1[3],positionZ_eris,coor_LISA_y2[3]);
	p_eris2=new TLine(positionZ_eris,coor_LISA_y1[0],positionZ_eris,coor_LISA_y2[0]);
	p_eris1->SetLineWidth(5);
	p_eris1->SetLineColor(kRed);
	p_eris2->SetLineWidth(5);
	p_eris2->SetLineColor(kRed);

	p_sparrow1=new TLine(positionZ_sparrow,coor_LISA_y1[3],positionZ_sparrow,coor_LISA_y2[3]);
	p_sparrow2=new TLine(positionZ_sparrow,coor_LISA_y1[0],positionZ_sparrow,coor_LISA_y2[0]);
	p_sparrow1->SetLineWidth(5);
	p_sparrow1->SetLineColor(kRed);
	p_sparrow2->SetLineWidth(5);
	p_sparrow2->SetLineColor(kRed);

	pzline=new TLine(positionZ_degrader,-1,positionZ_degrader,1);//z==0;degrader
	pzline->SetLineWidth(5);
	pzline->SetLineColor(kBlack);//6,purple

	offline_hx=new TH2D("offline_hx","Projection of X-Z (TOP VIEW)",20,-10,10,100,-10,10);
	offline_hy=new TH2D("offline_hy","Projection of Y-Z (SIDE VIEW)",20,-10,10,100,-10,10);
	offline_hpz=new TH2D("offline_hpz","Distribution at Z = 0mm",36,-6,6,36,-6,6);
	offline_hx->GetXaxis()->SetTitle("PositionZ[mm]");
	offline_hx->GetYaxis()->SetTitle("PositionX[mm]");
	offline_hy->GetXaxis()->SetTitle("PositionZ[mm]");
	offline_hy->GetYaxis()->SetTitle("PositionY[mm]");
	offline_hpz->GetXaxis()->SetTitle("PositionX[mm]");
	offline_hpz->GetYaxis()->SetTitle("PositionY[mm]");
	offline_hpz->GetYaxis()->SetTitleOffset(.8);
	offline_hx->SetStats(kFALSE);
	offline_hy->SetStats(kFALSE);
	offline_hpz->SetStats(kFALSE);

	offline_c1=new TCanvas("offline_c1","Projection",0,0,900,800);
	offline_c1->Divide(1,2);
	offline_c1->Draw();
	offline_c1->cd(1);
	//offline_c1->cd(1)->SetGrid();
	offline_hx->Draw();
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->cd(2);
	//offline_c1->cd(2)->SetGrid();
	offline_hy->Draw();
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->Modified();
	offline_c1->Update();

	offline_c2=new TCanvas("offline_c2","Z=0",910,0,800,800);
	offline_c2->cd();
	//offline_c2->cd()->SetGrid();
	offline_hpz->Draw();
	offline_c2->Modified();
	offline_c2->Update();

	//*** hit pattern ***//
	h_eris = new TH2F("Eris","Eris",30,-7,7,30,-7,7);//mm
	h_eris->GetXaxis()->SetTitle("PositionX[mm]");
	h_eris->GetYaxis()->SetTitle("PositionY[mm]");
	h_sparrow = new TH2F("Sparrow","Sparrow",30,-7,7,30,-7,7);
	h_sparrow->GetXaxis()->SetTitle("PositionX[mm]");
	h_sparrow->GetYaxis()->SetTitle("PositionY[mm]");
	h_eris->SetStats(kFALSE);
	h_sparrow->SetStats(kFALSE);
	c_hit_pattern = new TCanvas("hit_pattern","hit_pattern",0,850,900,400);
	c_hit_pattern->Divide(2,1);
	for(int ii=0;ii<4;ii++){//2x2

		//*** eris ***//
		hit_eris.fbox.push_back(new TBox(coor_LISA_x1[ii],coor_LISA_y1[ii],coor_LISA_x2[ii],coor_LISA_y2[ii]));
		hit_eris.fbox[ii]->SetLineWidth(5);
		hit_eris.fbox[ii]->SetLineColor(kRed);
		hit_eris.fbox[ii]->SetFillStyle(0);
		//*** sparrow ***//
		hit_sparrow.fbox.push_back(new TBox(coor_LISA_x1[ii],coor_LISA_y1[ii],coor_LISA_x2[ii],coor_LISA_y2[ii]));
		hit_sparrow.fbox[ii]->SetLineWidth(5);
		hit_sparrow.fbox[ii]->SetLineColor(6);//6, purple
		hit_sparrow.fbox[ii]->SetFillStyle(0);
	}

}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
//fhit, number of fired detectors in each event//
void offlineAna::offlineLoop(UInt_t fhit, Long64_t jentry, Long64_t nentries){

	if( jentry%SPEED !=0 )return;
	iptTree->GetEntry(jentry);
	//*** protection ***//
	if(fhit>8){
		std::cout<<"Error404!!!Illegal hit number: "<<fhit<<std::endl;
		return;
	}
	if(IF_DEBUG)std::cout<<"in test1"<<std::endl;
	double x1[1],y1[1],x2[1],y2[1];
	if(IF_DEBUG)std::cout<<"in test2"<<std::endl;
	for( int hh = 0; hh < fhit; hh++ )
	{
		if(IF_DEBUG)std::cout<<"in test3"<<std::endl;
		if(IF_DEBUG)std::cout<<"hit = "<<fhit<<std::endl;
		if(USE_CENTER){
			if(febCh[hh]<4){
				x1[0] = coor_LISA_center_x[febCh[hh]];
				y1[0] = coor_LISA_center_y[febCh[hh]];
				h_eris->Fill(x1[0],y1[0]);
			}
			if(IF_DEBUG)std::cout<<"in test4"<<std::endl;
			if(febCh[hh]>3){
				x2[0] = coor_LISA_center_x[febCh[hh]%4];
				y2[0] = coor_LISA_center_y[febCh[hh]%4];
				h_sparrow->Fill(x2[0],y2[0]);
			}
		}else if(USE_FAKE){
			if(IF_DEBUG)std::cout<<"in test5"<<std::endl;
			if(febCh[hh]<4){
				x1[0] = gRandom->Uniform(coor_LISA_x1[febCh[hh]],coor_LISA_x2[febCh[hh]]);
				y1[0] = gRandom->Uniform(coor_LISA_y1[febCh[hh]],coor_LISA_y2[febCh[hh]]);
				h_eris->Fill(x1[0],y1[0]);
			}
			if(febCh[hh]>3){
				x2[0] = gRandom->Uniform(coor_LISA_x1[febCh[hh]%4],coor_LISA_x2[febCh[hh]%4]);
				y2[0] = gRandom->Uniform(coor_LISA_y1[febCh[hh]%4],coor_LISA_y2[febCh[hh]%4]);
				h_sparrow->Fill(x2[0],y2[0]);
			}
		}else{
			if(febCh[hh]<4){
				x1[0] = gRandom->Uniform(coor_LISA_x1[febCh[hh]],coor_LISA_x2[febCh[hh]]);
				y1[0] = gRandom->Uniform(coor_LISA_y1[febCh[hh]],coor_LISA_y2[febCh[hh]]);
				h_eris->Fill(x1[0],y1[0]);
			}
			if(febCh[hh]>3){
				x2[0] = gRandom->Uniform(coor_LISA_x1[febCh[hh]%4],coor_LISA_x2[febCh[hh]%4]);
				y2[0] = gRandom->Uniform(coor_LISA_y1[febCh[hh]%4],coor_LISA_y2[febCh[hh]%4]);
				h_sparrow->Fill(x2[0],y2[0]);
			}
		}
	}//end of loop for hit
	if(IF_DEBUG)std::cout<<"in test6"<<std::endl;
	if( fhit == 2 && febCh[0] < 4 && febCh[1] > 3 )filltrack(x1[0],y1[0],positionZ_eris,x2[0],y2[0],positionZ_sparrow,positionZ_degrader,offline_hx,offline_hy,offline_hpz);
	//*** debug-begin ***//
	//int nbinsx = offline_hx->GetNbinsX();
	//std::cout<<"nbinsx after filling the histos = "<<nbinsx<<std::endl;
	//for(int ii=1;ii<nbinsx;ii++)
	//{
	//	double bcenterx = offline_hx->GetXaxis()->GetBinCenter(ii);
	//	int ibin = offline_hx->FindBin(bcenterx);
	//	std::cout<<"bin centerx = "<<bcenterx<<std::endl;
	//	std::cout<<"ibin = "<<ibin<<std::endl;
	//}
	//*** debug-end ***//

	//*** display ***//
	//if(jentry%SPEED==0)
	//if(jentry == 20)
	//{
	//*** tracking ***//
	offline_c1->cd(1);
	//if(if_rebin_x)offline_hx->RebinX(4);if_rebin_x=0;
	offline_hx->Draw("colz");
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->cd(2);
	//if(if_rebin_y)offline_hy->RebinX(4);if_rebin_y=0;
	offline_hy->Draw("colz");
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->Update();

	offline_c2->cd();
	offline_hpz->Draw("colz");
	offline_c2->Update();
	//*** hit pattern ***//
	c_hit_pattern->cd(1);
	h_eris->Draw("colz");
	for(int ii=0;ii<hit_eris.fbox.size();ii++)hit_eris.fbox[ii]->Draw();
	gPad->Modified();
	gPad->Update();

	c_hit_pattern->cd(2);
	h_sparrow->Draw("colz");
	for(int ii=0;ii<hit_sparrow.fbox.size();ii++)hit_sparrow.fbox[ii]->Draw();
	gPad->Modified();
	gPad->Update();
	//}//end if jentry
	//if(jentry%SPEED==0)std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
	std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
	fflush(stdout);
	printf("\r");
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::setEnt(){
	gROOT->SetStyle("Modern");
	//gStyle->SetHistFillColor(7);
	//gStyle->SetHistFillStyle(3002);
	//gStyle->SetHistLineColor(kBlue);
	//*** FAIR style ***//
	gStyle->SetHistFillColor(796);
	gStyle->SetHistFillStyle(1001);
	gStyle->SetHistLineColor(kBlack);
	gStyle->SetFuncColor(kRed);
	gStyle->SetFrameLineWidth(2);
	gStyle->SetCanvasColor(0);
	gStyle->SetTitleFillColor(0);
	gStyle->SetTitleStyle(0);
	gStyle->SetStatColor(0);
	gStyle->SetStatStyle(0);
	gStyle->SetStatX(0.9);  
	gStyle->SetStatY(0.9);  
	gStyle->SetPalette(1);
	//gStyle->SetOptLogz(1);
	//  gStyle->SetOptTitle(0);
	//gStyle->SetOptFit(1);
	gStyle->SetOptStat(1111111);
	//gStyle->SetOptStat(0);
	//gStyle->SetPadBorderMode(1);
	//  gStyle->SetOptDate(1);
	gStyle->SetLabelFont(132,"XYZ");
	gStyle->SetLabelSize(0.05,"X");
	gStyle->SetLabelSize(0.05,"Y");
	gStyle->SetLabelOffset(0.004);
	//gStyle->SetTitleOffset(0.1);
	gStyle->SetTitleFont(132,"XYZ");
	gStyle->SetTitleSize(0.045,"X");
	gStyle->SetTitleSize(0.045,"Y");
	//gStyle->SetTitleFont(132,"");
	gStyle->SetTextFont(132);
	gStyle->SetStatFont(132);
}

void offlineAna::convert(float fk1, float fb1, float fk2, float fb2){

	std::cout<<"New Slop = "<<fk1*fk2<<std::endl;
	std::cout<<"New bias = "<<fk2*fb1+fb2<<std::endl;

}

#endif
